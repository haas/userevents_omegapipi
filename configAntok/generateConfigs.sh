#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
	echo "There must be one argument: Year"
	exit 1;
fi


for x in *.cpp; do
	name=$(echo $x | cut -d'.' -f-1)
	if [ "$name" == "Threshold" ]; then
		continue 
	fi
	cpp -D YEAR=$1 $name.cpp > $name.yaml
	mv $name.yaml ${name}_${1}.yaml
done
