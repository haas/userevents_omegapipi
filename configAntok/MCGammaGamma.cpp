#define MC
#define NO_PI_RECO
#define ECAL1
#define ECAL2

NumberOfParticles: 3
AllParticles: &allChargedParticles [1, 2, 3]

TreeName: UserEvent1000

TreeBranches:

#include "source/TreeBranches.yaml"


Constants:

#define CONSTANTS
#include "source/Selection.yaml"
#include "source/ECAL.yaml"
#include "source/Pi0FitAndOmega.yaml"
#include "source/Beam.yaml"
#include "source/RPD.yaml"
#undef CONSTANTS

CalculatedQuantities:

#define CALC_QUANTITIES
#include "source/Selection.yaml"
#include "source/ECAL.yaml"
#include "source/PhotonPair.yaml"
#include "source/NoPiRecoVariables.yaml"
#undef CALC_QUANTITIES

Cuts:
#include "source/CutsMC.yaml"

Plots:
#define PLOTS
#include "source/PhotonPair.yaml"
#undef PLOTS
