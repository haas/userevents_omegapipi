#!/usr/bin/env bash
if [ "$1" != '2008' -a "$1" != '2009']; then
        echo "Missing or wrong Year: either 2008 and 2009 as first argument"
        exit 1;
fi
year="$1"
echo $year

exec="/nfs/freenas/tuph/e18/project/compass/analysis/phaas/software/antok/build/bin/runParallel"
data="/nfs/freenas/tuph/e18/scratch/user/phaas/phast${year}/uDST-*"

runConfig() {
	name=$(echo $1 | rev | cut -d'/' -f-1 | rev )
        mkdir $name
        cd $name
        $exec -c $1 $data -n 15 --memory=0.5G -t &
        sleep 10
        cd ..
}

for x in /nfs/freenas/tuph/e18/project/compass/analysis/phaas/software/userevents_omegapipi/configAntok/ThresholdStudies/*.yaml; do
        runConfig $x
done


