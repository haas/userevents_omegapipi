#!/usr/bin/env python


# merges files with post-correction and postpost-correction factors for ECAL1 and ECAL2 to compensate pi^0 mass shifts in omega pi pi


year = 2009
if (year == 2008):
	inputFileNames = ['./PostFactors2008.txt', './PostPostFactors2008.txt']
	outputFileName = './PostFactorsXPostPostFactors2008.txt'
else:
	inputFileNames = ['./PostFactors2009.txt', './PostPostFactors2009.txt']
	outputFileName = './PostFactorsXPostPostFactors2009.txt'

# read all correction factors
postCorrections = {}
with open(inputFileNames[0], 'r') as postFile:
	for line in postFile:
		line = line.strip()
		lineItems  = line.split()
		runNmb     = str(lineItems[0])
		factorECAL1 = float(lineItems[1])
		factorECAL2 = float(lineItems[2])
		if runNmb in postCorrections:
			print('Error: run number {} appears more than once'.format(runNmb))
		postCorrections[runNmb] = (factorECAL1, factorECAL2)

postPostCorrections = {}
with open(inputFileNames[1], 'r') as postPostFile:
	for line in postPostFile:
		line = line.strip()
		lineItems  = line.split()
		runNmb     = str(lineItems[0])
		factorECAL1 = float(lineItems[1])
		factorECAL2 = float(lineItems[2])
		if runNmb in postPostCorrections:
			print('Error: run number {} appears more than once'.format(runNmb))
		postPostCorrections[runNmb] = (factorECAL1, factorECAL2)
		#print('{} {} {}'.format(runNmb, factorECAL1, factorECAL2))

with open(outputFileName, 'w') as outFile:
	for runNmb in sorted(postCorrections.keys()):
		corrs = list(postCorrections[runNmb])
		if runNmb in postPostCorrections.keys():
			corrs[0] = corrs[0] * postPostCorrections[runNmb][0]
		outFile.write('{} {} {}\n'.format(runNmb, corrs[0], corrs[1]))