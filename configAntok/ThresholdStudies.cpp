#define STD_SEL
#define NO_PI_RECO
#define ECAL1
#define ECAL2

NumberOfParticles: 3
AllParticles: &allChargedParticles [1, 2, 3]

TreeName: UserEvent1000

TreeBranches:

#include "source/TreeBranches.yaml"


Constants:

#define CONSTANTS
#include "source/Selection.yaml"
#include "source/ECAL.yaml"
#include "source/Pi0FitAndOmega.yaml"
#include "source/Beam.yaml"
#include "source/RPD.yaml"
#undef CONSTANTS

CalculatedQuantities:

#define CALC_QUANTITIES
#include "source/Selection.yaml"
#include "source/ECAL.yaml"
#include "source/PhotonPair.yaml"
#include "source/Pi0FitAndOmega.yaml"
#include "source/Beam.yaml"
#include "source/RPD.yaml"
#include "source/Subsystems.yaml"
#include "source/NoPiRecoVariables.yaml"
#undef CALC_QUANTITIES

Cuts:
#include "source/Cuts.yaml"

Plots:

    - Name: "Omega Mass"
      Variable: OmegaMass
      LowerBound: 0.74
      UpperBound: 0.82
      NBins:      800

    - Name: "PreOmega Pi0PiMinusPiPlus Mass"
      Variable: Pi0PiMinusPiPlusMass
      LowerBound: 0.4
      UpperBound: 3.2
      NBins:      1400

    - Name: "Photon Pair Mass for ECAL1 photons"
      Variable: ECAL1PhotonPairMasses
      LowerBound: 0
      UpperBound: 0.6
      NBins: 300

    - Name: "Photon Pair Mass for ECAL2 photons"
      Variable: ECAL2PhotonPairMasses
      LowerBound: 0
      UpperBound: 0.6
      NBins: 300

    - Name: "Photon Pair Mass for mixed photons"
      Variable: ECALMixedPhotonPairMasses
      LowerBound: 0
      UpperBound: 0.6
      NBins: 300

    - Name: "Photon Pair Mass for all photons"
      Variable: ECALAllPhotonPairMasses
      LowerBound: 0
      UpperBound: 0.6
      NBins: 300

    - Name: "Beam Energy"
      Variable: BeamE
      LowerBound: 0
      UpperBound: 250
      NBins:      250
