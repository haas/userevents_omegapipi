#!/usr/bin/env bash
if [ "$1" != '2008' -a "$1" != '2009']; then
	echo "Missing or wrong Year: either 2008 and 2009 as first argument"
	exit 1;
fi
year="$1"
echo $year

ECAL1Values=('0.00'	'0.20'	'0.40'	'0.50'	'0.60'	'0.70'	'0.80'	'1.00'	'1.20'	'1.40')
ECAL2Values=('1.00'	'1.20'	'1.30'	'1.40'	'1.50'	'1.60'	'1.70'	'1.80'	'1.90'	'2.00'	'2.10'	'2.20'	'2.40'	'3.80')
PrecisionValues=('1e-12' '1e-11' '1e-10' '1e-09' '1e-08')


mkdir ThresholdStudies

getConfigs() {
	local name="$1"
	shift
	local array=("$@")
	for i in "${array[@]}"; do
		echo "#define ${name}_THRESHOLD $i" >> tmpThreshold
		cat ThresholdStudies.cpp >> tmpThreshold
		cpp -D YEAR="$year" tmpThreshold > "ThresholdStudies/Threshold${name}-$i.yaml" -P 
		rm tmpThreshold
	done
}

getConfigs "ECAL1" "${ECAL1Values[@]}"
getConfigs "ECAL2" "${ECAL2Values[@]}"
getConfigs "PRECISION" "${PrecisionValues[@]}"

