#define STD_SEL

NumberOfParticles: 3
AllParticles: &allChargedParticles [1, 2, 3]

TreeName: UserEvent1000

TreeBranches:

#include "source/TreeBranches.yaml"

Constants:

#define CONSTANTS
#include "source/Selection.yaml"
#include "source/ECAL.yaml"
#include "source/Pi0FitAndOmega.yaml"
#include "source/Beam.yaml"
#include "source/RPD.yaml"
#undef CONSTANTS

CalculatedQuantities:

#define CALC_QUANTITIES
#include "source/Selection.yaml"
#include "source/ECAL.yaml"
#include "source/PhotonPair.yaml"
#include "source/Pi0FitAndOmega.yaml"
#include "source/Beam.yaml"
#include "source/RPD.yaml"
#include "source/Subsystems.yaml"
#undef CALC_QUANTITIES

Cuts:
#include "source/Cuts.yaml"

OutputTree:
#include "source/OutTreeBranches.yaml"
