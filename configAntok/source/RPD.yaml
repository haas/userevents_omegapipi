#ifdef CALC_QUANTITIES
   ##################
   ## Recoil Proton #
   ##################

    ## lorentz vector of RPD proton
    - Name: RPDProtonLV
      Function:
          Name: getLorentzVec
          Px: RPD_momentumX
          Py: RPD_momentumY
          Pz: RPD_momentumZ
          E:  RPD_energyELossCorrectionRK4

    ## momentum vector of RPD proton
    - Name: RPDMomentum
      Function:
          Name: getVector3
          X: RPD_momentumX
          Y: RPD_momentumY
          Z: RPD_momentumZ

    ## absolut momentum of RPD proton
    - Name: RPDAbsMomentum
      Function:
          Name: abs
          Arg:  RPDMomentum


    ## angular distance and angular resolution between projections of outgoing system and RPD proton in plane perpenticular to beam axis
    - Name: [ rpdDeltaPhiProjection,
              rpdDeltaPhiResProjection ]
      Function:
        Name: getRpdPhi
        Method:              Projection
        BeamLorentzVec:      BeamLV
        RPDProtonLorentzVec: RPDProtonLV
        XLorentzVec:         XLV

    ## convert angular distance from rad to degrees
    - Name: rpdDeltaPhiProjection_deg
      Function:
          Name: radToDegree
          Angle: rpdDeltaPhiProjection

    ## absolut angular distance between projections of outgoing system and RPD proton in plane perpenticular to beam axis
    - Name: AbsRpdDeltaPhiProjection
      Function:
          Name: abs
          Arg: rpdDeltaPhiProjection

    ## angular distance and angular resolution between projections of outgoing system and RPD proton in plane perpenticular to beam axis only considering charged partciles
    - Name: [ rpdDeltaPhiChargedProjection,
              rpdDeltaPhiResChargedProjection ]
      Function:
        Name: getRpdPhi
        Method: Projection
        BeamLorentzVec: BeamChargedLV
        RPDProtonLorentzVec: RPDProtonLV
        XLorentzVec: Scattered_LVSum

    ## convert angular distance from rad to degrees
    - Name: rpdDeltaPhiChargedProjection_deg
      Function:
          Name: radToDegree
          Angle: rpdDeltaPhiChargedProjection

    ## absolut angular distance between projections of outgoing system and RPD proton in plane perpenticular to beam axis only considering charged partciles
    - Name: AbsRpdDeltaPhiChargedProjection
      Function:
          Name: abs
          Arg: rpdDeltaPhiChargedProjection

    ## energy difference between no correction and old correction
    - Name: RPDDeltaEnergyOld
      Function:
          Name: diff
          Minuend:    RPD_energyOldCorrection
          Subtrahend: RPD_energyNoCorrection

    ## energy difference between no correction and range correction
    - Name: RPDDeltaEnergyRange
      Function:
          Name: diff
          Minuend:    RPD_energyRangeCorrection
          Subtrahend: RPD_energyNoCorrection

    ## energy difference between no correction and stopping power correction (Euler method)
    - Name: RPDDeltaEnergyELoss
      Function:
          Name: diff
          Minuend:    RPD_energyELossCorrection
          Subtrahend: RPD_energyNoCorrection

    ## energy difference between no correction and stopping power correction (RK4 method)
    - Name: RPDDeltaEnergyELossRK
      Function:
          Name: diff
          Minuend:    RPD_energyELossCorrectionRK4
          Subtrahend: RPD_energyNoCorrection

    ## energy difference between range correction and stopping power correction (Euler method)
    - Name: RPDAbsEnergyDiffCorrectionMethods
      Function:
          Name: diff
          Minuend:    RPD_energyELossCorrection
          Subtrahend: RPD_energyRangeCorrection

    ## energy difference between range correction and stopping power correction (Euler method) divided by energy with no correction
    - Name: RPDRelEnergyDiffCorrectionMethods
      Function:
          Name: quotient
          Dividend: RPDAbsEnergyDiffCorrectionMethods
          Divisor:  RPD_energyNoCorrection

    ## energy difference between range correction and stopping power correction (Euler method) divided by energy difference between no correction and range correction
    - Name: RPDRelDeltaEnergyDiffCorrectionMethods
      Function:
          Name: quotient
          Dividend: RPDAbsEnergyDiffCorrectionMethods
          Divisor:  RPDDeltaEnergyRange

    ## energy difference between range correction and stopping power correction (RK4 method)
    - Name: RPDAbsEnergyDiffCorrectionMethodsRK
      Function:
          Name: diff
          Minuend:    RPD_energyELossCorrectionRK4
          Subtrahend: RPD_energyRangeCorrection

    ## energy difference between range correction and stopping power correction (RK4 method) divided by energy with no correction
    - Name: RPDRelEnergyDiffCorrectionMethodsRK
      Function:
          Name: quotient
          Dividend: RPDAbsEnergyDiffCorrectionMethodsRK
          Divisor:  RPD_energyNoCorrection

    ## energy difference between range correction and stopping power correction (RK4 method) divided by energy difference between no correction and range correction
    - Name: RPDRelDeltaEnergyDiffCorrectionMethodsRK
      Function:
          Name: quotient
          Dividend: RPDAbsEnergyDiffCorrectionMethodsRK
          Divisor:  RPDDeltaEnergyRange

#ifdef MC
    - Name: RPDProtonLV_MC
      Function:
          Name: getLorentzVec
          X:    Recoil_momentumMCX
          Y:    Recoil_momentumMCY
          Z:    Recoil_momentumMCZ
          M:    *ProtonMass

#endif
#endif
#ifdef PLOTS
  ########
  ## RPD #
  ########

    - Name: "RPD Proton Momentum X"
      Variable: RPD_momentumX
      LowerBound: -10
      UpperBound:  10
      NBins:      1000

    - Name: "RPD Proton Momentum Y"
      Variable: RPD_momentumY
      LowerBound: -10
      UpperBound:  10
      NBins:      1000

    - Name: "RPD Proton Momentum Z"
      Variable: RPD_momentumZ
      LowerBound:    0
      UpperBound:  100
      NBins:      1000

    - Name: "RPD Proton Absolut Momentum"
      Variable: RPDAbsMomentum
      LowerBound:    0
      UpperBound:  100
      NBins:      1000

    - Name: "RPD Number Tracks"
      Variable: RPD_numberTracks
      LowerBound:    0
      UpperBound: 20
      NBins:      20

    - Name: "RPD Delta Phi"
      Variable: rpdDeltaPhiProjection_deg
      LowerBound:   -180
      UpperBound:   180
      NBins:        720

    - Name: "RPD Delta Phi vs two different Resolutions"
      Variables: [ rpdDeltaPhiProjection_deg, rpdDeltaPhiResProjection ]
      LowerBounds: [ -180, 0.08 ]
      UpperBounds: [  180, 0.15 ]
      NBins:       [  720, 2 ]

    - Name: "RPD Energy No Correction"
      Variable: RPD_energyNoCorrection
      LowerBound:   0.9
      UpperBound:   3.9
      NBins:        1000

    - Name: "RPD Energy Old Correction"
      Variable: RPD_energyOldCorrection
      LowerBound:   0.9
      UpperBound:   3.9
      NBins:        1000

    - Name: "RPD Uncorrected Energy vs DeltaE Old Correction"
      Variables: [ RPD_energyNoCorrection, RPDDeltaEnergyOld ]
      LowerBounds: [ 0.9, -0.005 ]
      UpperBounds: [ 1.5, 0.05 ]
      NBins: [ 600, 550 ]

    - Name: "RPD Energy Range Correction"
      Variable: RPD_energyRangeCorrection
      LowerBound:   0.9
      UpperBound:   3.9
      NBins:        1000

    - Name: "RPD DeltaE Range Correction"
      Variable: RPDDeltaEnergyRange
      LowerBound:   0
      UpperBound:   0.05
      NBins:        1000

    - Name: "RPD Energy EnergyLoss Correction"
      Variable: RPD_energyELossCorrection
      LowerBound:   0.9
      UpperBound:   3.9
      NBins:        1000

    - Name: "RPD DeltaE Energy Loss Correction"
      Variable: RPDDeltaEnergyELoss
      LowerBound:   0
      UpperBound:   0.05
      NBins:        1000

    - Name: "RPD Absolut Energy Diff Range and Loss Correction"
      Variable: RPDAbsEnergyDiffCorrectionMethods
      LowerBound:   -0.002
      UpperBound:   0.002
      NBins:        1000

    - Name: "RPD Relative Energy Diff Range and Loss Correction"
      Variable: RPDRelEnergyDiffCorrectionMethods
      LowerBound:   -0.002
      UpperBound:   0.002
      NBins:        1000

    - Name: "RPD Relative Correction Diff Range and Loss Correction"
      Variable: RPDRelDeltaEnergyDiffCorrectionMethods
      LowerBound:   -0.04
      UpperBound:   0.04
      NBins:        1000

    - Name: "RPD Energy EnergyLoss Correction Heun Method"
      Variable: RPD_energyELossCorrectionHeun
      LowerBound:   0.9
      UpperBound:   3.9
      NBins:        1000

    - Name: "RPD Energy EnergyLoss Correction RK4 Method"
      Variable: RPD_energyELossCorrectionRK4
      LowerBound:   0.9
      UpperBound:   3.9
      NBins:        1000

    - Name: "RPD DeltaE Energy Loss Correction RK4"
      Variable: RPDDeltaEnergyELossRK
      LowerBound:   0
      UpperBound:   0.05
      NBins:        1000

    - Name: "RPD Absolut Energy Diff Range and Loss Correction RK4"
      Variable: RPDAbsEnergyDiffCorrectionMethodsRK
      LowerBound:   -0.002
      UpperBound:   0.002
      NBins:        1000

    - Name: "RPD Relative Energy Diff Range and Loss Correction RK4"
      Variable: RPDRelEnergyDiffCorrectionMethodsRK
      LowerBound:   -0.002
      UpperBound:   0.002
      NBins:        1000

    - Name: "RPD Relative Correction Diff Range and Loss Correction RK4"
      Variable: RPDRelDeltaEnergyDiffCorrectionMethodsRK
      LowerBound:   -0.01
      UpperBound:   0.01
      NBins:        1000

    - Name: "RPD Uncorrected Energy vs DeltaE Loss Correction RK4"
      Variables: [ RPD_energyNoCorrection, RPDDeltaEnergyELossRK ]
      LowerBounds: [ 0.9, -0.005 ]
      UpperBounds: [ 1.5, 0.05 ]
      NBins: [ 600, 550 ]
#endif
