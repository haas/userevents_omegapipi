#define NO_OMEGA_SEL
#define ECAL1
#define ECAL2

NumberOfParticles: 3
AllParticles: &allChargedParticles [1, 2, 3]

TreeName: UserEvent1000

TreeBranches:

#include "source/TreeBranches.yaml"

Constants:

#define CONSTANTS
#include "source/Selection.yaml"
#include "source/ECAL.yaml"
#include "source/Pi0FitAndOmega.yaml"
#include "source/Beam.yaml"
#include "source/RPD.yaml"
#undef CONSTANTS

CalculatedQuantities:

#define CALC_QUANTITIES
#include "source/Selection.yaml"
#include "source/ECAL.yaml"
#include "source/PhotonPair.yaml"
#include "source/Pi0FitAndOmega.yaml"
#include "source/Beam.yaml"
#include "source/RPD.yaml"
#include "source/Subsystems.yaml"
#undef CALC_QUANTITIES

    ## get variables for omega dalitz plot in peak
    - Name: [ OmegaDalitzX,
              OmegaDalitzY,
              OmegaDalitzZ,
              OmegaDalitzPhi,
              OmegaKinFactor ]
      Function:
          Name: getOmegaDalitzVariables
          Pi0LV_0:         Pi0LVKinFit_1
          Pi0LV_1:         Pi0LVKinFit_2
          ChargedPartLV_0: Scattered_LV1
          ChargedPartLV_1: Scattered_LV2
          ChargedPartLV_2: Scattered_LV3
          Charge_0:        Scattered_Charge1
          Charge_1:        Scattered_Charge2
          Charge_2:        Scattered_Charge3
          NeutralPionMass: *NeutralPionMass
          ChargedPionMass: *ChargedPionMass
          OmegaMass:       *OmegaMass
          OmegaMassWindow: *OmegaMassWindow

    ## get variables for omega dalitz plot in lower sideband
    - Name: [ OmegaDalitzX_LowerSideband,
              OmegaDalitzY_LowerSideband,
              OmegaDalitzZ_LowerSideband,
              OmegaDalitzPhi_LowerSideband,
              OmegaKinFactor_LowerSideband ]
      Function:
          Name: getOmegaDalitzVariables
          Pi0LV_0:         Pi0LVKinFit_1
          Pi0LV_1:         Pi0LVKinFit_2
          ChargedPartLV_0: Scattered_LV1
          ChargedPartLV_1: Scattered_LV2
          ChargedPartLV_2: Scattered_LV3
          Charge_0:        Scattered_Charge1
          Charge_1:        Scattered_Charge2
          Charge_2:        Scattered_Charge3
          NeutralPionMass: *NeutralPionMass
          ChargedPionMass: *ChargedPionMass
          OmegaMass:       *OmegaMassLowerSideband
          OmegaMassWindow: *OmegaMassWindowLowerSideband

    ## get variables for omega dalitz plot in upper sideband
    - Name: [ OmegaDalitzX_UpperSideband,
              OmegaDalitzY_UpperSideband,
              OmegaDalitzZ_UpperSideband,
              OmegaDalitzPhi_UpperSideband,
              OmegaKinFactor_UpperSideband ]
      Function:
          Name: getOmegaDalitzVariables
          Pi0LV_0:         Pi0LVKinFit_1
          Pi0LV_1:         Pi0LVKinFit_2
          ChargedPartLV_0: Scattered_LV1
          ChargedPartLV_1: Scattered_LV2
          ChargedPartLV_2: Scattered_LV3
          Charge_0:        Scattered_Charge1
          Charge_1:        Scattered_Charge2
          Charge_2:        Scattered_Charge3
          NeutralPionMass: *NeutralPionMass
          ChargedPionMass: *ChargedPionMass
          OmegaMass:       *OmegaMassUpperSideband
          OmegaMassWindow: *OmegaMassWindowUpperSideband

Cuts:
#include "source/Cuts.yaml"

Plots:
#define PLOTS
#undef PLOTS

  #######################
  ## omega dalitz plots #
  #######################

    - Name: "Omega DalitzXY Peak Plot"
      Variables: [ OmegaDalitzX, OmegaDalitzY ]
      LowerBounds: [  -1,  -1.1 ]
      UpperBounds: [   1,   0.8 ]
      NBins:       [  80,  76   ]

    - Name: "Omega DalitzXY Sideband Plot"
      Variables: [ OmegaDalitzX_LowerSideband, OmegaDalitzY_LowerSideband ]
      LowerBounds: [  -1,  -1.1 ]
      UpperBounds: [   1,   0.8 ]
      NBins:       [  80,  76   ]

    - Name: "Omega DalitzXY Upper Sideband Plot"
      Variables: [ OmegaDalitzX_UpperSideband, OmegaDalitzY_UpperSideband ]
      LowerBounds: [  -1,  -1.1 ]
      UpperBounds: [   1,   0.8 ]
      NBins:       [  80,  76   ]


    - Name: "Omega DalitzZPhi Peak Plot"
      Variables: [ OmegaDalitzZ, OmegaDalitzPhi ]
      LowerBounds: [   0,  0   ]
      UpperBounds: [   1,  360 ]
      NBins:       [  100, 72  ]

    - Name: "Omega DalitzZPhi Lower Sideband Plot"
      Variables: [ OmegaDalitzZ_LowerSideband, OmegaDalitzPhi_LowerSideband ]
      LowerBounds: [   0,  0   ]
      UpperBounds: [   1,  360 ]
      NBins:       [  100, 72  ]

    - Name: "Omega DalitzZPhi Upper Sideband Plot"
      Variables: [ OmegaDalitzZ_UpperSideband, OmegaDalitzPhi_UpperSideband ]
      LowerBounds: [   0,  0   ]
      UpperBounds: [   1,  360 ]
      NBins:       [  100, 72  ]

OutputTree:
    - OmegaDalitzX
    - OmegaDalitzY
    - OmegaKinFactor
    - OmegaDalitzX_LowerSideband
    - OmegaDalitzY_LowerSideband
    - OmegaKinFactor_LowerSideband
    - OmegaDalitzX_UpperSideband
    - OmegaDalitzY_UpperSideband
    - OmegaKinFactor_UpperSideband