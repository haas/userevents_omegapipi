#pragma once

// unit labels
const TString labelGeV            = "#scale[1.1]{[}GeV#scale[1.1]{]}";
const TString labelGeVoverc       = "#scale[1.1]{[}GeV/#it{c}#scale[1.1]{]}";
const TString labelGeVovercsq     = "#scale[1.1]{[}GeV/#it{c}^{2}#scale[1.1]{]}";
const TString labelGeVsq          = "#scale[1.1]{[}GeV^{2}#scale[1.1]{]}";
const TString labelGeVsqovercsq   = "#scale[1.1]{[}(GeV/#it{c})^{2}#scale[1.1]{]}";
const TString labelGeVovercsqsq   = "#scale[1.1]{[}(GeV/#it{c}^{2})^{2}#scale[1.1]{]}";
const TString labelMeV            = "#scale[1.1]{[}MeV#scale[1.1]{]}";
const TString labelMeVovercsq     = "#scale[1.1]{[}MeV/#it{c}^{2}#scale[1.1]{]}";
const TString labelns             = "#scale[1.1]{[}ns#scale[1.1]{]}";
const TString labelcm             = "#scale[1.1]{[}cm#scale[1.1]{]}";
const TString labelrad            = "#scale[1.1]{[}rad#scale[1.1]{]}";
const TString labelmrad           = "#scale[1.1]{[}mrad#scale[1.1]{]}";
const TString labelDeg            = "#scale[1.1]{[}deg#scale[1.1]{]}";

// symbols
const TString labelEcluster    = "#it{E}_{#kern[-0.07]{cluster}}";
const TString labelTcluster    = "#it{T}_{cluster}";
const TString labelTrescluster = "#it{#sigma}_{#it{T},cluster}";

const TString labelEbeam       = "#it{E}_{beam}";
const TString labelTbeam       = "#it{T}_{#kern[-0.14]{beam}}";
const TString labelxPV         = "#it{x}_{vertex}";
const TString labelyPV         = "#it{y}_{vertex}";
const TString labelzPV         = "#it{z}_{vertex}";
const TString labelPhiRPD      = "#it{#Delta#phi}_{recoil}";
const TString labeltPrime      = "#it{t'}";
const TString labelt           = "#it{t}";

const TString labelMgammagamma = "#it{m}_{#it{#gamma#gamma}}";
const TString labelEgammagamma = "#it{E}_{#it{#gamma#gamma}}";
const TString labelEgamma      = "#it{E}_{#it{#gamma}}";

const TString labelMpiz        = "#it{m}_{#it{#pi}^{0}}";
const TString labelWidthpiz    = "#it{#sigma}_{#it{#pi}^{0}}";
const TString labelEpim        = "#it{E}_{#it{#pi}^{#minus}}";
const TString labelEpiz        = "#it{E}_{#it{#pi}^{0}}";
const TString labelEpip        = "#it{E}_{#it{#pi}^{#plus}}";
const TString labelEpi         = "#it{E}_{#it{#pi}}";
const TString labelMomega      = "#it{m}_{#it{#omega}}";
const TString labelWidthomega  = "#it{#Gamma}^{nr}_{0}";

const TString labelM2pi        = "#it{m}_{#it{#pi#pi}}";
const TString labelM3pi        = "#it{m}_{#it{#pi}^{#minus}#it{#pi}^{0}#it{#pi}^{#plus}}";

const TString labelMpimpim     = "#it{m}_{#it{#pi}^{#minus}#it{#pi}^{#minus}}";
const TString labelMpimpip     = "#it{m}_{#it{#pi}^{#minus}#it{#pi}^{#plus}}";
const TString labelMpimpiz     = "#it{m}_{#it{#pi}^{#minus}#it{#pi}^{0}}";
const TString labelMpizpip     = "#it{m}_{#it{#pi}^{0}#it{#pi}^{#plus}}";
const TString labelMpizpiz     = "#it{m}_{#it{#pi}^{0}#it{#pi}^{0}}";

const TString labelMpimpizpizpip = "#it{m}_{#it{#pi}^{#minus}#it{#pi}^{0}#it{#pi}^{0}#it{#pi}^{#plus}}";
const TString labelMpimpimpizpip = "#it{m}_{#it{#pi}^{#minus}#it{#pi}^{#minus}#it{#pi}^{0}#it{#pi}^{#plus}}";
const TString labelMpimpimpizpiz = "#it{m}_{#it{#pi}^{#minus}#it{#pi}^{#minus}#it{#pi}^{0}#it{#pi}^{0}}";

const TString labelMpiomega    = "#it{m}_{#it{#font[32]{#pi}}#it{#omega}}";
const TString labelMpimomega   = "#it{m}_{#it{#font[32]{#pi}}^{#minus}#it{#omega}}";
const TString labelMpizomega   = "#it{m}_{#it{#font[32]{#pi}}^{0}#it{#omega}}";
const TString labelMpipiomega  = "#it{m}_{#it{#font[32]{#pi}}^{#minus}#it{#pi}^{0}#it{#omega}}";

const TString labelM2pimomega  = "#it{m}^{2}_{#it{#pi}^{#minus}#it{#omega}}";
const TString labelM2pizomega  = "#it{m}^{2}_{#it{#pi}^{0}#it{#omega}}";
const TString labelM2pimpiz    = "#it{m}^{2}_{#it{#pi}^{#minus}#it{#pi}^{0}}";

const TString labelCosThetaGJ = "cos #it{#vartheta}_{GJ}";
const TString labelPhiGJ      = "#it{#phi}_{GJ}";
const TString labelCosThetaHF = "cos #it{#vartheta}_{HF}";
const TString labelPhiHF      = "#it{#phi}_{HF}";
const TString labelJPC        = "#it{J#kern[-0.325]{#lower[0.065]{{}^{PC}}}}";

// axis labels
const TString axisLabelEcluster    = labelEcluster + " " + labelGeV;
const TString axisLabelTcluster    = labelTcluster + " " + labelns;
const TString axisLabelTrescluster = labelTrescluster + " " + labelns;

const TString axisLabelEbeam       = labelEbeam  + " " + labelGeV;
const TString axisLabelTbeam       = labelTbeam  + " " + labelns;
const TString axisLabelxPV         = labelxPV    + " " + labelcm;
const TString axisLabelyPV         = labelyPV    + " " + labelcm;
const TString axisLabelzPV         = labelzPV    + " " + labelcm;
const TString axisLabelPhiRPD      = labelPhiRPD + " " + labelDeg;
const TString axisLabeltPrime      = labeltPrime + " " + labelGeVsqovercsq;
const TString axisLabelt           = labelt      + " " + labelGeVsqovercsq;

const TString axisLabelMgammagamma    = labelMgammagamma + " " + labelGeVovercsq;
const TString axisLabelMgammagammaMeV = labelMgammagamma + " " + labelMeVovercsq;
const TString axisLabelEgammagamma    = labelEgammagamma + " " + labelGeV;
const TString axisLabelEgamma         = labelEgamma + " " + labelGeV;

const TString axisLabelMpiz        = labelMpiz       + " " + labelGeVovercsq;
const TString axisLabelWidthpiz    = labelWidthpiz   + " " + labelGeVovercsq;
const TString axisLabelMpizMeV     = labelMpiz       + " " + labelMeVovercsq;
const TString axisLabelWidthpizMeV = labelWidthpiz   + " " + labelMeVovercsq;
const TString axisLabelEpim        = labelEpim       + " " + labelGeV;
const TString axisLabelEpiz        = labelEpiz       + " " + labelGeV;
const TString axisLabelEpip        = labelEpip       + " " + labelGeV;
const TString axisLabelEpi         = labelEpi        + " " + labelGeV;
const TString axisLabelMomega      = labelMomega     + " " + labelMeVovercsq;
const TString axisLabelWidthomega  = labelWidthomega + " " + labelMeVovercsq;

const TString axisLabelM2pi        = labelM2pi + " " + labelGeVovercsq;
const TString axisLabelM3pi        = labelM3pi + " " + labelGeVovercsq;
const TString axisLabelM3piMeV     = labelM3pi + " " + labelMeVovercsq;

const TString axisLabelMpimpim     = labelMpimpim + " " + labelGeVovercsq;
const TString axisLabelMpimpip     = labelMpimpip + " " + labelGeVovercsq;
const TString axisLabelMpimpiz     = labelMpimpiz + " " + labelGeVovercsq;
const TString axisLabelMpizpip     = labelMpizpip + " " + labelGeVovercsq;
const TString axisLabelMpizpiz     = labelMpizpiz + " " + labelGeVovercsq;

const TString axisLabelMpimpizpizpip = labelMpimpizpizpip + " " + labelGeVovercsq;
const TString axisLabelMpimpimpizpip = labelMpimpimpizpip + " " + labelGeVovercsq;
const TString axisLabelMpimpimpizpiz = labelMpimpimpizpiz + " " + labelGeVovercsq;

const TString axisLabelMpiomega    = labelMpiomega + " " + labelGeVovercsq;
const TString axisLabelMpimomega   = labelMpimomega + " " + labelGeVovercsq;
const TString axisLabelMpizomega   = labelMpizomega + " " + labelGeVovercsq;
const TString axisLabelMpipiomega  = labelMpipiomega + " " + labelGeVovercsq;

const TString axisLabelM2pimomega  = labelM2pimomega + " " + labelGeVovercsqsq;
const TString axisLabelM2pizomega  = labelM2pizomega + " " + labelGeVovercsqsq;
const TString axisLabelM2pimpiz    = labelM2pimpiz + " " + labelGeVovercsqsq;

const TString axisLabelCosThetaGJ = labelCosThetaGJ;
const TString axisLabelPhiGJ      = labelPhiGJ  + " " + labelDeg;
const TString axisLabelCosThetaHF = labelCosThetaHF;
const TString axisLabelPhiHF      = labelPhiHF  + " " + labelDeg;
