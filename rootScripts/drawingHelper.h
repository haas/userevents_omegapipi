#include <string>
#include <vector>
#include <limits>
#include <cassert>

#include "TBox.h"
#include "TH1.h"
#include "TLatex.h"
#include "TROOT.h"
#include "TColor.h"
#include "TString.h"
#include "TCanvas.h"
#include "TArrow.h"
#include "TClass.h"

double
findMaxY(TH1& hist)  // finds maximum y-value in histogram taking into account error bars
{
	double max = std::numeric_limits<double>::min();
	for (size_t i = 1; i <= (size_t)hist.GetNbinsX(); ++i) {
		const double binVal = hist.GetBinContent(i) + hist.GetBinError(i);
		if (binVal > max)
			max = binVal;
	}
	return max;
}


void
redrawFrame(TVirtualPad* pad)  // redraws histogram frame to mask overprinting by histogram content
{
	assert(pad);
	pad->RedrawAxis();
	pad->Update();
	TBox* b = new TBox();
	b->SetLineColor(gStyle->GetFrameLineColor());
	b->SetLineStyle(gStyle->GetFrameLineStyle());
	b->SetLineWidth(gStyle->GetFrameLineWidth());
	//cout << "box " << b->GetLineColor() << ", " << b->GetLineStyle() << ", " << b->GetLineWidth() << endl;
	b->SetFillStyle(0);
	double xMin = pad->GetUxmin();
	double xMax = pad->GetUxmax();
	double yMin = pad->GetUymin();
	double yMax = pad->GetUymax();
	if (pad->GetLogx() == 1) {
		xMin = pow(10, xMin);
		xMax = pow(10, xMax);
	}
	if (pad->GetLogy() == 1) {
		yMin = pow(10, yMin);
		yMax = pow(10, yMax);
	}
	//cout << "pad " << xMin << ", " << xMax << ", " << yMin << ", " << yMax << endl;
	b->DrawBox(xMin, yMin, xMax, yMax);
}


void
makeTitle(bool    prelim,
		  TString toprightLabel,
          bool    header = true)
{
	if (header) {
		const double x = 1 - gPad->GetRightMargin();
		const double y = 1 - gPad->GetTopMargin() + 0.015;
		TLatex *lt = new TLatex(x, y, toprightLabel);
		lt->SetNDC(true);
		//lt->SetTextFont(132);
		lt->SetTextSize(43);
		lt->SetTextAlign(31);  // bottom-right
		if( toprightLabel != "" )
			lt->Draw();
	}

	if ( prelim ){
		// put "Preliminary" label
		const double x = 0.5 - gPad->GetRightMargin()/2 + gPad->GetLeftMargin()/2;
		const double y = 0.5 - gPad->GetTopMargin()/2 + gPad->GetBottomMargin()/2;
		TLatex* pre = new TLatex(x, y, "Preliminary");
		pre->SetNDC(true);
		pre->SetTextAlign(22);  // center-center
		pre->SetTextSize(130);
		pre->SetTextAngle(45);
		pre->SetTextColorAlpha(kGray+1, 0.7);
		pre->Draw();
	}
}

void
makeTitle2D(bool    prelim,
            TString toprightLabel,
            bool    header = true)
{
	if (header) {
		const double x = 1 - gPad->GetRightMargin();
		const double y = 1 - gPad->GetTopMargin() + 0.019;
		TLatex *lt = new TLatex(x, y, toprightLabel);
		lt->SetNDC(true);
		//lt->SetTextFont(132);
		lt->SetTextSize(43);
		lt->SetTextAlign(31);  // bottom-right
		if( toprightLabel != "")
			lt->Draw();
	}

	if ( prelim ){
		//// put "Preliminary" label
		//const double x = gPad->GetLeftMargin();
		//const double y = 1 - gPad->GetTopMargin() + 0.015;
		//TLatex* pre = new TLatex(x, y, "#font[132]{Preliminary}");
		//pre->SetNDC(true);
		//pre->SetTextAlign(11);  // bottom-left
		//pre->SetTextSize(43);
		//pre->Draw();

		// put "Preliminary" label
		const double x = 0.5 - gPad->GetRightMargin()/2 + gPad->GetLeftMargin()/2;
		const double y = 0.5 - gPad->GetTopMargin()/2 + gPad->GetBottomMargin()/2;
		TLatex* pre = new TLatex(x, y, "Preliminary");
		pre->SetNDC(true);
		pre->SetTextAlign(22);  // center-center
		pre->SetTextSize(130);
		pre->SetTextAngle(45);
		pre->SetTextColorAlpha(kGray+1, 0.7);
		pre->Draw();
	}
}

void
printPlot(TVirtualPad*   canv,
          const TString& folder,
          const TString& fileName,
          const TString& topRightLabel  = "",
          const bool     prelim       = false,
		  const bool     plot2D       = false)
{
	canv->cd();
	if (!plot2D) makeTitle(prelim, topRightLabel, true);
	if (plot2D) {
		makeTitle2D(prelim, topRightLabel, true);
		canv->SetRightMargin (canv->GetRightMargin()*canv->GetWw()/800.0 + (canv->GetWw()-800.0)/canv->GetWw());
		canv->SetLeftMargin  (0.160/canv->GetWw()*800);
		//canv->SetRightMargin (0.1);
	}
	gPad->SetFillStyle(4000);  // make all canvasses transparent
	redrawFrame(gPad);

	canv->Print(folder + fileName + ".pdf");
	canv->Print(folder + fileName + ".root");
}



void
putPercentage(double  percentage,
              TString orientation)
{
	TLatex *lt = new TLatex;
	lt->SetNDC(true);
	lt->SetTextFont(gStyle->GetTextFont());
	lt->SetTextSize(gStyle->GetTextSize());

	char text[20];
	if (percentage >= 0.05)
		sprintf(text, "%.1f%%", percentage);
	else
		sprintf(text, "< 0.1%%");

	if (orientation == 'l')
		lt->DrawLatex(0.80, 0.87, text);  // right
	if (orientation == 'r')
		lt->DrawLatex(0.19, 0.87, text);  // left
}

// beautification functors

// virtual base class for beautification functor
class postProcessFunctor {
public:
	virtual void operator()(TH1* h = 0, Color_t color = kRed) = 0;

	static bool disablePostProcess;
};

// beautification functor that draws shaded vertical band
class postProcessVertLines : public postProcessFunctor {
public:

	postProcessVertLines()
		: _color       (kGray + 2),
		  _style       (2),  // dashed
		  _width       (3),
		  _yMaxFraction(1.05 / 1.0)
	{ }

	postProcessVertLines* setXPos(size_t num, ...)
	{
		_xPositions.clear();
		_xPositions.resize(num, 0);
		va_list arguments;
		va_start(arguments, num);
		for (size_t i = 0; i < num; ++i)
			_xPositions[i] = va_arg(arguments, double);
		va_end(arguments);
		return this;
	}

	virtual void operator()(TH1* h = 0, Color_t color = kGray+2)
	{
		_color = color;
		if (disablePostProcess or not h)
			return;
		TLine* l = new TLine();
		l->SetLineColor(_color);
		l->SetLineStyle(_style);
		if (h->GetDimension()==1) {
			for (size_t i = 0; i < _xPositions.size(); ++i)
				l->DrawLine(_xPositions[i], 0, _xPositions[i], h->GetMaximum());
		} else if (h->GetDimension()==2) {
			for (size_t i = 0; i < _xPositions.size(); ++i)
				l->DrawLine(_xPositions[i], h->GetYaxis()->GetXmin(), _xPositions[i], h->GetYaxis()->GetXmax());
		}
		//h->Draw("SAME");
		gPad->RedrawAxis();
	}

	virtual void operator()(TH1* h, const double yMin, const double yMax, Color_t color = kGray+2)
	{
		_color = color;
		if (disablePostProcess or not h)
			return;
		TLine* l = new TLine();
		l->SetLineColor(_color);
		l->SetLineStyle(_style);
		for (size_t i = 0; i < _xPositions.size(); ++i)
			l->DrawLine(_xPositions[i], yMin, _xPositions[i], yMax);
		//h->Draw("SAME");
		gPad->RedrawAxis();
	}

	Color_t        _color;
	Style_t        _style;
	Width_t        _width;
	double         _yMaxFraction;
	vector<double> _xPositions;
};

// beautification functor that draws shaded horizontal band
class postProcessHorzLines : public postProcessFunctor {
public:

	postProcessHorzLines()
		: _color       (kGray + 2),
		  _style       (2),  // dashed
		  _width       (3)
	{ }

	postProcessHorzLines* setYPos(size_t num, ...)
	{
		_yPositions.clear();
		_yPositions.resize(num, 0);
		va_list arguments;
		va_start(arguments, num);
		for (size_t i = 0; i < num; ++i)
			_yPositions[i] = va_arg(arguments, double);
		va_end(arguments);
		return this;
	}

	virtual void operator()(TH1* h = 0, Color_t color = kGray+2)
	{
		_color = color;
		if (disablePostProcess or not h)
			return;
		TLine* l = new TLine();
		l->SetLineColor(_color);
		l->SetLineStyle(_style);
		for (size_t i = 0; i < _yPositions.size(); ++i) {
			l->DrawLine(h->GetXaxis()->GetXmin(), _yPositions[i], h->GetXaxis()->GetXmax(), _yPositions[i]);
		}
		//h->Draw("SAME");
		gPad->RedrawAxis();
		gPad->RedrawAxis("F");
	}

	virtual void operator()(TH1* h, const double xMin, const double xMax, Color_t color = kGray+2)
	{
		_color = color;
		if (disablePostProcess or not h)
			return;
		TLine* l = new TLine();
		l->SetLineColor(_color);
		l->SetLineStyle(_style);
		for (size_t i = 0; i < _yPositions.size(); ++i) {
			l->DrawLine(xMin, _yPositions[i], xMax, _yPositions[i]);
		}
		//h->Draw("SAME");
		gPad->RedrawAxis();
	}

	Color_t        _color;
	Style_t        _style;
	Width_t        _width;
	vector<double> _yPositions;
};

// beautification functor that shades bins in given intervals
class postProcessShadeBins : public postProcessFunctor {
public:

	postProcessShadeBins()
		: _color(kGray)
	{ }

	postProcessShadeBins* setXIntervals(size_t num, ...)
	{
		assert(num % 2 == 0);
		_xIntervals.clear();
		_xIntervals.resize(num / 2, make_pair(0., 0.));
		va_list arguments;
		va_start(arguments, num);
		for (size_t i = 0; i < num / 2; ++i) {
			const double xMin = va_arg(arguments, double);
			const double xMax = va_arg(arguments, double);
			_xIntervals[i] = make_pair(xMin, xMax);
		}
		va_end(arguments);
		return this;
	}

	virtual void operator()(TH1* h = 0)
	{
		if (disablePostProcess or not h)
			return;
		for (size_t i = 0; i < _xIntervals.size(); ++i) {
			const double xMin = _xIntervals[i].first;
			const double xMax = _xIntervals[i].second;
			int minBin = h->FindBin(xMin);
			int maxBin = h->FindBin(xMax);
			// make sure xMin is lower bin edge and xMax upper
			if (h->GetBinCenter(minBin) < xMin)
				++minBin;
			if (h->GetBinCenter(maxBin) > xMax)
				--maxBin;
			// cout << "!!! HERE " << i << ", " << h->GetXaxis()->GetBinLowEdge(minBin) << ", "
			//      << h->GetXaxis()->GetBinUpEdge(maxBin) << ", " << minBin << ", " << maxBin << endl;
			TH1* hShaded = (TH1*)h->DrawClone("SAME BAR");
			hShaded->SetFillColor(_color);
			hShaded->GetXaxis()->SetRange(minBin, maxBin);
		}
	}

	Color_t                       _color;
	vector<pair<double, double> > _xIntervals;
};


// beautification functor that puts a label
class postProcessLabel : public postProcessFunctor {
public:

	postProcessLabel()
		: _label(""),
		  _xPos(0),
		  _yPos(0),
		  _color(kBlack)
	{ }

	postProcessLabel* set(const TString& label,
	                      const double  xPos,
	                      const double  yPos,
	                      const Color_t color = kBlack)
	{
		_label = label;
		_xPos  = xPos;
		_yPos  = yPos;
		_color = color;
		return this;
	}

	virtual void operator()(TH1* h = 0, Color_t color = kGray+2)
	{
		if (disablePostProcess or not h)
			return;
		TLatex *lt = new TLatex;
		lt->SetNDC(true);
		lt->SetTextFont(132);
		lt->SetTextSize(gStyle->GetTextSize());
		lt->SetTextColor(_color);
		lt->DrawLatex(_xPos, _yPos, _label);
	}

	TString _label;
	double  _xPos;
	double  _yPos;
	Color_t _color;
};

void
putVertArrows(TH1*           hist,
              const size_t   nmbArrows,
              const double*  xVals,
              const double*  yMinVals,
              const double*  yMaxVals,
              const TString* labels,
              const double   labelXOffset,
              const char*    labelAligns  = 0,
              const double   labelSize    = 0,
              const double   labelYOffset = 0)
{
	TArrow* arrow = new TArrow();
	//arrow->SetLineStyle(2);
	arrow->SetFillColor(kBlack);
	arrow->SetArrowSize(0.02);
	arrow->SetAngle(30);
	arrow->SetOption("|>");
	TLatex* label = new TLatex();
	label->SetTextFont(gStyle->GetTextFont());
	if (labelSize == 0)
		label->SetTextSize(gStyle->GetTextSize());
	else
		label->SetTextSize(labelSize);
	for (size_t i = 0; i < nmbArrows; ++i) {
		arrow->DrawArrow(xVals[i], yMaxVals[i], xVals[i], yMinVals[i]);
		double xLabel = xVals[i];
		if (not labelAligns or (labelAligns[i] == 'l')) {
			label->SetTextAlign(12);  // left center
			xLabel += labelXOffset;
		} else if (labelAligns[i] == 'e') {
			label->SetTextAlign(11);  // left bottom
			xLabel += labelXOffset;
		} else if (labelAligns[i] == 'r') {
			label->SetTextAlign(32);  // right center
			xLabel -= labelXOffset;
		} else if (labelAligns[i] == 'i') {
			label->SetTextAlign(31);  // right bottom
			xLabel -= labelXOffset;
		} else if (labelAligns[i] == 'c') {
			label->SetTextAlign(21);  // center bottom
			//xLabel -= labelXOffset;
		} else
			return;
		label->DrawLatex(xLabel, yMaxVals[i] + labelYOffset, labels[i]);
	}
}


void
getXYRange(TH1*    hist,
           double& xRange,
           double& yRange)
{
	// there is no GetUserRange(); sigh
	xRange  =  hist->GetXaxis()->GetBinUpEdge (hist->GetXaxis()->GetLast ())
	         - hist->GetXaxis()->GetBinLowEdge(hist->GetXaxis()->GetFirst());
  const string className = hist->IsA()->GetName();
  yRange = 0;
  if (className.substr(0, 3) == "TH1")
	  yRange = hist->GetMaximum() - hist->GetMinimum();
  else if (className.substr(0, 3) == "TH2")
	  yRange =  hist->GetYaxis()->GetBinUpEdge (hist->GetYaxis()->GetLast ())
		        - hist->GetYaxis()->GetBinLowEdge(hist->GetYaxis()->GetFirst());
}


void
putVertArrowsAt(TH1*           hist,
                const size_t   nmbArrows,
                const double*  xVals,
                const double*  yVals,
                const TString* labels,
                const char*    labelAligns      = 0,
                const double*  extraArrowLength = 0)
{
	double xRange, yRange;
	getXYRange(hist, xRange, yRange);
	double yMinVals[nmbArrows];
	double yMaxVals[nmbArrows];
	for (size_t i = 0; i < nmbArrows; ++i) {
		yMinVals[i] = yVals[i]    + 0.02 * yRange;
		yMaxVals[i] = yMinVals[i] + 0.06 * yRange;
		if (extraArrowLength)
			yMaxVals[i] += extraArrowLength[i] * yRange;
	}
	putVertArrows(hist, nmbArrows, xVals, yMinVals, yMaxVals, labels, 0.015 * xRange, labelAligns);
}


void
putVertArrowsAtTopLabel(TH1*           hist,
                        const size_t   nmbArrows,
                        const double*  xVals,
                        const double*  yVals,
                        const TString* labels,
                        const char*    labelAligns = 0,
                        const double   labelSize   = 0)
{
	double xRange, yRange;
	getXYRange(hist, xRange, yRange);
	double yMinVals[nmbArrows];
	double yMaxVals[nmbArrows];
	const double yMin = hist->GetYaxis()->GetBinLowEdge(hist->GetYaxis()->GetFirst());
	for (size_t i = 0; i < nmbArrows; ++i) {
		yMinVals[i] = yVals[i] + 0.02 * yRange;
		yMaxVals[i] = yMin     + 0.93 * yRange;
	}
	putVertArrows(hist, nmbArrows, xVals, yMinVals, yMaxVals, labels,
	              -0.015 * xRange, labelAligns, labelSize, 0.02 * yRange);
}


void
putHorRightArrowsAt(TH1*           hist,
                    const size_t   nmbArrows,
                    const double*  xVals,
                    const double*  yVals,
                    const TString* labels,
                    const double   xMinOffset = 0)
{
	double xRange, yRange;
	getXYRange(hist, xRange, yRange);
  TArrow* arrow = new TArrow();
  //arrow->SetLineStyle(2);
  arrow->SetFillColor(kBlack);
  arrow->SetArrowSize(0.02);
  arrow->SetAngle(30);
  arrow->SetOption("|>");
  TLatex* label = new TLatex();
  label->SetTextFont(gStyle->GetTextFont());
  label->SetTextSize(gStyle->GetTextSize());
  label->SetTextAlign(11);  // left bottom
  const double xMin = *min_element(xVals, xVals + nmbArrows) - (0.02 + 0.06 + xMinOffset) * xRange;
  for (size_t i = 0; i < nmbArrows; ++i) {
	  const double xMax = xVals[i] - 0.02 * xRange;
	  arrow->DrawArrow(xMin, yVals[i], xMax, yVals[i]);
	  label->DrawLatex(xMin - 0.06 * xRange, yVals[i] + 0.025 * yRange, labels[i]);
  }
}

void
makeChannel(const TString&     channel,
            //const std::string& range,
            //const TString&     var_type,
            const TString&     orientation,
            int                line       = 0,
            double             xOffset    = 0,
            double             yOffset    = 0,
            Color_t            labelColor = kBlack)
{
	const double leftAlignX       = 0.19 + xOffset;
	const double rightAlignX      = 0.37 + xOffset;
	double       firstLineY       = 0.87 + yOffset;
	double       firstLineHeight  = 0.050 * (972. / gPad->GetWh());  // scale, if window height != 1000 (canvas height 972)
	const double otherLinesHeight = 0.050 * (972. / gPad->GetWh());  // scale, if window height != 1000 (canvas height 972)
	TLatex *lt = new TLatex;
	lt->SetNDC(true);
	lt->SetTextFont(gStyle->GetTextFont());
	lt->SetTextSize(gStyle->GetTextSize());
	lt->SetTextColor(labelColor);
	if (line == 0) {  // print wave label in first line
		if (orientation == 'r')
			lt->DrawLatex(rightAlignX, firstLineY, channel);  // right
		if (orientation == 'l')
			lt->DrawLatex(leftAlignX,  firstLineY, channel);  // left
	};
	if (line >= 1000) {  // put wave label above frame and ranges in top line
		const Short_t align = lt->GetTextAlign();
		lt->SetTextAlign(31);  // bottom-right
		const double x = 1 - gPad->GetRightMargin();
		const double y = 1 - gPad->GetTopMargin() + 0.03;
		lt->DrawLatex(x, y, channel);
		lt->SetTextAlign(align);
		line           -= 1000;
		firstLineHeight = 0;
	}
	/* 
	const std::pair<double, double> valRange = parseRange(range);
	char subtitle[200];
	if (var_type == "t1")
		sprintf(subtitle, "%.1f < #it{t'} < %.1f (GeV/#it{c})^{2}",                        valRange.first, valRange.second);
	else if (var_type == "t2")
		sprintf(subtitle, "%.2f < #it{t'} < %.2f (GeV/#it{c})^{2}",                        valRange.first, valRange.second);
	else if (var_type == "t")
		sprintf(subtitle, "%.3f < #it{t'} < %.3f (GeV/#it{c})^{2}",                        valRange.first, valRange.second);
	else if (var_type == "m")
		sprintf(subtitle, "%.1f < #it{m}_{3#it{#pi}} < %.1f GeV/#it{c}^{2}",               valRange.first, valRange.second);
	else if (var_type == "f")
		sprintf(subtitle, "%.2f < #it{m}_{3#it{#pi}} < %.2f GeV/#it{c}^{2}",               valRange.first, valRange.second);
	else if (var_type == "d")
		sprintf(subtitle, "%.3f < #it{m}_{3#it{#pi}} < %.3f GeV/#it{c}^{2}",               valRange.first, valRange.second);
	else if (var_type == "i")
		sprintf(subtitle, "%.2f < #it{m_{#pi^{#minus}#pi^{#plus}}} < %.2f GeV/#it{c}^{2}", valRange.first, valRange.second);
	else if (var_type == "x")  // "misuse" 'range' argument as text field
		sprintf(subtitle, "%s", range.c_str());
	if (orientation == 'r')
		lt->DrawLatex(rightAlignX, firstLineY - firstLineHeight - line * otherLinesHeight, subtitle);  // right
	if (orientation == 'l')
		lt->DrawLatex(leftAlignX,  firstLineY - firstLineHeight - line * otherLinesHeight, subtitle);  // left*/

	delete lt;
}

TString getYearLabel(const int year) {
	switch (year) {
		case 2008: return "2008";
		case 2009: return "2009";
		default: return "";
	}
}

void updateAxis(TH1* histo, const TString xTitle, const double xMin, const double xMax, const TString yTitle, const double yMin = 0, const double yMax = 0, const bool is2D = false, const bool isLogY = false) {
	if (xTitle != "") histo->GetXaxis()->SetTitle(xTitle);
    histo->GetXaxis()->CenterTitle();
    if (xMin != xMax) histo->GetXaxis()->SetRangeUser(xMin, xMax);
    if (yTitle != "") histo->GetYaxis()->SetTitle(yTitle);
    histo->GetYaxis()->CenterTitle();
	if (yMin != yMax) {
		if (!is2D) {
			//histo->SetAxisRange(yMin, ((double)((int)(yMax * 20))+1) / 20, "Y");
			histo->SetMaximum(yMax);
			if (yMin != 0) histo->SetMinimum(yMin);
		}
		if (is2D)  histo->GetYaxis()->SetRangeUser(yMin, yMax);
	} else if (yMax == 0 && !is2D && !isLogY) {
		double maximum = histo->GetMaximum()*1.05;
		const double base = TMath::Power(10.0, (int)(TMath::Log10(maximum)));

		if (maximum/base < 2.5) {
			maximum = (TMath::Ceil(maximum/base*10+0.2))/10*base;
		} else if (maximum/base < 5.5) {
			maximum = (TMath::Ceil(maximum/base*5+0.2))/5*base;
		} else {
			maximum = (TMath::Ceil(maximum/base*2+0.2))/2*base;
		}

		//histo->SetMaximum( (double)((int)( maximum / base)) * base );
		histo->SetMaximum(maximum);
	} else if (yMax == 0 && !is2D && isLogY) {
		double maximum = histo->GetMaximum()*1.4	;
		histo->SetMaximum( TMath::Ceil(maximum) );
	} else if (is2D && !isLogY) {
		double maximum = histo->GetMaximum()*1.05;
		const double base = TMath::Power(10.0, (int)(TMath::Log10(maximum)));

		if (maximum/base < 2.5) {
			maximum = (TMath::Ceil(maximum/base*10+0.2))/10*base;
		} else if (maximum/base < 5.5) {
			maximum = (TMath::Ceil(maximum/base*5+0.2))/5*base;
		} else {
			maximum = (TMath::Ceil(maximum/base*2+0.2))/2*base;
		}
		//histo->SetMaximum( (double)((int)( maximum / base)) * base );
		histo->SetMaximum(maximum);
	} else if (is2D && isLogY) {
		double maximum = histo->GetMaximum();
		const double base = TMath::Power(10.0, (int)(TMath::Log10(maximum)));
		maximum = (int(maximum/base) + 1) * base; 
		histo->SetMaximum(maximum);
	}
}

TPaletteAxis* GetPaletteAxis(TH1* histo, double labelSize = 0.05) {
	TGaxis::SetExponentOffset(-50., 0., "Z");
    const double xMin = histo->GetXaxis()->GetBinLowEdge(histo->GetXaxis()->GetFirst());
    const double xMax = histo->GetXaxis()->GetBinUpEdge( histo->GetXaxis()->GetLast());
    const double yMin = histo->GetYaxis()->GetBinLowEdge(histo->GetYaxis()->GetFirst());
    const double yMax = histo->GetYaxis()->GetBinUpEdge( histo->GetYaxis()->GetLast());
	TPaletteAxis *palette = new TPaletteAxis(xMax + (xMax - xMin)*0.007, yMin, xMax + (xMax - xMin)*0.045, yMax, histo);
	//TPaletteAxis *palette = (TPaletteAxis*)histo->GetListOfFunctions()->FindObject("palette");
	//palette->SetX2NDC(0.93);
	//palette->SetLineWidth(4);
	//palette->SetLineColor(1);
	//palette->SetBorderSize(3);
    //palette->Draw();

	TAxis* axis = histo->GetZaxis();
	//axis->SetNdivisions(2*1000000 + (axis->GetNdiv()%1000000));
	//axis->SetMaxDigits(2);
	axis->SetTickSize(0.01);
	axis->SetLabelSize(labelSize);
	//axis->SetLabelColor(0);
	axis->SetLabelOffset(0.005);
	axis->SetLabelFont(122);
	//axis->SetNdivisions(504);

	return palette;
}
enum COMPASSLabelPosition {topRight, topLeft, bottomRight};

void
drawCompassLabel(TVirtualPad* pad, COMPASSLabelPosition positionIndex = topRight)  // redraws histogram frame to mask overprinting by histogram content
{
		double x,y;
		int alignPosition;
		switch (positionIndex) {
			case topRight: {
				x = 1 - pad->GetRightMargin();
				y = 1 - pad->GetTopMargin() + 0.015;
				alignPosition = 31;
				break;
			}
			case topLeft: {
				x = pad->GetLeftMargin();
				y = 1 - pad->GetTopMargin() + 0.015;
				alignPosition = 11;
				break;
			}
			case bottomRight: {
				x = 1 - pad->GetRightMargin() - 0.01;
				y = 1 - pad->GetTopMargin() - 0.015;
				alignPosition = 33;
				break;
			}
		}
		TLatex *lt = new TLatex(x, y, "COMPASS");
		lt->SetNDC(true);
		lt->SetTextAlign(alignPosition);
		lt->SetTextSize(43);
		lt->Draw();
}