# userevents_omegapipi #

This repository includes costum software used to analyse the diffractive $`\pi^{-} + p \to \omega\pi^{-}\pi^{0} + p`$ reaction.
The content in each subdirectory is explained in the following.

## MCConfigs ##

Configurations used to generate the flat phase-space Monte-Carlo. For this the `detectorSimulation` scripts included in [ROOTPWATools](https://gitlab.cern.ch/compass/hadron/ROOTPWAtools.git) were used. These scripts handle the full MC chain, where first events are drawn from flat phase-space, propagated through the detector using COMGEANT, and reconstructed with CORAL and phast.

## PWA ##

- keyfiles used for [ROOTPWA](https://gitlab.cern.ch/compass/hadron/ROOTPWA.git) which represent all partial-waves that are included in the PWD.
- Script to generate the $`\omega(782)`$ mass shape used in the decay amplitudes.

## configAntok ##

Config files used for [antok](https://gitlab.cern.ch/compass/hadron/antok.git) which performs the final event selection on top of phast.

## rootScripts ##

Collection of plotting scripts used during the event selection.

## userPhast ##

`UserEvent1000` and helper classes used for the event selection.
