#include <algorithm>
#include <map>

#include "SelectionHelper.h"

std::map<int, std::vector<int>> SelectionHelper::_badRunList = std::map<int, std::vector<int>>();

SelectionHelper::SelectionHelper(TTree& tree)
	: _phast(Phast::Ref())
{
	clear();
	_phast.h_file->cd();
	_phast.HistFileDir("UserEvent");

	_hStatistics = new TH1D("statistics", "statistics", 20, -0.5, 19.5);

	// read bad runs from file given as userflag
	for (int i = 0; i < Phast::Ref().NTextUserFlag(); ++i) {
		std::string userFlag = Phast::Ref().TextUserFlag(i);
		if (userFlag.find("badRunList:") != std::string::npos) {  // found flag
			std::string badRunFileName;
			badRunFileName = userFlag.replace(0, 11, "");  // remove "badRunList:"
			std::ifstream badRunFile;
			badRunFile.open(badRunFileName);
			if (not badRunFile) {
				std::cerr << "Could not open 'badRunList' file at '" << badRunFileName << "." << std::endl;
			}
			int badRunNumber;
			int badSpillNumber;
			while (badRunFile >> badRunNumber >> badSpillNumber) {
				_badRunList[badRunNumber].push_back(badSpillNumber);
				std::cout << "Bad Run " << badRunNumber << ": " << badSpillNumber << std::endl;
			}
		}
	}

	tree.Branch("Selection_runNumber",     &_runNumber,     "runNumber/I");
	tree.Branch("Selection_spillNumber",   &_spillNumber,   "spillNumber/I");
	tree.Branch("Selection_eventInSpill",  &_eventInSpill,  "eventInSpill/I");
	tree.Branch("Selection_triggerMask",   &_triggerMask,   "triggerMask/I");
	tree.Branch("Selection_masterTrigger", &_masterTrigger, "masterTrigger/I");
	tree.Branch("Selection_DT0",           &_DT0,           "DT0/I");
	tree.Branch("Selection_tcsPhase",      &_tcsPhase,      "tcsPhase/D");
	tree.Branch("Selection_timeInSpill",   &_timeInSpill,   "timeInSpill/D");
	tree.Branch("Selection_isAcceptedMC",  &_isAcceptedMC,  "isAcceptedMC/I");
}


void
SelectionHelper::clear()
{
	const double failValue = std::numeric_limits<double>::quiet_NaN();
	_runNumber     = 0;
	_spillNumber   = 0;
	_eventInSpill  = 0;
	_triggerMask   = 0;
	_masterTrigger = 0;
	_DT0           = 0;
	_tcsPhase      = failValue;
	_timeInSpill   = failValue;
	_isAcceptedMC  = 0;
}


void
SelectionHelper::fill(const PaEvent& event)
{
	_runNumber    = event.RunNum();
	_spillNumber  = event.SpillNum();
	_eventInSpill = event.EvInSpill();
	_DT0          = isDT0();
	if (event.IsMC()) {
		//TODO clarify whether rpd.IsDT0() is also working in simulation
		if (_DT0) {
			_triggerMask   = 1;
			_masterTrigger = 1;
		} else {
			_triggerMask   = 0;
			_masterTrigger = 0;
		}
	} else {
		_triggerMask   = event.TrigMask() & 0xfff;
		_masterTrigger = event.MasterTriggerMask();
	}
	_tcsPhase    = event.TCSphase();
	_timeInSpill = event.TimeInSpill();
}


void
SelectionHelper::handleMCEvent(TTree&         tree,
                               const bool     accepted,
                               const PaEvent& event)
{
	fill(event);
	setIsAcceptedMC(accepted);
	tree.Fill();
	Phast::Ref().h_file = tree.GetCurrentFile();
}


bool
SelectionHelper::ensureChargeConservation(const PaEvent&            event,
                                          const int                 primVertexIndex,
                                          const PaParticle&         beamParticle,
                                          const vector<PaParticle>& scatteredParticles)
{
	// Require that charge of each particle is equal to its charge in the primary vertex
	const PaTrack& beamTrack = event.vTrack(beamParticle.iTrack());
	const PaTPar&  beamTPar  = beamParticle.ParInVtx(primVertexIndex);
	if (beamParticle.Q() != beamTrack.Q() or beamParticle.Q() != beamTPar.Q()) {
		return false;
	}
	for (const auto& scatteredParticle : scatteredParticles) {
		const PaTrack& scatteredTrack = event.vTrack(scatteredParticle.iTrack());
		const PaTPar&  scatteredTPar  = scatteredParticle.ParInVtx(primVertexIndex);
		if (scatteredParticle.Q() != scatteredTrack.Q() or scatteredParticle.Q() != scatteredTPar.Q()) {
			return false;
		}
	}
	// Check charge conservation
	int scatteredChargeSum = 0;
	for (const auto& scatteredParticle : scatteredParticles) {
		scatteredChargeSum += scatteredParticle.Q();
	}
	return scatteredChargeSum == beamParticle.Q();
}


std::string
SelectionHelper::getYear()
{
	std::string year;
	for (int i = 0; i < Phast::Ref().NTextUserFlag(); ++i) {
		std::string userFlag = Phast::Ref().TextUserFlag(i);
		if (userFlag.find("year:") != std::string::npos) {  // found flag
			if (year.empty()) {  // first appearance
				year = userFlag.replace(0, 5, "");  // remove "year:"
			} else {
				throw std::invalid_argument("Multiple years given in 'phast -T year:<####>' call! Aborting...");
			}
		}
	}
	if (year.size() == 0) {
		throw std::invalid_argument("No year statement given. Give analyzed year as '-T year:<####>'. Aborting...");
	}
	return year;
}


bool
SelectionHelper::isBadRun(const PaEvent& event)
{
	if (_badRunList.empty()) return false;
	bool badRun = false;
	const int runNumber   = event.RunNum();
	const int spillNumber = event.SpillNum();
	if (_badRunList.count(runNumber) != 0) {
		if (_badRunList[runNumber].empty() or _badRunList[runNumber][0] < 1) badRun = true;
		else if (*find(_badRunList[runNumber].begin(), _badRunList[runNumber].end(), spillNumber) == spillNumber) badRun = true;
	}
	return badRun;
}