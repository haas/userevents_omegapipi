/*
 * Likelihood.cpp
 *
 *  Created on: Jun 23, 2016
 *      Author: ga34liw
 */

#include "PaCEDARLikelihood.h"

#include <stdio.h>

#ifdef __INTEL_OFFLOAD
#pragma omp declare target
FILE* mystdout = stdout;
#pragma omp end declare target
#else
FILE* mystdout = stdout;
#endif




#define QRT(X) (X)*(X)

namespace Cedar {


	void Cedar::Likelihood::setDeltaRefractionIndex(const double value){
		_DrefractionIndex = value;
		_cherenkovAnglePion     = acos(1.0/(1.0+_DrefractionIndex) * sqrt(1+QRT(_pionMass/_momentum)) );
		_cherenkovAngleKaon     = acos(1.0/(1.0+_DrefractionIndex) * sqrt(1+QRT(_kaonMass/_momentum)) );
		_cherenkovAngleElectron = acos(1.0/(1.0+_DrefractionIndex) * sqrt(1+QRT(_electronMass/_momentum)) );
		_cherenkovAngleProton   = acos(1.0/(1.0+_DrefractionIndex) * sqrt(1+QRT(_protonMass/_momentum)) );
		for( int i = 0; i < 8; ++i ) calcPmtPosDxy(i);
	}

} /* namespace CEDAR */

const double Cedar::Likelihood::_pionMass = 0.13957018; // PDG 2014
const double Cedar::Likelihood::_kaonMass =  0.493677; // PDG 2014
const double Cedar::Likelihood::_electronMass = 0.000510998928; // PDG 2014
const double Cedar::Likelihood::_protonMass = 0.938272046; // PDG 2014
const double Cedar::Likelihood::_momentum =  190 ; // fixed to 190 GeV TODO

Cedar::Likelihood::Likelihood(){
	// lazy way to initialize everything to zero
	double initParameters[128] = {0.0};
	setParameters(initParameters);
}

Cedar::LikelihoodResult Cedar::Likelihood::operator ()(const double dX, const double dY, const char hitMask)const {
	Cedar::LikelihoodResult result;
	double likePion = 1.0;
	double likeKaon = 1.0;
	double likeElectron = 1.0;
	double likeProton = 1.0;
#if defined(_OPENMP)
#pragma omp simd reduction(*:likePion,likeKaon,likeElectron, likeProton) // aligned(_PmtPosDx,_PmtPosDy,_PmtPosTheta,_PmtProbAmplitude,_PmtProbPeakWidth,_PmtProbNorm,_PmtProbSlope:64)
#endif
	for( int iPmt = 0; iPmt < 8; ++iPmt){
		const double distance = sqrt(QRT(dX-_PmtPosDx[iPmt]) + QRT(dY-_PmtPosDy[iPmt]));
		const double phi = asin( (dX*_PmtPosDy[iPmt] - dY*_PmtPosDx[iPmt])
								/(sqrt(QRT(dX) + QRT(dY) + QRT(_PmtPosTheta[iPmt]) - 2*(dX*_PmtPosDx[iPmt] + dY*_PmtPosDy[iPmt])) * _PmtPosTheta[iPmt])
								);
		const double probPion     = calcProb(iPmt, distance, phi, _cherenkovAnglePion, hitMask);
		const double probKaon     = calcProb(iPmt, distance, phi, _cherenkovAngleKaon, hitMask);
		const double probProton   = calcProb(iPmt, distance, phi, _cherenkovAngleProton, hitMask);
		const double probElectron = calcProb(iPmt, distance, phi, _cherenkovAngleElectron, hitMask);
		likePion     *= probPion;
		likeKaon     *= probKaon;
		likeElectron *= probElectron;
		likeProton   *= probProton ;
	}
	result.likePion = likePion;
	result.likeKaon = likeKaon;
	result.likeElectron = likeElectron;
	result.likeProton = likeProton;
	return result;
}



void Cedar::Likelihood::calcPmtPosDxy(const int iPmt){
	_PmtPosTheta[iPmt] = _cherenkovAngleKaon + _PmtPosDeltaTheta[iPmt];
	_PmtPosDx[iPmt] = _PmtPosTheta[iPmt] * cos(_PmtPosPhi[iPmt]);
	_PmtPosDy[iPmt] = _PmtPosTheta[iPmt] * sin(_PmtPosPhi[iPmt]);
}



void Cedar::Likelihood::printParameters(FILE* stream)const{

	if( stream == nullptr){
		stream = mystdout;
		printf("Likelihood parameter:\n");
	}
	for(int i = 0; i < 8; ++i){
		fprintf(stream, "%20.8g", _PmtPosDeltaTheta[i]);
		fprintf(stream, "%20.8g", _PmtPosPhi[i]);
		fprintf(stream, "%20.8g", _PmtProbAmplitude[i]);
		fprintf(stream, "%20.8g", _PmtProbAmplitudeChange[i]);
		fprintf(stream, "%20.8g", _PmtProbPeakWidth[i]);
		fprintf(stream, "%20.8g", _PmtProbPeakWidthChange[i]);
		fprintf(stream, "%20.8g", _PmtProbSigma[i]);
		fprintf(stream, "%20.8g", _PmtProbSigmaChange[i]);
		fprintf(stream, "%20.8g", _PmtProbBkgAmpl[i]);
//		fprintf(stream, "%20.8g", _PmtProbBkgPos[i]);
//		fprintf(stream, "%20.8g", _PmtProbBkgWidth[i]);
		fprintf(stream, "\n");
	}
	fprintf(stream, "%20.8g\n", _DrefractionIndex);
}

void Cedar::Likelihood::getParameters(double* para) const{
	int j = 0;
	for (int i = 0; i < 8; ++i) {
		j = 0;
		para[(j++) * 8 + i] = getPmtPosDeltaTheta(      i);
		para[(j++) * 8 + i] = getPmtPosPhi(             i);
		para[(j++) * 8 + i] = getPmtProbAmplitude(      i);
		para[(j++) * 8 + i] = getPmtProbAmplitudeChange(i);
		para[(j++) * 8 + i] = getPmtProbPeakWidth(      i);
		para[(j++) * 8 + i] = getPmtProbPeakWidthChange(i);
		para[(j++) * 8 + i] = getPmtProbSigma(          i);
		para[(j++) * 8 + i] = getPmtProbSigmaChange(    i);
		para[(j++) * 8 + i] = getPmtProbBkgAmpl(        i);
//		para[(j++) * 8 + i] = getPmtProbBkgPos(         i);
//		para[(j++) * 8 + i] = getPmtProbBkgWidth(       i);
	}
	para[j*8+0] = getDeltaRefractionIndex();
}

void Cedar::Likelihood::setParameters(const double* para) {
	int j = 0;
	for (int i = 0; i < 8; ++i) {
		j = 0;
		setPmtPosDeltaTheta(      i, para[(j++) * 8 + i]);
		setPmtPosPhi(             i, para[(j++) * 8 + i]);
		setPmtProbAmplitude(      i, para[(j++) * 8 + i]);
		setPmtProbAmplitudeChange(i, para[(j++) * 8 + i]);
		setPmtProbPeakWidth(      i, para[(j++) * 8 + i]);
		setPmtProbPeakWidthChange(i, para[(j++) * 8 + i]);
		setPmtProbSigma(          i, para[(j++) * 8 + i]);
		setPmtProbSigmaChange(    i, para[(j++) * 8 + i]);
		setPmtProbBkgAmpl(        i, para[(j++) * 8 + i]);
//		setPmtProbBkgPos(         i, para[(j++) * 8 + i]);
//		setPmtProbBkgWidth(       i, para[(j++) * 8 + i]);
	}
	setDeltaRefractionIndex(para[j*8+0]);
}

