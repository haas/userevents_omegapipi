#include <algorithm>
#include <cmath>
#include <numeric>
#include <sstream>

#include "ECALDefinition.h"


ECALDefinition::ECALDefinition(TTree& tree)
	: _phast      (Phast::Ref()),
	  _ECAL1      (PaSetup::Ref().Calorimeter(0)),
	  _ECAL2      (PaSetup::Ref().Calorimeter(1)),
	  _ECAL1FrontZ(getECALZFrontCoords(1)),
	  _ECAL2FrontZ(getECALZFrontCoords(2))
{
	clear();
	_phast.h_file->cd();

	tree.Branch("ECAL_numberClusters",             &_numberClusters, "numberClusters/I");
	tree.Branch("ECAL_clusterECALIndices",         &_clusterECALIndices);
	tree.Branch("ECAL_clusterXs",                  &_clusterXs);
	tree.Branch("ECAL_clusterYs",                  &_clusterYs);
	tree.Branch("ECAL_clusterZs",                  &_clusterZs);
	tree.Branch("ECAL_clusterXVariances",          &_clusterXVariances);
	tree.Branch("ECAL_clusterYVariances",          &_clusterYVariances);
	tree.Branch("ECAL_clusterZVariances",          &_clusterZVariances);
	tree.Branch("ECAL_clusterEnergies",            &_clusterEnergies);
	tree.Branch("ECAL_clusterEnergyVariances",     &_clusterEnergyVariances);
	tree.Branch("ECAL_clusterTimes",               &_clusterTimes);
	tree.Branch("ECAL_clusterTimeResolutions",     &_clusterTimeResolutions);
	tree.Branch("ECAL_clusterSizes",               &_clusterSizes);
	tree.Branch("ECAL_clusterCellIndices",         &_clusterCellIndices);
	tree.Branch("ECAL_clusterXInCells",            &_clusterXInCells);
	tree.Branch("ECAL_clusterYInCells",            &_clusterYInCells);
	tree.Branch("ECAL_clusterTracksMinDistance",   &_clusterTracksMinDistance);
	tree.Branch("ECAL_clusterTracksMinDistZFirst", &_clusterTracksMinDistZFirst);
	tree.Branch("ECAL_smallestTrackMinDistance",   &_smallestTrackMinDistance, "smallestTrackMinDistance/D");
}


void
ECALDefinition::clear()
{
	_numberClusters = 0;
	_clusterECALIndices.clear();
	_clusterXs.clear();
	_clusterYs.clear();
	_clusterZs.clear();
	_clusterXVariances.clear();
	_clusterYVariances.clear();
	_clusterZVariances.clear();
	_clusterEnergies.clear();
	_clusterEnergyVariances.clear();
	_clusterTimes.clear();
	_clusterTimeResolutions.clear();
	_clusterSizes.clear();
	_clusterCellIndices.clear();
	_clusterXInCells.clear();
	_clusterYInCells.clear();
	_clusterTracksMinDistance.clear();
	_clusterTracksMinDistZFirst.clear();
	_smallestTrackMinDistance = std::numeric_limits<double>::infinity();
}


void
ECALDefinition::fill(const PaEvent& event)
{
	getTrackCoordsAtEcals(event);

	_numberClusters = (int)getNumberNeutralClusters(event);
	_clusterECALIndices.resize        (_numberClusters);
	_clusterXs.resize                 (_numberClusters);
	_clusterYs.resize                 (_numberClusters);
	_clusterZs.resize                 (_numberClusters);
	_clusterXVariances.resize         (_numberClusters);
	_clusterYVariances.resize         (_numberClusters);
	_clusterZVariances.resize         (_numberClusters);
	_clusterEnergies.resize           (_numberClusters);
	_clusterEnergyVariances.resize    (_numberClusters);
	_clusterTimes.resize              (_numberClusters);
	_clusterTimeResolutions.resize    (_numberClusters);
	_clusterSizes.resize              (_numberClusters);
	_clusterCellIndices.resize        (_numberClusters);
	_clusterXInCells.resize           (_numberClusters);
	_clusterYInCells.resize           (_numberClusters);
	_clusterTracksMinDistance.resize  (_numberClusters);
	_clusterTracksMinDistZFirst.resize(_numberClusters);
	_smallestTrackMinDistance = std::numeric_limits<double>::infinity();

	for (size_t i = 0; i < _neutralParticleIndices.size(); ++i) {
		const PaParticle& particle = event.vParticle(_neutralParticleIndices[i]);
		const PaCaloClus& cluster  = event.vCaloClus(particle.iCalorim(0));
		double cellCenterX = 0;
		double cellCenterY = 0;
		double chargedTrackMinDistance   = std::numeric_limits<double>::infinity();
		double chargedTrackMinDistZFirst = std::numeric_limits<double>::infinity();
		if (_ECAL1.IsMyCluster(cluster)) {
			// ECAL 1
			_clusterECALIndices[i] = 1;
			// Note: PaCalorimeter::iCell() finds more cells than
			// PaCalorimeter::iCell() since in the latter the search runs
			// only over those cells that are associated to the cluster
			_clusterCellIndices[i] = _ECAL1.iCell(cluster.X(), cluster.Y(), cellCenterX, cellCenterY);
			// calculate closest distance of the cluster to any of the charged tracks for ECAL 1
			for (size_t j = 0; j < _tracksCoordsECAL1.size(); ++j) {
				const double dist = (_tracksCoordsECAL1[j] - TVector2(cluster.X(), cluster.Y())).Mod();
				if (dist < chargedTrackMinDistance) {
					chargedTrackMinDistance   = dist;
					chargedTrackMinDistZFirst = _tracksZFirst[j];
				}
			}
		} else if (_ECAL2.IsMyCluster(cluster)) {
			// ECAL 2
			_clusterECALIndices[i] = 2;
			_clusterCellIndices[i] = _ECAL2.iCell(cluster.X(), cluster.Y(), cellCenterX, cellCenterY);
			// calculate closest distance of the cluster to any of the charged tracks for ECAL 2
			for (size_t j = 0; j < _tracksCoordsECAL2.size(); ++j) {
				const double dist = (_tracksCoordsECAL2[j] - TVector2(cluster.X(), cluster.Y())).Mod();
				if (dist < chargedTrackMinDistance) {
					chargedTrackMinDistance   = dist;
					chargedTrackMinDistZFirst = _tracksZFirst[j];
				}
			}
		}
		_clusterXs                 [i] = cluster.X();
		_clusterYs                 [i] = cluster.Y();
		_clusterZs                 [i] = cluster.Z();
		_clusterXVariances         [i] = cluster.Cov()[0];
		_clusterYVariances         [i] = cluster.Cov()[2];
		_clusterZVariances         [i] = cluster.Cov()[5];
		_clusterEnergies           [i] = cluster.E();
		const double Eerr = cluster.Eerr();
		_clusterEnergyVariances    [i] = Eerr * Eerr;
		_clusterTimes              [i] = cluster.Time();
		_clusterTimeResolutions    [i] = cluster.SigmaT();
		_clusterSizes              [i] = cluster.Size();
		_clusterXInCells           [i] = cluster.X() - cellCenterX;
		_clusterYInCells           [i] = cluster.Y() - cellCenterY;
		_clusterTracksMinDistance  [i] = chargedTrackMinDistance;
		_clusterTracksMinDistZFirst[i] = chargedTrackMinDistZFirst;
		_smallestTrackMinDistance = std::min(chargedTrackMinDistance, _smallestTrackMinDistance);

	}  // loop over _neutralParticleIndices
}


const std::vector<int>&
ECALDefinition::getNeutralParticleIndices(const PaEvent& event)
{
	_neutralParticleIndices.clear();
	_neutralParticleIndices.reserve(event.NCaloClus());
	for (int i = 0; i < event.NParticle(); ++i) {
		const PaParticle& particle = event.vParticle(i);
		// Neutral particle
		if (particle.Q() != 0) {
			continue;
		}

		// No track associated to neutral particle
		if (particle.iTrack() != -1) {
			continue;
		}

		// Only one ECAL cluster allowed for photons
		if (particle.NCalorim() != 1) {
			continue;
		}
		const PaCaloClus& cluster = event.vCaloClus(particle.iCalorim(0));

		// Ensure that cluster is in an ECAL
		if (cluster.CalorimName()[0] != 'E') {
			continue;
		}

		_neutralParticleIndices.push_back(i);
	}

	return _neutralParticleIndices;
}


// calculates transverse position charged tracks at upstream faces of ECALs
void
ECALDefinition::getTrackCoordsAtEcals(const PaEvent& event)
{
	_tracksZFirst.clear();
	_tracksZFirst.reserve(event.NParticle());
	_tracksCoordsECAL1.clear();
	_tracksCoordsECAL1.reserve(event.NParticle());
	_tracksCoordsECAL2.clear();
	_tracksCoordsECAL2.reserve(event.NParticle());
	// iterate over all charged particles with a track to find closest hit
	for (int i = 0; i < event.NParticle(); ++i) {
		const PaParticle& particle = event.vParticle(i);
		// charged particle
		if (particle.Q() == 0) {
			continue;
		}
		// particle has track
		if (particle.iTrack() < 0) {
			continue;
		}
		const PaTrack& track = event.vTrack(particle.iTrack());
		PaTPar trackParEcal1, trackParEcal2;
		// extrapolate track to z-positions of upstream faces of ECAL 1 and ECAL 2
		if (    track.Extrapolate(_ECAL1FrontZ, trackParEcal1, false)
		    and track.Extrapolate(_ECAL2FrontZ, trackParEcal2, false)) {
			// extrapolations were successful
			_tracksCoordsECAL1.push_back(TVector2(trackParEcal1.X(), trackParEcal1.Y()));
			_tracksCoordsECAL2.push_back(TVector2(trackParEcal2.X(), trackParEcal2.Y()));
			_tracksZFirst.push_back(track.ZFirst());
		}
	}
}


double
ECALDefinition::getECALZFrontCoords(const int ECALIndex) const
{
	if (ECALIndex != 1 and ECALIndex != 2) {
		throw std::runtime_error("There is no ECAL with index " + std::to_string(ECALIndex) + ". Only 1 or 2 allowed. Aborting...");
	}
	const PaCalorimeter& ECAL = (ECALIndex == 1) ? _ECAL1 : _ECAL2;
	vector<double> cellOffsets(ECAL.vCaloCellType().size(), std::numeric_limits<double>::quiet_NaN());
	for (const auto& cell : ECAL.vCalorimCell()) {
		const int iType = cell.iType();
		assert(0 <= iType and iType < (int)cellOffsets.size());
		if (std::isnan(cellOffsets[iType])) {
			cellOffsets[iType] = cell.Position().Z();
		} else {
			assert(cellOffsets[iType] == cell.Position().Z());  // make sure offsets for the same cell type are all equal
		}
	}
	vector<double> ECALZs;
	for (const auto& cellType : ECAL.vCaloCellType()) {
		const int iType = cellType.MyIndex();
		assert(0 <= iType and iType < (int)cellOffsets.size());
		ECALZs.push_back(ECAL.Position().Z() + cellOffsets[iType] - cellType.Size().Z() / 2);
	}
	assert(std::equal(ECALZs.begin() + 1, ECALZs.end(), ECALZs.begin()));  // ensure that all z-coordinates of the cells are equal
	std::cout << "Using z position of " << ECALZs[0] << " cm for upstream face of ECAL " << ECALIndex << "." << std::endl;
	return ECALZs[0];
}
