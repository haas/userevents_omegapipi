#include "RecoilDefinition.h"


RecoilDefinition::RecoilDefinition(TTree& tree)
	: _phast(Phast::Ref())
{
	clear();
	_phast.h_file->cd();

	tree.Branch("Recoil_momentumMCX", &_momentumMCX, "momentumMCX/D");
	tree.Branch("Recoil_momentumMCY", &_momentumMCY, "momentumMCY/D");
	tree.Branch("Recoil_momentumMCZ", &_momentumMCZ, "momentumMCZ/D");
}


void
RecoilDefinition::clear()
{
	const double failValue = std::numeric_limits<double>::quiet_NaN();
	_momentumMCX = failValue;
	_momentumMCY = failValue;
	_momentumMCZ = failValue;
}


void
RecoilDefinition::fillMC(const PaEvent&    event,
                         const PaMCvertex& primVertex)
{
	const PaMCtrack& track = getRecoilTrackMC(event, primVertex);
	_momentumMCX = track.P(0);
	_momentumMCY = track.P(1);
	_momentumMCZ = track.P(2);
}


const PaMCtrack&
RecoilDefinition::getRecoilTrackMC(const PaEvent&    event,
                                   const PaMCvertex& primVertex)
{
	for (int i = 0; i < primVertex.NMCtrack(); ++i) {
		const PaMCtrack& track = event.vMCtrack(primVertex.iMCtrack(i));
		if (track.IsPileup() or track.IsBeam()) {
			continue;
		}
		if (track.Pid() == MC_PID_PROTON) {
			return track;
		}
	}
	throw std::runtime_error("No recoil MC track found! Aborting...");
}
