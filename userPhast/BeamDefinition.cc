#include <algorithm>
#include <cctype>
#include <sstream>

#include "BeamDefinition.h"
#include "SelectionHelper.h"


BeamDefinition::BeamDefinition(TTree& tree)
	: _phast                  (Phast::Ref()),
	  _cedar                  (),
	  _cedarMCHelper          (NULL),
	  _ignoreMissingCedarCalib(getIgnoreMissingCedarCalib()),
	  _beamTruePID            (getTrueCEDARBeamPid())
{
	clear();
	_phast.h_file->cd();

	// Read CEDAR calibration files
	std::stringstream cedarCalibFilePath;
	const string year = SelectionHelper::getYear();
	cedarCalibFilePath << PaUtils::PhastHome() << "/user/cedar_calib/" << year << "/calib" << year << ".root";
	std::cout << "\nReading CEDAR calibration from file '" << cedarCalibFilePath.str() << "'." << std::endl;
	_cedar.readCalibration(cedarCalibFilePath.str());

	// Read CEDAR efficiency maps
	std::stringstream cedarMapsFilePath;
	cedarMapsFilePath << PaUtils::PhastHome() << "/user/cedar_calib/" << year << "/maps" << year << ".root";
	const char* likelihoodRatioThresholdKaonMC = "4.0";
	const char* likelihoodRatioThresholdPionMC = "0.0";
	std::cout << "Reading CEDAR efficiency maps from file '" << cedarMapsFilePath.str() << "'." << std::endl;
	_cedarMCHelper = new CEDARHelper::MCHelper(cedarMapsFilePath.str().c_str(), likelihoodRatioThresholdKaonMC, likelihoodRatioThresholdPionMC);
	assert(_cedarMCHelper);

	tree.Branch("Beam_gradX",                &_gradX,                "gradX/D");
	tree.Branch("Beam_gradY",                &_gradY,                "gradY/D");
	tree.Branch("Beam_time",                 &_time,                 "time/D");
	tree.Branch("Beam_firstZ",               &_firstZ,               "firstZ/D");
	tree.Branch("Beam_lastZ",                &_lastZ,                "lastZ/D");
	tree.Branch("Beam_minimalZ",             &_minimalZ,             "minimalZ/D");
	tree.Branch("Beam_maximumZ",             &_maximumZ,             "maximumZ/D");
	tree.Branch("Beam_chi2red",              &_chi2red,              "chi2red/D");
	tree.Branch("Beam_charge",               &_charge,               "charge/I");
	tree.Branch("Beam_pid",                  &_pid,                  "pid/I");
	tree.Branch("Beam_majorityCEDAR1",       &_majorityCEDAR1,       "majorityCEDAR1/I");
	tree.Branch("Beam_majorityCEDAR2",       &_majorityCEDAR2,       "majorityCEDAR2/I");
	tree.Branch("Beam_likelihoodPionCEDAR1", &_likelihoodPionCEDAR1, "likelihoodPionCEDAR1/D");
	tree.Branch("Beam_likelihoodPionCEDAR2", &_likelihoodPionCEDAR2, "likelihoodPionCEDAR2/D");
	tree.Branch("Beam_likelihoodKaonCEDAR1", &_likelihoodKaonCEDAR1, "likelihoodKaonCEDAR1/D");
	tree.Branch("Beam_likelihoodKaonCEDAR2", &_likelihoodKaonCEDAR2, "likelihoodKaonCEDAR2/D");
	tree.Branch("Beam_momentumMCX",          &_momentumMCX,          "momentumMCX/D");
	tree.Branch("Beam_momentumMCY",          &_momentumMCY,          "momentumMCY/D");
	tree.Branch("Beam_momentumMCZ",          &_momentumMCZ,          "momentumMCZ/D");
}


BeamDefinition::~BeamDefinition()
{
	if (_cedarMCHelper) {
		delete _cedarMCHelper;
	}
}


void
BeamDefinition::clear()
{
	const double failValue = std::numeric_limits<double>::quiet_NaN();
	_gradX                = failValue;
	_gradY                = failValue;
	_time                 = failValue;
	_firstZ               = failValue;
	_lastZ                = failValue;
	_minimalZ             = failValue;
	_maximumZ             = failValue;
	_chi2red              = failValue;
	_charge               = -777;  // fail value used in PHAST
	_pid                  = -1;    // fail value used in PHAST
	_majorityCEDAR1       = 0;
	_majorityCEDAR2       = 0;
	_likelihoodPionCEDAR1 = -1;
	_likelihoodPionCEDAR2 = -1;
	_likelihoodKaonCEDAR1 = -1;
	_likelihoodKaonCEDAR2 = -1;
	_momentumMCX          = failValue;
	_momentumMCY          = failValue;
	_momentumMCZ          = failValue;
}


void
BeamDefinition::fill(const PaEvent& event)
{
	const PaVertex&   primVertex      = event.vVertex(event.iBestPrimaryVertex());
	const int         primVertexIndex = primVertex.MyIndex();
	const PaParticle& beamParticle    = getBeamParticle(event, primVertex);
	const PaTrack&    beamTrack       = event.vTrack(beamParticle.iTrack());
	const PaTPar&     beamTPar        = beamParticle.ParInVtx(primVertexIndex);

	_gradX    = beamTPar.dXdZ();
	_gradY    = beamTPar.dYdZ();
	_time     = beamTrack.MeanTime();
	_firstZ   = beamTrack.ZFirst();
	_lastZ    = beamTrack.ZLast();
	_minimalZ = beamTrack.Zmin();
	_maximumZ = beamTrack.Zmax();
	_chi2red  = beamTrack.Chi2tot() / beamTrack.Ndf();
	_charge   = beamParticle.Q();
	_pid      = beamParticle.PID();

	// Get CEDAR information; code adapted from Stefan Wallner's event selection
	// see BeamParticleWithCEDARPid::setCEDARInformation()
	// in /nfs/freenas/tuph/e18/project/compass/analysis/swallner/EventSelection/phast_userevent/TreeFiller.cc
	if (not event.IsMC()) {
		// Real-data event
		try {
			_cedar.initEvent(event, beamTPar, false);
		} catch (const PaCEDARDetector::NoCalibrationError& e) {
			if (_ignoreMissingCedarCalib)
				return;
			throw e;
		}
		_cedar.Getmajority(_majorityCEDAR1, _majorityCEDAR2);
		try {
			const PaCEDARDetector::Likelihoods likelihoods = _cedar.getLikelihoods();
			_likelihoodKaonCEDAR1 = likelihoods.first.likeKaon;
			_likelihoodPionCEDAR1 = likelihoods.first.likePion;
			_likelihoodKaonCEDAR2 = likelihoods.second.likeKaon;
			_likelihoodPionCEDAR2 = likelihoods.second.likePion;
		} catch (const PaCEDARDetector::NoCalibrationError& e) {
			if (_ignoreMissingCedarCalib)
				return;
			throw e;
		}
	} else {
		// MC event; simulate CEDAR using efficiency maps (electrons and muons are identified as pions)
		_cedar.initMCEvent(event, beamTPar);
		double dXdZCEDAR1, dYdZCEDAR1, dXdZCEDAR2, dYdZCEDAR2;  // inclinations of beam particle w.r.t. z axis
		_cedar.getDXDYcor(dXdZCEDAR1, dYdZCEDAR1, dXdZCEDAR2, dYdZCEDAR2);
		if (beamTrack.iMCtrack() > -1) {  // beam track is associated with an MC track
			const int pidMC = _cedarMCHelper->predict(_beamTruePID, dXdZCEDAR1, dYdZCEDAR1);
			switch (pidMC) {
				case CEDARHelper::MCHelper::kaon :
					_majorityCEDAR1       = 8;
					_majorityCEDAR2       = 8;
					_likelihoodKaonCEDAR1 = 1;
					_likelihoodPionCEDAR1 = 1e-10;
					_likelihoodKaonCEDAR2 = 1;
					_likelihoodPionCEDAR2 = 1e-10;
					break;
				case CEDARHelper::MCHelper::pion :
					_majorityCEDAR1       = 0;
					_majorityCEDAR2       = 0;
					_likelihoodKaonCEDAR1 = 1e-10;
					_likelihoodPionCEDAR1 = 1;
					_likelihoodKaonCEDAR2 = 1e-10;
					_likelihoodPionCEDAR2 = 1;
					break;
				case CEDARHelper::MCHelper::noPID :
					_majorityCEDAR1       = 4;
					_majorityCEDAR2       = 4;
					_likelihoodKaonCEDAR1 = 1;
					_likelihoodPionCEDAR1 = 1;
					_likelihoodKaonCEDAR2 = 1;
					_likelihoodPionCEDAR2 = 1;
					break;
				default :
					std::cerr << "Unknown MC CEDAR beam particle ID = " << pidMC << " from CEDAR simulation." << std::endl;
			}
		}
	}
}


void
BeamDefinition::fillMC(const PaEvent&    event,
                       const PaMCvertex& primVertex)
{
	const PaMCtrack& beamTrack = getBeamTrackMC(event, primVertex);
	// in MC beam particle goes the "wrong" direction
	_momentumMCX = -1 * beamTrack.P(0);
	_momentumMCY = -1 * beamTrack.P(1);
	_momentumMCZ = -1 * beamTrack.P(2);
}


const PaMCtrack&
BeamDefinition::getBeamTrackMC(const PaEvent&    event,
                               const PaMCvertex& primVertex)
{
	for (int i = 0; i < primVertex.NMCtrack(); ++i) {
		const PaMCtrack& track = event.vMCtrack(primVertex.iMCtrack(i));
		if (track.IsBeam() and track.Pid() == MC_PID_BEAM and not track.IsPileup()) {
			return track;
		}
	}
	throw std::runtime_error("No MC beam track found! Aborting...");
}


bool
BeamDefinition::getIgnoreMissingCedarCalib()
{
	for (int i = 0; i < Phast::Ref().NTextUserFlag(); ++i) {
		const std::string flag = Phast::Ref().TextUserFlag(i);
		if (flag == "ignoreMissingCEDARCalibration") {
			std::cout << "\nIgnoring missing CEDAR calibrations." << std::endl;
			return true;
		}
	}
	return false;
}


CEDARHelper::MCHelper::pid
BeamDefinition::getTrueCEDARBeamPid()
{
	std::string beamParticleName;
	for (int i = 0; i < Phast::Ref().NTextUserFlag(); ++i) {
		std::string userFlag = Phast::Ref().TextUserFlag(i);
		if (userFlag.find("MCBeamParticle:") != std::string::npos) {  // found flag
			if (beamParticleName.empty()) {  // first appearance
				beamParticleName = userFlag.replace(0, 15, "");  // remove "MCBeamParticle:"
			} else {
				throw std::invalid_argument("Multiple beam particle names given in 'phast -T MCBeamParticle:<name>' call! Aborting...");
			}
		}
	}
	CEDARHelper::MCHelper::pid beamTruePID;
	if (beamParticleName.empty()) {
		std::cout << "\nNo MC CEDAR beam particle ID was defined via '-T MCBeamParticle:<name>' "
		          << "with name = {pion, kaon}. Assuming pion beam.";
		beamTruePID = CEDARHelper::MCHelper::pion;
	} else {
		std::transform(beamParticleName.begin(), beamParticleName.end(), beamParticleName.begin(),
		               [](unsigned char c){ return std::tolower(c); });
		if (beamParticleName == "pion") {
			beamTruePID = CEDARHelper::MCHelper::pion;
		} else if (beamParticleName == "kaon") {
			beamTruePID = CEDARHelper::MCHelper::kaon;
		} else {
			throw std::invalid_argument("Unkown MC CEDAR beam particle ID '" + beamParticleName
				+ "' was defined via '-T MCBeamParticle:<name>'. Aborting...");
		}
	}
	std::cout << "\nThe MC CEDAR beam particle ID is set to " << beamTruePID << "." << std::endl;
	return beamTruePID;
}
