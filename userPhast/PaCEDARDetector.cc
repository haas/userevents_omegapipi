#include "PaCEDARDetector.h"
#include <string>
#include <sstream>
#include <iostream>
#include "TString.h"
#include "TMath.h"
#include "TLatex.h"
#include "TLine.h"
#include "TDatime.h"
#include "TTree.h"
#include <fstream>
#include <assert.h>
#include <stdexcept>
#include <set>


namespace {
	void readDCSTree(TFile* dcsfile, const char* treename, std::map<int, double>& out_valuelist, const double offset = 0.0);
	double getValueFromMap( const int timestamp, const std::map<int,double>& value_map, const bool interpolate = false);
}

//------------------------------------------------------------------------------
// constructor
//------------------------------------------------------------------------------
PaCEDARDetector::PaCEDARDetector() {
	isCalibrationObject = false;
	currentCalib_ = NULL;

	nSigma_ = 3;
	cedarInfo[0].resize(4);
	cedarInfo[1].resize(4);

	// default remap
	for( int i = 0; i < 8; ++i){
		remap_[0][i] = i;
		remap_[1][i] = i;
	}


}

// read ROOT calibration file
void PaCEDARDetector::readCalibration(const std::string& fileName){

	double CEDARTiltAvgX[2], CEDARTiltAvgY[2];
	CEDARTiltAvgX[0] = CEDARTiltAvgX[1] = CEDARTiltAvgY[0] = CEDARTiltAvgY[1] = 0.0;
	int nRuns = 0;

	// read per-run calibration
	try {
		PaCEDARDetectorCalibration::PerRunDataHandler calibPerRun(fileName.c_str());
		for( int i_run = 0; i_run < calibPerRun.getEntries(); ++i_run){
			calibPerRun.getEntry(i_run);
			const int run = calibPerRun.getRunNumber();
			if( perRunCalib_.count(run) != 0){
				std::cerr << "Run " << run << " loaded twice from calibration file" << std::endl;
				exit(1);
			}
			perRunCalib_[run] = calibPerRun;

			++nRuns;
			CEDARTiltAvgX[0] += calibPerRun.getInclinationCalibX(0);
			CEDARTiltAvgX[1] += calibPerRun.getInclinationCalibX(1);
			CEDARTiltAvgY[0] += calibPerRun.getInclinationCalibY(0);
			CEDARTiltAvgY[1] += calibPerRun.getInclinationCalibY(1);
		}
		calibPerRun.close();
	} catch(std::exception& ){
		std::cerr << "CEDAR: No per run calibration found!" << std::endl;
	}

	if(nRuns > 0){
		MCCalib_.setInclinationCalibX(0, CEDARTiltAvgX[0]/nRuns);
		MCCalib_.setInclinationCalibX(1, CEDARTiltAvgX[1]/nRuns);
		MCCalib_.setInclinationCalibY(0, CEDARTiltAvgY[0]/nRuns);
		MCCalib_.setInclinationCalibY(1, CEDARTiltAvgY[1]/nRuns);
	}

	// read PMT remapping
	PaCEDARDetectorCalibration::ConstantDataHandler constCalib(fileName.c_str());
	for (int i_CEDAR = 0; i_CEDAR < 2; ++i_CEDAR) {
		for (int i_PMT = 0; i_PMT < 8; ++i_PMT) {
			remap_[i_CEDAR][i_PMT] = constCalib.getRemap(i_CEDAR, i_PMT);
		}

		transpMat_[i_CEDAR].resize(65);
		for( int i = 0; i < 65; ++i){
			transpMat_[i_CEDAR][i] = constCalib.getTransportMatrixElem(i_CEDAR, i);
		}
	}



}
//------------------------------------------------------------------------------
// convert Digits into hitmaps only needed on Phast environment
//------------------------------------------------------------------------------
#ifdef PHAST_HOME
void PaCEDARDetector::_readDigits(const PaEvent &event,  bool forceRaw, TH1D* timeHist[2][8], TH1D* timeOfRunHist) {
	bool foundCsDigits = false;
	hitMask_[0] = 0;
	hitMask_[1] = 0;

	if (timeOfRunHist) {
		// get timestamp of event
		int Y, M, D, h, m, s;
		event.Date(Y, M, D, h, m, s);
	timeOfRunHist->Fill(TDatime(Y,M,D,h,m,s).Convert());
	}
	// get calibration
	try{
		const int runnbr = (perRunCalib_.find(0) != perRunCalib_.end())? 0 : event.RunNum(); // default run is 0 if no run-by-run calibration was loaded
		// set calibration constants for this run
		currentCalib_ = &perRunCalib_.at(runnbr);
	} catch (std::out_of_range&) {
		std::stringstream msg;
		msg << "No calibration for run " << event.RunNum() << "!";
		if(not isCalibrationObject){
			throw PaCEDARDetector::NoCalibrationError(msg.str());
			exit(1);
		}
	}


	if(!forceRaw) {

	}
	if(forceRaw || foundCsDigits == false)
	{


		std::vector<PaDigit> rawDigits = event.RawDigits();
		for(int idig = 0; idig < (signed) rawDigits.size(); ++idig) {
			if( rawDigits[idig].NDigInfo() == 34 ) continue; // skip digits with to much bits ( S. Uhl )
			bool is_ce1 = rawDigits[idig].DecodeMapName() == "CE01P1__";
			bool is_ce2 = rawDigits[idig].DecodeMapName() == "CE02P1__";
			int cedar = is_ce1 ? 1 : (is_ce2 ? 2 : 0);
			if(cedar){
				double hit_time = rawDigits[idig].DigInfo(4);
				int ipm = std::fabs(rawDigits[idig].IWire())-1;
				if( timeHist != NULL )
					timeHist[cedar-1][ipm]->Fill(hit_time);
				if(currentCalib_){
					if(std::fabs(hit_time - currentCalib_->getTimeCalibOffset(cedar - 1,ipm)) < nSigma_ * currentCalib_->getTimeCalibSigma(cedar - 1,ipm))
						hitMask_[cedar-1] |= 1 << remap_[cedar-1][ipm];
				}
			}
		}
	}
}


void PaCEDARDetector::initEvent(const PaEvent& event, const PaTPar& beamTrackParameter, const bool foreRaw){
	this->readDigits(event, foreRaw);
	//                          Z                      X                      Y                       dX                     dY
	const double beamPar[5] = { beamTrackParameter(0), beamTrackParameter(1), beamTrackParameter(2),  beamTrackParameter(3), beamTrackParameter(4) };
	this->initDXDY(beamPar);
}

void PaCEDARDetector::initMCEvent(const PaEvent& event, const PaTPar& beamTrackParameter){
	currentCalib_ = &MCCalib_;
	//                          Z                      X                      Y                       dX                     dY
	const double beamPar[5] = { beamTrackParameter(0), beamTrackParameter(1), beamTrackParameter(2),  beamTrackParameter(3), beamTrackParameter(4) };
	this->initDXDY(beamPar);
}

#endif

//------------------------------------------------------------------------------
// extrapolate beamTrack to cedar position and get dx/dz and dy/dz
// according to the transport matrix
//------------------------------------------------------------------------------
void PaCEDARDetector::initDXDY(const double par[5]) {
	const double x30    = par[1] + (transpMat_[0][0] - par[0]) * par[3];
	const double y30    = par[2] + (transpMat_[0][0] - par[0]) * par[4];
	const double x_up   = transpMat_[0][53] * x30 + transpMat_[0][15] * 100 * par[3];
	const double y_up   = transpMat_[0][57] * y30 + transpMat_[0][19] * 100 * par[4];
	const double x_down = transpMat_[0][14] * x30 + transpMat_[0][15] * 100 * par[3];
	const double y_down = transpMat_[0][18] * y30 + transpMat_[0][19] * 100 * par[4];
	const double dz     = (-transpMat_[0][52] + transpMat_[0][13]);
	dxCEDAR_ = (x_up - x_down) / dz;
	dyCEDAR_ = (y_up - y_down) / dz;
	if( currentCalib_){
		for(int iCE = 0; iCE < 2; ++iCE){
			dxCEDARcor_[iCE] = dxCEDAR_ - currentCalib_->getInclinationCalibX(iCE);
			dyCEDARcor_[iCE] = dyCEDAR_ - currentCalib_->getInclinationCalibY(iCE);
		}
	} else {
		if(not isCalibrationObject){
			throw PaCEDARDetector::NoCalibrationError("No dX/dY calibration!");
		} else {
			for(int iCE = 0; iCE < 2; ++iCE){
				dxCEDARcor_[iCE] = dxCEDAR_;
				dyCEDARcor_[iCE] = dyCEDAR_;
			}
		}
	}
}


//------------------------------------------------------------------------------
// extrapolate beamTrack to cedar position and get dx/dz and dy/dz
// according to the transport matrix and correct for the CEDAR alignment
//------------------------------------------------------------------------------
void PaCEDARDetector::getDXDYcor(double& dxCEDAR1, double& dyCEDAR1, double& dxCEDAR2, double& dyCEDAR2)const{
	dxCEDAR1 = dxCEDARcor_[0];
	dyCEDAR1 = dyCEDARcor_[0];
	dxCEDAR2 = dxCEDARcor_[1];
	dyCEDAR2 = dyCEDARcor_[1];

}

void PaCEDARDetector::getThetacor(double& thetaCEDAR1, double& thetaCEDAR2){
	thetaCEDAR1 = sqrt( dxCEDARcor_[0] * dxCEDARcor_[0] + dyCEDARcor_[0] * dyCEDARcor_[0]);
	thetaCEDAR2 = sqrt( dxCEDARcor_[1] * dxCEDARcor_[1] + dyCEDARcor_[1] * dyCEDARcor_[1]);
}



PaCEDARDetector::Likelihoods PaCEDARDetector::getLikelihoods() const {
	if(not currentCalib_){
		throw PaCEDARDetector::NoCalibrationError("No calibration!");
	}
	return Likelihoods(currentCalib_->getLikelihood(0)(dxCEDARcor_[0], dyCEDARcor_[0], hitMask_[0]),
	                   currentCalib_->getLikelihood(1)(dxCEDARcor_[1], dyCEDARcor_[1], hitMask_[1]));
}


void PaCEDARDetector::readDCS(const std::string& filename){
	TFile* dcsfile = new TFile(filename.c_str());
	if( dcsfile and dcsfile->IsOpen()){
		const bool naming2008 = dcsfile->FindKey("Cedar1_Pressure") != nullptr;
		readDCSTree(dcsfile, naming2008? "Cedar1_Pressure" : "Cedar1_pressure"   , DCS_CEDAR1_preasure_);
		readDCSTree(dcsfile, "Cedar1_Temp_Head"  , DCS_CEDAR1_temp_head_,   + 273.15);
		readDCSTree(dcsfile, "Cedar1_Temp_Middle", DCS_CEDAR1_temp_middle_, + 273.15);
		readDCSTree(dcsfile, "Cedar1_Temp_Tail"  , DCS_CEDAR1_temp_tail_,   + 273.15);
		readDCSTree(dcsfile, "Cedar1_Temp_Room"  , DCS_CEDAR1_temp_room_,   + 273.15);
		readDCSTree(dcsfile, "Cedar1_Temp_Mean"  , DCS_CEDAR1_temp_mean_,   + 273.15);
		readDCSTree(dcsfile, naming2008? "Cedar2_Pressure" : "Cedar2_pressure"   , DCS_CEDAR2_preasure_);
		readDCSTree(dcsfile, "Cedar2_Temp_Head"  , DCS_CEDAR2_temp_head_,   + 273.15);
		readDCSTree(dcsfile, "Cedar2_Temp_Middle", DCS_CEDAR2_temp_middle_, + 273.15);
		readDCSTree(dcsfile, "Cedar2_Temp_Tail"  , DCS_CEDAR2_temp_tail_,   + 273.15);
		readDCSTree(dcsfile, "Cedar2_Temp_Room"  , DCS_CEDAR2_temp_room_,   + 273.15);
		readDCSTree(dcsfile, "Cedar2_Temp_Mean"  , DCS_CEDAR2_temp_mean_,   + 273.15);

	} else {
		std::cerr << "Can not open dcs file for CEDARs \"" << filename << "\"" << std::endl;
		exit(1);
	}


	dcsfile->Close();
}

#ifdef PHAST_HOME
void PaCEDARDetector::getPreassureAndMeanTemp(const PaEvent& event, int& event_timestamp, double& PCEDAR1, double& PCEDAR2, double& TempCEDAR1, double& TempCEDAR2){
	// get timestamp of event
	int Y, M, D, h, m, s;
	event.Date(Y, M, D, h, m, s);
	event_timestamp = TDatime(Y,M,D,h,m,s).Convert();

	PCEDAR1    = getPressureCE1(event_timestamp);
	PCEDAR2    = getPressureCE2(event_timestamp);
	TempCEDAR1 = getTempMeanCE1(event_timestamp);
	TempCEDAR2 = getTempMeanCE2(event_timestamp);

}
#endif

double PaCEDARDetector::getPressureCE1(const int timestamp, const bool interpolate)const {
	return getValueFromMap(timestamp, DCS_CEDAR1_preasure_, interpolate);
}

double PaCEDARDetector::getTempMeanCE1(const int timestamp, const bool interpolate)const {
	return getValueFromMap(timestamp, DCS_CEDAR1_temp_mean_, interpolate);
}

double PaCEDARDetector::getTempHeadCE1(const int timestamp, const bool interpolate)const {
	return getValueFromMap(timestamp, DCS_CEDAR1_temp_head_, interpolate);
}

double PaCEDARDetector::getTempMiddleCE1(const int timestamp, const bool interpolate)const {
	return getValueFromMap(timestamp, DCS_CEDAR1_temp_middle_, interpolate);
}

double PaCEDARDetector::getTempTailCE1(const int timestamp, const bool interpolate)const {
	return getValueFromMap(timestamp, DCS_CEDAR1_temp_tail_, interpolate);
}

double PaCEDARDetector::getPressureCE2(const int timestamp, const bool interpolate)const {
	return getValueFromMap(timestamp, DCS_CEDAR2_preasure_, interpolate);
}

double PaCEDARDetector::getTempMeanCE2(const int timestamp, const bool interpolate)const {
	return getValueFromMap(timestamp, DCS_CEDAR2_temp_mean_, interpolate);
}

double PaCEDARDetector::getTempHeadCE2(const int timestamp, const bool interpolate)const {
	return getValueFromMap(timestamp, DCS_CEDAR2_temp_head_, interpolate);
}

double PaCEDARDetector::getTempMiddleCE2(const int timestamp, const bool interpolate)const {
	return getValueFromMap(timestamp, DCS_CEDAR2_temp_middle_, interpolate);
}

double PaCEDARDetector::getTempTailCE2(const int timestamp, const bool interpolate)const {
	return getValueFromMap(timestamp, DCS_CEDAR2_temp_tail_, interpolate);
}

namespace {
	inline void readDCSTree(TFile* dcsfile, const char* treename, std::map<int, double>& out_valuelist, const double offset) {
		int time;
		float value;
		TTree* tree = dynamic_cast<TTree*>(dcsfile->Get(treename));
		if (tree) {
			const long n_entries = tree->GetEntries();
			tree->SetBranchAddress("time", &time);
			tree->SetBranchAddress("value", &value);
			for (long i = 0; i < n_entries; ++i) {
				tree->GetEntry(i);
				out_valuelist[time] = value + offset;
			}
		} else {
			std::cerr << "Can not open tree \"" << treename << "\" from DCS file." << std::endl;
			exit(1);
		}
	}


    inline double getValueFromMap(const int timestamp, const std::map<int, double>& value_map, const bool interpolate) {
		double value = 0.0;
		const std::map<int, double>::const_iterator upper = value_map.upper_bound(timestamp);
		if (upper != value_map.begin() && upper != value_map.end()) {
			std::map<int, double>::const_iterator lower = upper;
			--lower;
			if (not interpolate) {
				if (timestamp - upper->first < lower->first - timestamp) {
					value = upper->second;
				} else {
					value = lower->second;
				}
			} else {
				const double w_lower = static_cast<double>(upper->first - timestamp) / static_cast<double>(upper->first - lower->first);
				value = w_lower * lower->second + (1.0 - w_lower) * upper->second;
			}
		} else {
			if (upper == value_map.begin()) {
				std::cerr << "Timestamp \"" << timestamp << "\" before any DCS data!" << std::endl;
				throw std::range_error("Timestamp out of range (upper bound)");
			} else if (upper == value_map.end()) {
				std::cerr << "Timestamp \"" << timestamp << "\" after the last DCS data!" << std::endl;
				throw std::range_error("Timestamp out of range (lower bound)");
			} else {
				assert(true);
			}
		}
		return value;
	}
}

PaCEDARDetectorCalib::PaCEDARDetectorCalib(const std::string& outFileName){
	isCalibrationObject = true;
#ifdef PHAST_HOME
	Phast::Ref().HistFileDir("U32_CEDAR");
	gDirectory->mkdir("hist")->cd();
#else
	if(outFileName == "")
		of = NULL;
	else{
		of = new TFile(outFileName.c_str(),"RECREATE");
		gFile->cd();
		gDirectory->mkdir("CEDAR");
		gDirectory->cd("CEDAR");
	}
#endif

	const int nSecondsPerJear = 3600*24*365;
	hTimeOfRun_ = new TH1D("hTimeOfRun", "hTimeOfRun", static_cast<int>(nSecondsPerJear*10.0/600) , 1.21e9, 1.21e9 + nSecondsPerJear*10.0); // 2008 - 2009 10min resolution
	for(int ce = 0 ; ce < 2; ++ce) {
		hDx_[ce] = new TH1D(
		           Form("hDx_CE0%d", ce + 1),
		           Form("hDx_CE0%d", ce + 1),
		           500, -0.4e-3, 0.4e-3);
		hDy_[ce] = new TH1D(
		           Form("hDy_CE0%d", ce + 1),
		           Form("hDy_CE0%d", ce + 1),
		           500, -0.4e-3, 0.4e-3);
		hDx_[ce + 2] = new TH1D(
		               Form("hDx_CE0%dcor", ce + 1),
		               Form("hDx_CE0%dcor", ce + 1),
		               500, -0.4e-3, 0.4e-3);
		hDy_[ce + 2] = new TH1D(
		               Form("hDy_CE0%dcor", ce + 1),
		               Form("hDy_CE0%dcor", ce + 1),
		               500, -0.4e-3, 0.4e-3);
		hPhiNum_[ce] = new TH2D(
		               Form("hPhiNumCE0%d", ce + 1),
		               Form("hPhiNumCE0%d", ce + 1),
		               600, 0, 2*TMath::TwoPi(), 16, -8.5, 7.5);
		for(int pm = 0 ; pm < 8; ++pm) {
			hTime_[ce][pm] = new TH1D(
			                 Form("hTimeCE0%d_%d", ce + 1, pm),
			                 Form("hTimeCE0%d_%d", ce + 1, pm),
			                 36000, -2500, 4000);
		}
		hPhiRU_[ce] =   new TH3D(
		                         Form("hPhiRUCE0%d_all", ce + 1),
		                         Form("hPhiRUCE0%d;R;U;#Theta", ce + 1),
		                         50, 0.0, 0.5e-3,
		                         8,-0.5,7.5,
		                         200, 0.0, 2 * TMath::Pi()
		                );
		hPhiR_[ce] =    new TH2D(
		                         Form("hPhiRCE0%d_all", ce + 1),
		                         Form("hPhiRCE0%d;R;#Theta", ce + 1),
		                         50, 0.0, 0.5e-3,
		                         200, 0.0, 2 * TMath::Pi()
		                );
	}
}

void PaCEDARDetectorCalib::initDXDY(const double par[5]){
	PaCEDARDetector::initDXDY(par);
	if(hitMask_[0] == 0xff){
		hDx_[0]->Fill(dxCEDAR_);
		hDy_[0]->Fill(dyCEDAR_);
		hDx_[2]->Fill(dxCEDARcor_[0]);
		hDy_[2]->Fill(dyCEDARcor_[0]);
	}
	if(hitMask_[1] == 0xff){
		hDx_[1]->Fill(dxCEDAR_);
		hDy_[1]->Fill(dyCEDAR_);
		hDx_[3]->Fill(dxCEDARcor_[1]);
		hDy_[3]->Fill(dyCEDARcor_[1]);
	}
}

//------------------------------------------------------------------------------
// draw hit pattern vs. track angle Phi / needed to extract cedar rotation
// and possible wrong cedar mapping
//------------------------------------------------------------------------------
void PaCEDARDetectorCalib::drawPhiCorrelation(){
	for(int ce = 0; ce < 2; ++ce) {
		const double dxcedcor = dxCEDARcor_[ce];
		const double dycedcor = dyCEDARcor_[ce];
		const double CEDr = std::sqrt(dxcedcor*dxcedcor + dycedcor*dycedcor);
		double CEDphi =  std::atan2(dxcedcor,dycedcor);
		CEDphi += 0.5*TMath::Pi();
		if(CEDphi < 0)
			CEDphi += TMath::TwoPi();

		hPhiR_[ce]->Fill(CEDr, CEDphi );
		for(int u=0;u<8;u++){
			if(hitMask_[ce] & (1<<u)) {
				hPhiRU_[ce]->Fill(CEDr, u , CEDphi );
				if(CEDr<1.e-3) {
					hPhiNum_[ce]->Fill(CEDphi, u);
					if((5 - 1.27 * CEDphi) < u) {
						hPhiNum_[ce]->Fill(CEDphi, u);
						hPhiNum_[ce]->Fill(CEDphi+ TMath::TwoPi(), u-8);
						if((14 - 1.27 * (CEDphi+TMath::TwoPi())) < u-8)
							hPhiNum_[ce]->Fill(CEDphi , u-8);
					}
					if((14 - 1.27 * (CEDphi+TMath::TwoPi())) > u) {
						hPhiNum_[ce]->Fill(CEDphi + TMath::TwoPi(), u);
					}
				}
			}
		}
	}
}


PaCEDARDetectorCalibration::PerRunDataHandler::PerRunDataHandler(const char* calibFilename, const bool write) :
		_write(write),
		_calibFile(NULL),
		_calibTree(NULL),
		_likelihoodParameters{0.0},
		_likelihoodParameterErrors{0.0}
{
	if( _write ) _calibFile = new TFile(calibFilename, "UPDATE");
	else         _calibFile = new TFile(calibFilename, "OPEN");
	if (  _calibFile == NULL or (not _calibFile->IsOpen()))
		throw std::invalid_argument("Can not open CEDAR calibration file!");

	if( _write ){
		if( _calibFile->FindKey("calibCEDARPerRun")){ // tree already exists -> remove
			std::cout << "Write" << std::endl;
			_calibFile->Delete("calibCEDARPerRun;1");
		}
	}

	if( _write ) _calibTree =                             new TTree("calibCEDARPerRun", "calibCEDARPerRun");
	else         _calibTree = dynamic_cast<TTree*>( _calibFile->Get("calibCEDARPerRun"));
	if (  _calibTree == NULL or  _calibTree->IsZombie() )
		throw std::invalid_argument("Can not open CEDAR calibration tree!");

	if(_write) _calibTree->Branch("runNumber", &_runNumber);
	else       _calibTree->SetBranchAddress("runNumber", &_runNumber);

	if(_write) _calibTree->Branch("runAvgTimestamp", &_runAvgTimestamp);
	else       _calibTree->SetBranchAddress("runAvgTimestamp", &_runAvgTimestamp);

	for( int i_CEDAR = 0; i_CEDAR < 2; ++i_CEDAR){
		_addBranchToTree(_inclinationCalib[i_CEDAR][0], "DxDz_CE0%d", i_CEDAR);
		_addBranchToTree(_inclinationCalib[i_CEDAR][1], "DyDz_CE0%d", i_CEDAR);

		for( int i_PMT = 0; i_PMT<8; ++i_PMT){
			_addBranchToTree(_timeCalibOffset[i_CEDAR][i_PMT], "timeOffset_CE0%d_PMT%d", i_CEDAR, i_PMT);
			_addBranchToTree(_timeCalibSigma[i_CEDAR][i_PMT],  "timeSigma_CE0%d_PMT%d", i_CEDAR, i_PMT);
		}

		if(_write){
			_calibTree->Branch(Form("likelihoodParameter_CE0%d",i_CEDAR+1),_likelihoodParameters[i_CEDAR], Form("likelihoodParameter_CE0%d[128]/D",i_CEDAR+1));
		}
		else{
			_calibTree->SetBranchAddress(Form("likelihoodParameter_CE0%d",i_CEDAR+1),_likelihoodParameters[i_CEDAR]);
		}
		if(_write) _calibTree->Branch(Form("likelihoodID_CE0%d", i_CEDAR+1), &_likelihoodID[i_CEDAR]);
		else       _calibTree->SetBranchAddress(Form("likelihoodID_CE0%d", i_CEDAR+1), &_likelihoodID[i_CEDAR]);

		if(_write){
			_calibTree->Branch(Form("likelihoodParameterErrors_CE0%d",i_CEDAR+1),_likelihoodParameterErrors[i_CEDAR],
			                   Form("likelihoodParameterErrors_CE0%d[128]/D",i_CEDAR+1));
		}
		else{
			_calibTree->SetBranchAddress(Form("likelihoodParameterErrors_CE0%d",i_CEDAR+1),_likelihoodParameterErrors[i_CEDAR]);
		}
	}
}

void PaCEDARDetectorCalibration::PerRunDataHandler::_addBranchToTree(double& address, const char* format, const int i_CEDAR, const int i_PMT ){
	std::string name;
	if( i_PMT > -1 )name = Form(format, i_CEDAR+1, i_PMT);
	else name = Form(format, i_CEDAR+1);
	if(_write) _calibTree->Branch(name.c_str(), &address, Form("%s/D", name.c_str()));
	else       _calibTree->SetBranchAddress(name.c_str(), &address);
}

PaCEDARDetectorCalibration::ConstantDataHandler::ConstantDataHandler(const char* calibFilename, const bool write):
		_write(write),
		_calibFile(NULL),
		_calibTree(NULL){
	for( int i = 0; i < 8; ++i ){
		_remap[0][i] = -1;
		_remap[1][i] = -1;
	}
	if( _write ) _calibFile = new TFile(calibFilename, "UPDATE");
	else         _calibFile = new TFile(calibFilename, "OPEN");
	if (  _calibFile == NULL or (not _calibFile->IsOpen()))
		throw std::invalid_argument("Can not open CEDAR calibration file!");

	// check if the file contains an calibCEDARPerRun tree
	if(_write){
		if(_calibFile->FindKey("calibCEDARConst") != NULL){
			throw std::runtime_error("File already contains an 'calibCEDARConst' tree!");
		}
	}

	if( _write ) _calibTree =                             new TTree("calibCEDARConst", "calibCEDARConst");
	else         _calibTree = dynamic_cast<TTree*>( _calibFile->Get("calibCEDARConst"));
	if (  _calibTree == NULL or  _calibTree->IsZombie() )
		throw std::invalid_argument("Can not open CEDAR calibration tree!");

	if( not _write ){
		// load map if not in writing mode
		_calibTree->SetBranchAddress("remapCEDAR1", &_remap[0][0]);
		_calibTree->SetBranchAddress("remapCEDAR2", &_remap[1][0]);
		_calibTree->SetBranchAddress("transportMatrixCEDAR1", &_transportMatrix[0][0]);
		_calibTree->SetBranchAddress("transportMatrixCEDAR2", &_transportMatrix[1][0]);

		_calibTree->GetEntry(0);
	} else {
		_calibTree->Branch("remapCEDAR1", &_remap[0][0], "remapCEDAR1[8]/I");
		_calibTree->Branch("remapCEDAR2", &_remap[1][0], "remapCEDAR2[8]/I");
		_calibTree->Branch("transportMatrixCEDAR1", &_transportMatrix[0][0], "transportMatrixCEDAR1[65]/D");
		_calibTree->Branch("transportMatrixCEDAR2", &_transportMatrix[1][0], "transportMatrixCEDAR2[65]/D");
	}
}

void PaCEDARDetectorCalibration::ConstantDataHandler::close(){
	// store map if in writing mode
	if(_write){
		_calibTree->Fill();
		_calibFile->Write();
	}
	_calibFile->Close();
}

void PaCEDARDetectorCalibration::ConstantDataHandler::checkRemap(){
	for(int iCEDAR = 0; iCEDAR < 2; ++iCEDAR){
		std::set<int> map;
		for( int i = 0; i<8; ++i){
			const int pmt = getRemap(iCEDAR, i);
			if(pmt < 0 or pmt > 7){
				std::cerr << "PMT mapping " << i << "->" << pmt << " out of range for CEDAR " << iCEDAR+1 << "." << std::endl;
				throw std::range_error("PMT mapping");
			}
			if(map.count(pmt) != 0){
				std::cerr << "PMT mapping " << i << "->" << pmt << " defined twice for CEDAR " << iCEDAR+1 << "." << std::endl;
				throw std::range_error("PMT mapping");
			}
			map.insert(pmt);
		}
	}
}

void PaCEDARDetectorCalibration::PerRunDataHandler::_setLikelihoodParameters(){
	for(int i = 0; i<2; ++i){
		if(  _likelihoodID[i] == _likelihood[i].getLikelihoodID()){
			_likelihood[i].setParameters(_likelihoodParameters[i]);
		} else if ( _likelihoodID[i] != 0 ){ // ID == 0 means no parameters stored in configuration file
			throw std::runtime_error("CEDAR: ID of likelihood parameterization and ID of stored parameters does not agree.");
		}
	}
}

void PaCEDARDetectorCalibration::PerRunDataHandler::_getLikelihoodParameters(){
	for(int i = 0; i<2; ++i) _likelihood[i].getParameters(_likelihoodParameters[i]);
}

PaCEDARDetectorCalibration::PerRunData::PerRunData(){
	for( int iCedar = 0; iCedar < 2; ++iCedar){
		_inclinationCalib[iCedar][0] = 0.0;
		_inclinationCalib[iCedar][1] = 0.0;
		for( int iPMT = 0; iPMT < 8; ++iPMT){
			_timeCalibOffset[iCedar][iPMT] = 0.0;
			_timeCalibSigma[iCedar][iPMT] = 1.0;
		}
		_likelihoodID[iCedar] = 0;
 	}
}


void PaCEDARDetectorCalibration::PerRunData::setLikelihood(const int i_CEDAR, const Cedar::Likelihood& likelihood){
	_likelihood[i_CEDAR] = likelihood;
	_likelihoodID[i_CEDAR] = likelihood.getLikelihoodID();
}
void PaCEDARDetectorCalibration::PerRunData::unsetLikelihood(const int i_CEDAR){
	_likelihood[i_CEDAR] = Cedar::Likelihood();
	_likelihoodID[i_CEDAR] = 0;
}

const Cedar::Likelihood& PaCEDARDetectorCalibration::PerRunData::getLikelihood(const int i_CEDAR) const{
	if (_likelihoodID[i_CEDAR] == 0){
		throw PaCEDARDetector::NoCalibrationError("No likelihood calibration of this run!");
	}
	return _likelihood[i_CEDAR];
}

void PaCEDARDetectorCalibration::PerRunDataHandler::setLikelihoodParameterErrors(const int i_CEDAR, const std::vector<double>& parameterErrors){
	for( size_t i = 0; i < 128; ++i){
		if( i < parameterErrors.size()){
			_likelihoodParameterErrors[i_CEDAR][i] = parameterErrors[i];
		} else {
			_likelihoodParameterErrors[i_CEDAR][i] = 0.0;
		}
	}
}

void PaCEDARDetectorCalibration::PerRunDataHandler::unsetLikelihoodParameterErrors(const int i_CEDAR){
	for( size_t i = 0; i < 128; ++i){
		_likelihoodParameterErrors[i_CEDAR][i] = 0.0;
	}
}

std::vector<double> PaCEDARDetectorCalibration::PerRunDataHandler::getLikelihoodParameterErrors(const int i_CEDAR){
	std::vector<double> errors;
	for( size_t i = 0; i < 128; ++i){
		if(_likelihoodParameterErrors[i_CEDAR][i] != 0.0){
			errors.push_back(_likelihoodParameterErrors[i_CEDAR][i]);
		}else{
			break;
		}
	}
	return errors;
}
