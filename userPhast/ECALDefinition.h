#ifndef ECAL_DEFINITION_H
#define ECAL_DEFINITION_H

#include "Phast.h"


class ECALDefinition
{

public:

	ECALDefinition(TTree& tree);
	~ECALDefinition() { }

	void clear();

	void fill(const PaEvent& event);

	size_t getNumberNeutralClusters(const PaEvent& event) { return getNeutralParticleIndices(event).size(); }

private:

	const std::vector<int>& getNeutralParticleIndices(const PaEvent& event);
	void                    getTrackCoordsAtEcals    (const PaEvent& event);
	double                  getECALZFrontCoords(const int ECALIndex) const;

	Phast&               _phast;
	const PaCalorimeter& _ECAL1;
	const PaCalorimeter& _ECAL2;

	const double          _ECAL1FrontZ;
	const double          _ECAL2FrontZ;
	std::vector<int>      _neutralParticleIndices;
	std::vector<double>   _tracksZFirst;
	std::vector<TVector2> _tracksCoordsECAL1;
	std::vector<TVector2> _tracksCoordsECAL2;

	// output tree variables
	int                 _numberClusters;
	std::vector<int>    _clusterECALIndices;
	std::vector<double> _clusterXs;
	std::vector<double> _clusterYs;
	std::vector<double> _clusterZs;
	std::vector<double> _clusterXVariances;
	std::vector<double> _clusterYVariances;
	std::vector<double> _clusterZVariances;
	std::vector<double> _clusterEnergies;
	std::vector<double> _clusterEnergyVariances;
	std::vector<double> _clusterTimes;
	std::vector<double> _clusterTimeResolutions;
	std::vector<int>    _clusterSizes;
	std::vector<int>    _clusterCellIndices;  // indices of ECAL cell at cluster position
	std::vector<double> _clusterXInCells;
	std::vector<double> _clusterYInCells;
	std::vector<double> _clusterTracksMinDistance;
	std::vector<double> _clusterTracksMinDistZFirst;
	double              _smallestTrackMinDistance;

};

#endif  // ECAL_DEFINITION_H
