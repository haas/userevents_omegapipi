#include <sstream>

#include "PaPid.h"
#include "ScatteredDefinition.h"
#include "SelectionHelper.h"


namespace {

	template<typename T>
	bool
	setParticleBranches(TTree&             tree,
	                    std::vector<T>&    data,
	                    const std::string& branchName,
	                    const std::string& leafName,
	                    const std::string& leafType)
	{
		bool success = true;
		for (size_t i = 0; i < data.size(); ++i) {
			std::stringstream name;
			name << branchName << i + 1;
			std::stringstream leaf;
			leaf << leafName << i + 1 << "/" << leafType;
			if (not tree.Branch(name.str().c_str(), &(data[i]), leaf.str().c_str())) {
				std::cerr << "Cannot create branch '" << name.str() << "' with leaf '" << leaf.str()
				          << "' in tree '" << tree.GetName() <<  "'" << std::endl;
				success = false;
			}
		}
		return success;
	}

}


ScatteredDefinition::ScatteredDefinition(TTree&       tree,
                                         const size_t nmbChargedParticles,
                                         const size_t nmbNeutralParticles,
										 const double upperMassLimit,
										 const double lowerMassLimit)
	: _phast              (Phast::Ref()),
	  _nmbChargedParticles(nmbChargedParticles),
	  _nmbNeutralParticles(nmbNeutralParticles),
	  _preselectOmega(getPreselectOmegaFlag(upperMassLimit, lowerMassLimit)),
	  _upperMassLimit(upperMassLimit),
	  _lowerMassLimit(lowerMassLimit)
{
	clear();
	_phast.h_file->cd();

	setParticleBranches<double>(tree, _momentumX,                "Scattered_Momentum_X",               "momentumX",                "D");
	setParticleBranches<double>(tree, _momentumY,                "Scattered_Momentum_Y",               "momentumY",                "D");
	setParticleBranches<double>(tree, _momentumZ,                "Scattered_Momentum_Z",               "momentumZ",                "D");
	setParticleBranches<int>   (tree, _charge,                   "Scattered_Charge",                   "charge",                   "I");
	setParticleBranches<int>   (tree, _pid,                      "Scattered_Pid",                      "pid",                      "I");
	setParticleBranches<double>(tree, _firstZ,                   "Scattered_First_Z",                  "firstZ",                   "D");
	setParticleBranches<double>(tree, _lastZ,                    "Scattered_Last_Z",                   "lastZ",                    "D");
	setParticleBranches<double>(tree, _minimalZ,                 "Scattered_Minimal_Z",                "minimalZ",                 "D");
	setParticleBranches<double>(tree, _maximalZ,                 "Scattered_Maximal_Z",                "maximalZ",                 "D");
	setParticleBranches<double>(tree, _time,                     "Scattered_Time",                     "time",                     "D");
	setParticleBranches<double>(tree, _chi2red,                  "Scattered_Chi2red",                  "chi2red",                  "D");
	setParticleBranches<double>(tree, _XX0,                      "Scattered_XX0",                      "XX0",                      "D");
	setParticleBranches<int>   (tree, _hasPidRICH,               "Scattered_hasPidRICH",               "hasPidRICH",               "I");
	setParticleBranches<double>(tree, _likelihoodPionRICH,       "Scattered_LikelihoodPionRICH",       "likelihoodPionRICH",       "D");
	setParticleBranches<double>(tree, _likelihoodKaonRICH,       "Scattered_LikelihoodKaonRICH",       "likelihoodKaonRICH",       "D");
	setParticleBranches<double>(tree, _likelihoodProtonRICH,     "Scattered_LikelihoodProtonRICH",     "likelihoodProtonRICH",     "D");
	setParticleBranches<double>(tree, _likelihoodElectronRICH,   "Scattered_LikelihoodElectronRICH",   "likelihoodElectronRICH",   "D");
	setParticleBranches<double>(tree, _likelihoodMuonRICH,       "Scattered_LikelihoodMuonRICH",       "likelihoodMuonRICH",       "D");
	setParticleBranches<double>(tree, _likelihoodBackgroundRICH, "Scattered_LikelihoodBackgroundRICH", "likelihoodBackgroundRICH", "D");
	setParticleBranches<double>(tree, _thetaRICH,                "Scattered_ThetaRICH",                "thetaRICH",                "D");
	setParticleBranches<double>(tree, _positionXAtRICH,          "Scattered_PositionAtRICH_X",         "positionXAtRICH",          "D");
	setParticleBranches<double>(tree, _positionYAtRICH,          "Scattered_PositionAtRICH_Y",         "positionYAtRICH",          "D");
	setParticleBranches<double>(tree, _positionZAtRICH,          "Scattered_PositionAtRICH_Z",         "positionZAtRICH",          "D");
	setParticleBranches<double>(tree, _momentumXAtRICH,          "Scattered_MomentumAtRICH_X",         "momentumXAtRICH",          "D");
	setParticleBranches<double>(tree, _momentumYAtRICH,          "Scattered_MomentumAtRICH_Y",         "momentumYAtRICH",          "D");
	setParticleBranches<double>(tree, _momentumZAtRICH,          "Scattered_MomentumAtRICH_Z",         "momentumZAtRICH",          "D");
	setParticleBranches<double>(tree, _thetaAtRICH,              "Scattered_ThetaAtRICH",              "thetaAtRICH",              "D");
	setParticleBranches<double>(tree, _momentumMCX,              "Scattered_Momentum_MC_X",            "momentumMCX",              "D");
	setParticleBranches<double>(tree, _momentumMCY,              "Scattered_Momentum_MC_Y",            "momentumMCY",              "D");
	setParticleBranches<double>(tree, _momentumMCZ,              "Scattered_Momentum_MC_Z",            "momentumMCZ",              "D");
	setParticleBranches<int>   (tree, _chargeMC,                 "Scattered_Charge_MC",                "chargeMC",                 "I");
	setParticleBranches<int>   (tree, _pidMC,                    "Scattered_Pid_MC",                   "pidMC",                    "I");
}


void
ScatteredDefinition::clear()
{
	const double failValue = std::numeric_limits<double>::quiet_NaN();
	_momentumX.assign               (_nmbChargedParticles, failValue);
	_momentumY.assign               (_nmbChargedParticles, failValue);
	_momentumZ.assign               (_nmbChargedParticles, failValue);
	_charge.assign                  (_nmbChargedParticles, -777);  // fail value used in PHAST
	_pid.assign                     (_nmbChargedParticles, -1);    // fail value used in PHAST
	_firstZ.assign                  (_nmbChargedParticles, failValue);
	_lastZ.assign                   (_nmbChargedParticles, failValue);
	_minimalZ.assign                (_nmbChargedParticles, failValue);
	_maximalZ.assign                (_nmbChargedParticles, failValue);
	_time.assign                    (_nmbChargedParticles, failValue);
	_chi2red.assign                 (_nmbChargedParticles, failValue);
	_XX0.assign                     (_nmbChargedParticles, failValue);
	_hasPidRICH.assign              (_nmbChargedParticles, 0);
	_likelihoodPionRICH.assign      (_nmbChargedParticles, -1);
	_likelihoodKaonRICH.assign      (_nmbChargedParticles, -1);
	_likelihoodProtonRICH.assign    (_nmbChargedParticles, -1);
	_likelihoodElectronRICH.assign  (_nmbChargedParticles, -1);
	_likelihoodMuonRICH.assign      (_nmbChargedParticles, -1);
	_likelihoodBackgroundRICH.assign(_nmbChargedParticles, -1);
	_thetaRICH.assign               (_nmbChargedParticles, failValue);
	_positionXAtRICH.assign         (_nmbChargedParticles, failValue);
	_positionYAtRICH.assign         (_nmbChargedParticles, failValue);
	_positionZAtRICH.assign         (_nmbChargedParticles, failValue);
	_momentumXAtRICH.assign         (_nmbChargedParticles, failValue);
	_momentumYAtRICH.assign         (_nmbChargedParticles, failValue);
	_momentumZAtRICH.assign         (_nmbChargedParticles, failValue);
	_thetaAtRICH.assign             (_nmbChargedParticles, failValue);
	_momentumMCX.assign             (_nmbChargedParticles + _nmbNeutralParticles, failValue);
	_momentumMCY.assign             (_nmbChargedParticles + _nmbNeutralParticles, failValue);
	_momentumMCZ.assign             (_nmbChargedParticles + _nmbNeutralParticles, failValue);
	_chargeMC.assign                (_nmbChargedParticles + _nmbNeutralParticles, -777);  // fail value used in PHAST
	_pidMC.assign                   (_nmbChargedParticles + _nmbNeutralParticles, -1);    // fail value used in PHAST
}


void
ScatteredDefinition::fill(const PaEvent&            event,
                          const vector<PaParticle>& particles,
                          const PaVertex&           primVertex)
{
	const size_t nmbParticles = particles.size();
	assert(nmbParticles == _nmbChargedParticles);

	for (size_t i = 0; i < nmbParticles; ++i) {
		const PaParticle& particle       = particles[i];
		const PaTrack&    track          = event.vTrack(particle.iTrack());
		const PaTPar&     parameter      = particle.ParInVtx(primVertex.MyIndex());
		const TVector3    momentumVector = parameter.Mom3();

		_momentumX[i] = momentumVector.X();
		_momentumY[i] = momentumVector.Y();
		_momentumZ[i] = momentumVector.Z();
		_charge   [i] = particle.Q();
		_pid      [i] = particle.PID();  // GEANT3 ID
		_firstZ   [i] = track.ZFirst();
		_lastZ    [i] = track.ZLast();
		_minimalZ [i] = track.Zmin();
		_maximalZ [i] = track.Zmax();
		_time     [i] = track.MeanTime();
		_chi2red  [i] = track.Chi2tot() / track.Ndf();
		_XX0      [i] = track.XX0();

		getRICHInformation(
			event,
			track,
			parameter,
			_hasPidRICH              [i],
			_likelihoodPionRICH      [i],
			_likelihoodKaonRICH      [i],
			_likelihoodProtonRICH    [i],
			_likelihoodElectronRICH  [i],
			_likelihoodMuonRICH      [i],
			_likelihoodBackgroundRICH[i],
			_thetaRICH               [i],
			_positionXAtRICH         [i],
			_positionYAtRICH         [i],
			_positionZAtRICH         [i],
			_momentumXAtRICH         [i],
			_momentumYAtRICH         [i],
			_momentumZAtRICH         [i],
			_thetaAtRICH             [i]
		);
	}
}


void
ScatteredDefinition::fillMC(const PaEvent&    event,
                            const PaMCvertex& primVertex)
{
	const vector<PaMCtrack>& tracks = getScatteredParticleMCtracks(event, primVertex);
	const size_t nmbMCTracks = tracks.size();
	assert(nmbMCTracks == _nmbChargedParticles + _nmbNeutralParticles);
	_momentumMCX.resize(nmbMCTracks);
	_momentumMCY.resize(nmbMCTracks);
	_momentumMCZ.resize(nmbMCTracks);
	_chargeMC.resize   (nmbMCTracks);
	_pidMC.resize      (nmbMCTracks);

	for (size_t i = 0; i < nmbMCTracks; ++i) {
		const PaMCtrack& track = tracks[i];
		_momentumMCX[i] = track.P(0);
		_momentumMCY[i] = track.P(1);
		_momentumMCZ[i] = track.P(2);
		_chargeMC   [i] = track.Q();
		_pidMC      [i] = track.Pid();
	}
}

bool
ScatteredDefinition::checkMCOmega(const PaEvent&    event,
                                 const PaMCvertex& primVertex)
{
	if (!_preselectOmega) return true;
	// check if one of the piMinus pi0 piPlus combinations is within the given mass range 
	vector<PaMCtrack> tracks = getScatteredParticleMCtracks(event, primVertex);

	for (size_t i = 0; i < tracks.size() - 2; ++i) {
		for (size_t j = i + 1; j < tracks.size() - 1; ++j) {
			for (size_t k = j + 1; k < tracks.size(); ++k) {
				if ((tracks[i].Q() + tracks[j].Q() + tracks[k].Q()) != 0) {
					continue;
				}
				const double invMass = 1000 * (tracks[i].LzVec() + tracks[j].LzVec() + tracks[k].LzVec()).M(); // convert from GeV to MeV
				if (invMass > _lowerMassLimit and invMass < _upperMassLimit) {
					return true;
				}
			}
		}
	}
	return false;
}


vector<PaParticle>
ScatteredDefinition::getScatteredParticles(const PaEvent&  event,
                                           const PaVertex& primVertex)
{
	vector<PaParticle> scatteredParticles;
	for (int i = 0; i < primVertex.NOutParticles(); ++i) {
		scatteredParticles.push_back(event.vParticle(primVertex.iOutParticle(i)));
	}
	return scatteredParticles;
}


// Get RICH information; code adapted from Stefan Wallner's event selection
// see ParticleWithRICHPid::setRICHInfo()
// in /nfs/freenas/tuph/e18/project/compass/analysis/swallner/EventSelection/phast_userevent/TreeFiller.cc
void
ScatteredDefinition::getRICHInformation(const PaEvent& event,
                                        const PaTrack& track,
                                        const PaTPar&  parameter,
                                        int&           hasPidRICH,
                                        double&        likelihoodPionRICH,
                                        double&        likelihoodKaonRICH,
                                        double&        likelihoodProtonRICH,
                                        double&        likelihoodElectronRICH,
                                        double&        likelihoodMuonRICH,
                                        double&        likelihoodBackgroundRICH,
                                        double&        thetaRICH,
                                        double&        positionXAtRICH,
                                        double&        positionYAtRICH,
                                        double&        positionZAtRICH,
                                        double&        momentumXAtRICH,
                                        double&        momentumYAtRICH,
                                        double&        momentumZAtRICH,
                                        double&        thetaAtRICH)
{
	static const RICH::MCHelper* richMCHelper = []() {
		// Read RICH efficiency maps
		std::stringstream richMapsFilePath;
		const string year = SelectionHelper::getYear();
		richMapsFilePath << PaUtils::PhastHome() << "/user/rich_calib/" << year << "/RICHmaps" << year << ".root";
		const char* richLikelihoodThresholdMC = "1.15";
		const RICH::MCHelper* helper = new RICH::MCHelper(richMapsFilePath.str().c_str(), richLikelihoodThresholdMC);
		assert(helper);
		std::cout << "Using z position of " << RICH_POSITION_Z << " cm to extrapolate tracks to RICH." << std::endl;
		return helper;
	}();

	PaTPar trackParAtRICH;
	if (getTrackParAtRICH(parameter, event, trackParAtRICH)) {
		positionXAtRICH = trackParAtRICH.X();
		positionYAtRICH = trackParAtRICH.Y();
		positionZAtRICH = trackParAtRICH.Z();
		momentumXAtRICH = trackParAtRICH.Mom3().Px();
		momentumYAtRICH = trackParAtRICH.Mom3().Py();
		momentumZAtRICH = trackParAtRICH.Mom3().Pz();
		thetaAtRICH     = trackParAtRICH.Theta();
	}
	if (not event.IsMC()) {
		// Real-data track
		static PaPid pidRICH;
		if (pidRICH.CheckRichInfo(track)) {
			// RICH PID information is available
			hasPidRICH               = 1;
			likelihoodPionRICH       = pidRICH.GetLike(RICH::PID::pion,       track);
			likelihoodKaonRICH       = pidRICH.GetLike(RICH::PID::kaon,       track);
			likelihoodProtonRICH     = pidRICH.GetLike(RICH::PID::proton,     track);
			likelihoodElectronRICH   = pidRICH.GetLike(RICH::PID::electron,   track);
			likelihoodMuonRICH       = pidRICH.GetLike(RICH::PID::muon,       track);
			likelihoodBackgroundRICH = pidRICH.GetLike(RICH::PID::background, track);
			thetaRICH                = pidRICH.Theta_Ch(track);
		}
	} else {
		// MC track; simulate RICH based on efficiency maps
		int pidMC = -1;
		if (track.iMCtrack() > -1) {  // track is associated with an MC track
			const PaMCtrack& trackMC = event.vMCtrack(track.iMCtrack());
			pidMC = trackMC.Pid();  // GEANT3 ID
			double absMomAtRICH, thetaAtRICH;
			if (getMomThetaAtRICH(trackMC, event, absMomAtRICH, thetaAtRICH)) {
				// extrapolation of track to RICH position was successful
				int richPidMC, chargeMC;  // RICH ID and charge
				RICH::geant3ToRICHPid(pidMC, richPidMC, chargeMC);
				if (   richPidMC == RICH::PID::pion   or richPidMC == RICH::PID::kaon
				    or richPidMC == RICH::PID::proton or richPidMC == RICH::PID::muon) {
					// we do have efficiency maps for the particle
					const int pidSimulation = richMCHelper->predictPID(absMomAtRICH, thetaAtRICH, richPidMC, chargeMC);
					// set RICH variables to default values
					hasPidRICH               = 1;
					likelihoodPionRICH       = 1e-10;
					likelihoodKaonRICH       = 1e-10;
					likelihoodProtonRICH     = 1e-10;
					likelihoodElectronRICH   = 1e-10;
					likelihoodMuonRICH       = 1e-10;
					likelihoodBackgroundRICH = 1e-10;
					switch (pidSimulation) {
						case RICH::PID::pion :
							likelihoodPionRICH = 1;
							break;
						case RICH::PID::kaon :
							likelihoodKaonRICH = 1;
							break;
						case RICH::PID::proton :
							likelihoodProtonRICH = 1;
							break;
						case RICH::PID::electron :
							likelihoodElectronRICH = 1;
							break;
						case RICH::PID::muon :
							likelihoodMuonRICH = 1;
							break;
						case RICH::PID::background :
							likelihoodBackgroundRICH = 1;
							break;
						case RICH::PID::nopid :
							hasPidRICH               =  0;
							likelihoodPionRICH       = -1;
							likelihoodKaonRICH       = -1;
							likelihoodProtonRICH     = -1;
							likelihoodElectronRICH   = -1;
							likelihoodMuonRICH       = -1;
							likelihoodBackgroundRICH = -1;
							break;
						case RICH::PID::outOfKinematicRange :
							hasPidRICH               =  0;
							likelihoodPionRICH       = -2;
							likelihoodKaonRICH       = -2;
							likelihoodProtonRICH     = -2;
							likelihoodElectronRICH   = -2;
							likelihoodMuonRICH       = -2;
							likelihoodBackgroundRICH = -2;
							break;
						default :
							break;
					}
				} else {
					// std::cout << "There is no RICH efficiency map for particles with GEANT3 ID = " << pidMC << ". Will not simulate RICH." << std::endl;
					hasPidRICH               =  0;
					likelihoodPionRICH       = -3;
					likelihoodKaonRICH       = -3;
					likelihoodProtonRICH     = -3;
					likelihoodElectronRICH   = -3;
					likelihoodMuonRICH       = -3;
					likelihoodBackgroundRICH = -3;
				}
			} else {
				// set no PID if we cannot extrapolate to the RICH position
				hasPidRICH               =  0;
				likelihoodPionRICH       = -4;
				likelihoodKaonRICH       = -4;
				likelihoodProtonRICH     = -4;
				likelihoodElectronRICH   = -4;
				likelihoodMuonRICH       = -4;
				likelihoodBackgroundRICH = -4;
			}
		}
	}
}


vector<PaTrack>
ScatteredDefinition::getScatteredParticleTracks(const PaEvent&  event,
                                                const PaVertex& primVertex)
{
	vector<PaTrack>    tracks;
	vector<PaParticle> scatteredParticles = getScatteredParticles(event, primVertex);
	for (size_t i = 0; i < scatteredParticles.size(); ++i) {
		const PaParticle& particle = scatteredParticles[i];
		tracks.push_back(event.vTrack(particle.iTrack()));
	}
	return tracks;
}


vector<PaMCtrack>
ScatteredDefinition::getScatteredParticleMCtracks(const PaEvent&    event,
                                                  const PaMCvertex& primVertex)
{
	vector<PaMCtrack> tracks;
	for (int i = 0; i < primVertex.NMCtrack(); ++i) {
		const PaMCtrack& track = event.vMCtrack(primVertex.iMCtrack(i));
		if (track.IsBeam() or track.Pid() == MC_PID_PROTON) {
			continue;
		} else {
			tracks.push_back(track);
		}
	}
	return tracks;
}


bool
ScatteredDefinition::getTrackParAtRICH(const PaTPar&  trackParIn,
                                       const PaEvent& event,
                                       PaTPar&        trackParAtRICH)
{
	const bool success = trackParIn.Extrapolate(RICH_POSITION_Z, trackParAtRICH, false);
	return success and trackParAtRICH.HasMom();
}


bool
ScatteredDefinition::getTrackParAtRICH(const PaMCtrack& trackMC,
                                       const PaEvent&   event,
                                       PaTPar&          trackParAtRICH)
{
	const PaTPar trackParAtVertex = trackMC.ParInVtx();
	const bool success = trackParAtVertex.Extrapolate(RICH_POSITION_Z, trackParAtRICH, false);
	return success and trackParAtRICH.HasMom();
}


bool
ScatteredDefinition::getMomThetaAtRICH(const PaTPar&  trackPar,
                                       const PaEvent& event,
                                       double&        absMomAtRICH,
                                       double&        thetaAtRICH)
{
	PaTPar trackParAtRICH;
	const bool success = getTrackParAtRICH(trackPar, event, trackParAtRICH);
	if (success) {
		absMomAtRICH = trackParAtRICH.Mom();
		thetaAtRICH  = trackParAtRICH.Theta();
	}
	return success;
}


bool
ScatteredDefinition::getMomThetaAtRICH(const PaMCtrack& trackMC,
                                       const PaEvent&   event,
                                       double&          absMomAtRICH,
                                       double&          thetaAtRICH)
{
	PaTPar trackParAtRICH;
	const bool success = getTrackParAtRICH(trackMC, event, trackParAtRICH);
	if (success) {
		absMomAtRICH = trackParAtRICH.Mom();
		thetaAtRICH  = trackParAtRICH.Theta();
	}
	return success;
}

bool
ScatteredDefinition::getPreselectOmegaFlag(const double upperMassLimit,
										   const double lowerMassLimit)
{
	std::string preselectFlag;
	for (int i = 0; i < Phast::Ref().NTextUserFlag(); ++i) {
		std::string userFlag = Phast::Ref().TextUserFlag(i);
		if (userFlag.find("PreselectMCOmega:") != std::string::npos) {  // found flag
			if (preselectFlag.empty()) {  // first appearance
				preselectFlag = userFlag.replace(0, 17, "");  // remove "PreselectMCOmega:"
			} else {
				throw std::invalid_argument("Multiple booleans given in 'phast -T PreselectMCOmega:<bool>' call! Aborting...");
			}
		}
	}
	bool preselectBool;
	if (preselectFlag.empty()) {
		std::cout << "\nNo boolean was defined via '-T PreselectMCOmega:<bool>' "
		          << "with bool = {true, false}. No preselection is performed.";
		preselectBool = false;
	} else {
		std::transform(preselectFlag.begin(), preselectFlag.end(), preselectFlag.begin(),
		               [](unsigned char c){ return std::tolower(c); });
		if (preselectFlag == "false") {
			preselectBool = false;
		} else if (preselectFlag == "true") {
			preselectBool = true;
		} else {
			throw std::invalid_argument("Unkown boolean '" + preselectFlag
				+ "' was defined via '-T PreselectMCOmega:<bool>'. Use either 'true' or 'false'. Aborting...");
		}
		std::cout << "\nThe 'PreselectMCOmega' flag is set to " << preselectFlag << "." << std::endl;
		if (preselectFlag == "true") {
			std::cout << "\nOnly events containing atleast one PiMinus Pi0 PiPlus combination with an inv mass in [" << lowerMassLimit << ";" << upperMassLimit <<  "] are selected." << std::endl;
		}
	}
	return preselectBool;
}