#ifndef BEAMDEFINITION_H
#define BEAMDEFINITION_H

#include "CEDARMCHelper.h"
#include "PaCEDARDetector.h"
#include "Phast.h"


class BeamDefinition
{

public:

	BeamDefinition(TTree& tree);
	~BeamDefinition();

	void clear();

	void fill  (const PaEvent& event);
	void fillMC(const PaEvent& event, const PaMCvertex& primVertex);

	static const PaParticle& getBeamParticle(const PaEvent& event, const PaVertex&   primVertex) { return event.vParticle(primVertex.InParticle()); }

private:

	static const PaMCtrack&  getBeamTrackMC (const PaEvent& event, const PaMCvertex& primVertex);

	static bool                       getIgnoreMissingCedarCalib();
	static CEDARHelper::MCHelper::pid getTrueCEDARBeamPid();

	Phast&                           _phast;
	PaCEDARDetector                  _cedar;
	const CEDARHelper::MCHelper*     _cedarMCHelper;
	const bool                       _ignoreMissingCedarCalib;
	const CEDARHelper::MCHelper::pid _beamTruePID;

	static const int MC_PID_BEAM = 44;  // special pi- beam PID (see COMGEANT fort.15 aka main_hadron_200{8,9}.ffr)

	// output tree variables
	double _gradX;
	double _gradY;
	double _time;
	double _firstZ;
	double _lastZ;
	double _minimalZ;
	double _maximumZ;
	double _chi2red;
	int    _charge;
	int    _pid;
	int    _majorityCEDAR1;
	int    _majorityCEDAR2;
	double _likelihoodPionCEDAR1;
	double _likelihoodPionCEDAR2;
	double _likelihoodKaonCEDAR1;
	double _likelihoodKaonCEDAR2;
	double _momentumMCX;
	double _momentumMCY;
	double _momentumMCZ;

};

#endif  // BEAMDEFINITION_H
