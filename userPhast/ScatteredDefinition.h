#ifndef SCATTEREDDEFINITION_H
#define SCATTEREDDEFINITION_H

#include "Phast.h"
#include "RICHMCHelper.h"


class ScatteredDefinition
{

public:

	ScatteredDefinition(TTree& tree, const size_t nmbChargedPartricles, const size_t nmbNeutralPartricles, const double upperMassLimit, const double lowerMassLimit);
	~ScatteredDefinition() { }

	void clear();

	void fill  (const PaEvent& event, const vector<PaParticle>& particles, const PaVertex& primVertex);
	void fillMC(const PaEvent& event, const PaMCvertex& primVertex);

	bool checkMCOmega(const PaEvent& event, const PaMCvertex& primVertex);

	static vector<PaParticle> getScatteredParticles(const PaEvent& event, const PaVertex&   primVertex);

	static void getRICHInformation(const PaEvent& event,
	                               const PaTrack& track,
	                               const PaTPar&  parameter,
	                               int&           hasPidRICH,
	                               double&        likelihoodPionRICH,
	                               double&        likelihoodKaonRICH,
	                               double&        likelihoodProtonRICH,
	                               double&        likelihoodElectronRICH,
	                               double&        likelihoodMuonRICH,
	                               double&        likelihoodBackgroundRICH,
	                               double&        thetaRICH,
	                               double&        positionXAtRICH,
	                               double&        positionYAtRICH,
	                               double&        positionZAtRICH,
	                               double&        momentumXAtRICH,
	                               double&        momentumYAtRICH,
	                               double&        momentumZAtRICH,
	                               double&        thetaAtRICH);

private:

	static vector<PaTrack>   getScatteredParticleTracks  (const PaEvent& event, const PaVertex&   primVertex);
	static vector<PaMCtrack> getScatteredParticleMCtracks(const PaEvent& event, const PaMCvertex& primVertex);

	static bool getTrackParAtRICH(const PaTPar&    trackParIn, const PaEvent& event, PaTPar& trackParAtRICH);
	static bool getTrackParAtRICH(const PaMCtrack& trackMC,    const PaEvent& event, PaTPar& trackParAtRICH);
	static bool getMomThetaAtRICH(const PaTPar&    trackPar, const PaEvent& event, double& absMomAtRICH, double& thetaAtRICH);
	static bool getMomThetaAtRICH(const PaMCtrack& trackMC,  const PaEvent& event, double& absMomAtRICH, double& thetaAtRICH);

	static bool getPreselectOmegaFlag(const double upperMassLimit, const double lowerMassLimit);

	Phast&       _phast;
	const size_t _nmbChargedParticles;
	const size_t _nmbNeutralParticles;
	const bool   _preselectOmega;
	const double _upperMassLimit;
	const double _lowerMassLimit;

	static constexpr double RICH_POSITION_Z = 615.5;  // z position to which tracks are extrapolated [cm]
	static const     int    MC_PID_PROTON   = 14;     // Geant3 ID for proton

	// output tree variables
	std::vector<double> _momentumX;
	std::vector<double> _momentumY;
	std::vector<double> _momentumZ;
	std::vector<int>    _charge;
	std::vector<int>    _pid;
	std::vector<double> _firstZ;
	std::vector<double> _lastZ;
	std::vector<double> _minimalZ;
	std::vector<double> _maximalZ;
	std::vector<double> _time;
	std::vector<double> _chi2red;
	std::vector<double> _XX0;
	std::vector<int>    _hasPidRICH;
	std::vector<double> _likelihoodPionRICH;
	std::vector<double> _likelihoodKaonRICH;
	std::vector<double> _likelihoodProtonRICH;
	std::vector<double> _likelihoodElectronRICH;
	std::vector<double> _likelihoodMuonRICH;
	std::vector<double> _likelihoodBackgroundRICH;
	std::vector<double> _thetaRICH;
	std::vector<double> _positionXAtRICH;
	std::vector<double> _positionYAtRICH;
	std::vector<double> _positionZAtRICH;
	std::vector<double> _momentumXAtRICH;
	std::vector<double> _momentumYAtRICH;
	std::vector<double> _momentumZAtRICH;
	std::vector<double> _thetaAtRICH;
	std::vector<double> _momentumMCX;
	std::vector<double> _momentumMCY;
	std::vector<double> _momentumMCZ;
	std::vector<int>    _chargeMC;
	std::vector<int>    _pidMC;

};

#endif  // SCATTEREDDEFINITION_H
