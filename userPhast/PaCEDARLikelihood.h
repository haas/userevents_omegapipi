/*
 * Likelihood.h
 *
 *  Created on: Jun 23, 2016
 *      Author: ga34liw
 */

#ifndef LOGLIKELIHOOD_H_
#define LOGLIKELIHOOD_H_


#include <cmath>
#include <iostream>

namespace Cedar {

	class Likelihood;
	struct LikelihoodResult{
		double likePion;
		double likeKaon;
		double likeElectron;
		double likeProton;
	};
} /* namespace CEDAR */




#ifdef __INTEL_OFFLOAD
#pragma omp declare target
#endif
class Cedar::Likelihood {
public:
	Likelihood();

	void setPmtPosDeltaTheta(const int iPmt, const double value)      { _PmtPosDeltaTheta[iPmt]       = value; calcPmtPosDxy(iPmt);}
	void setPmtPosPhi(const int iPmt, const double value)             { _PmtPosPhi[iPmt]              = value; calcPmtPosDxy(iPmt);}
	void setPmtProbAmplitude(const int iPmt, const double value)      { _PmtProbAmplitude[iPmt]       = value; }
	void setPmtProbAmplitudeChange(const int iPmt, const double value){ _PmtProbAmplitudeChange[iPmt] = value;}
	void setPmtProbPeakWidth(const int iPmt, const double value)      { _PmtProbPeakWidth[iPmt]       = value;}
	void setPmtProbPeakWidthChange(const int iPmt, const double value){ _PmtProbPeakWidthChange[iPmt] = value;}
	void setPmtProbSigma(const int iPmt, const double value)          { _PmtProbSigma[iPmt]           = value;}
	void setPmtProbSigmaChange(const int iPmt, const double value)    { _PmtProbSigmaChange[iPmt]     = value;}
	void setPmtProbBkgAmpl(const int iPmt, const double value)        { _PmtProbBkgAmpl[iPmt]         = value;}
//	void setPmtProbBkgPos(const int iPmt, const double value)         { _PmtProbBkgPos[iPmt]          = value;}
//	void setPmtProbBkgWidth(const int iPmt, const double value)       { _PmtProbBkgWidth[iPmt]        = value;}
	void setDeltaRefractionIndex(const double value); // n - 1.0

	double getPmtPosDeltaTheta(const int iPmt)const      {return  _PmtPosDeltaTheta[iPmt]; }
	double getPmtPosPhi(const int iPmt)const             {return  _PmtPosPhi[iPmt];}
	double getPmtProbAmplitude(const int iPmt)const      {return  _PmtProbAmplitude[iPmt];}
	double getPmtProbAmplitudeChange(const int iPmt)const{return  _PmtProbAmplitudeChange[iPmt];}
	double getPmtProbPeakWidth(const int iPmt)const      {return  _PmtProbPeakWidth[iPmt];}
	double getPmtProbPeakWidthChange(const int iPmt)const{return  _PmtProbPeakWidthChange[iPmt];}
	double getPmtProbSigma(const int iPmt)const          {return  _PmtProbSigma[iPmt];}
	double getPmtProbSigmaChange(const int iPmt)const    {return  _PmtProbSigmaChange[iPmt];}
	double getPmtProbBkgAmpl(const int iPmt)const        {return  _PmtProbBkgAmpl[iPmt];}
//	double getPmtProbBkgPos(const int iPmt)const         {return  _PmtProbBkgPos[iPmt];}
//	double getPmtProbBkgWidth(const int iPmt)const       {return  _PmtProbBkgWidth[iPmt];}
	double getDeltaRefractionIndex()const                { return _DrefractionIndex;}
	int getLikelihoodID() const                          { return 1;}

	Cedar::LikelihoodResult operator() (const double dX, const double dY, const char hitMask)const;

	double getCherenkovAnglePion() const {return _cherenkovAnglePion;}
	double getCherenkovAngleKaon() const {return _cherenkovAngleKaon;}
	double getDrefractionIndex() const { return _DrefractionIndex;}

	void printParameters(FILE* stream = nullptr)const;
	void getParameters(double* para)const;
	void setParameters(const double* para);


private:
	static const double _pionMass;
	static const double _kaonMass;
	static const double _electronMass;
	static const double _protonMass;
	static const double _momentum;
	double _PmtPosDeltaTheta[8] __attribute__((aligned(64)));
	double _PmtPosTheta[8] __attribute__((aligned(64)));
	double _PmtPosPhi[8] __attribute__((aligned(64)));
	double _PmtPosDx[8] __attribute__((aligned(64)));
	double _PmtPosDy[8] __attribute__((aligned(64)));
	double _PmtProbAmplitude[8] __attribute__((aligned(64)));
	double _PmtProbAmplitudeChange[8] __attribute__((aligned(64)));
	double _PmtProbPeakWidth[8] __attribute__((aligned(64)));
	double _PmtProbPeakWidthChange[8] __attribute__((aligned(64)));
	double _PmtProbSigma[8] __attribute__((aligned(64)));
	double _PmtProbSigmaChange[8] __attribute__((aligned(64)));
	double _PmtProbBkgAmpl[8] __attribute__((aligned(64)));
//	double _PmtProbBkgPos[8] __attribute__((aligned(64)));
//	double _PmtProbBkgWidth[8] __attribute__((aligned(64)));
	double _DrefractionIndex;
	double _cherenkovAnglePion;
	double _cherenkovAngleKaon;
	double _cherenkovAngleElectron;
	double _cherenkovAngleProton;

	void calcPmtPosDxy(const int iPmt);

	double calcProb(const int iPmt, const double distance, const double phi, const double cherenkovAngle, const char hitMaks)const;
	double calcProbParam(const int iPmt, const double delta, const double phi)const;
};

#if defined(_OPENMP)
#pragma omp declare simd
#endif


inline double Cedar::Likelihood::calcProbParam(const int iPmt, const double delta, const double phi)const{
	const double sigma = _PmtProbSigma[iPmt] + _PmtProbSigmaChange[iPmt]*phi*phi;
	const double width = _PmtProbPeakWidth[iPmt] + _PmtProbPeakWidthChange[iPmt]*phi*phi;
	return  erf(+(delta+width)/sigma)-erf(+(delta-width)/sigma);
}

#if defined(_OPENMP)
#pragma omp declare simd
#endif
inline double Cedar::Likelihood::calcProb(const int iPmt, const double distance, const double phi, const double cherenkovAngle, const char hitMask) const {
		const double delta = cherenkovAngle - distance;
//		if ( _PmtProbAmplitudeChange[iPmt] != 0.0 )
//			printf("%20.8g%20.8g\n", phi, phi*_PmtProbAmplitudeChange[iPmt]);
		double prob = (_PmtProbAmplitude[iPmt] - _PmtProbAmplitudeChange[iPmt]*phi*phi) \
		              * (calcProbParam(iPmt, delta, phi) + _PmtProbBkgAmpl[iPmt])
		              / (calcProbParam(iPmt, 0.0, phi) + _PmtProbBkgAmpl[iPmt])
		              ;
		prob = (prob == 0.0)? 1.0e-25 : prob; 
		return ((hitMask&(1<<iPmt)) != 0)? prob: 1.0 - prob;
}

#ifdef __INTEL_OFFLOAD
#pragma omp end declare target
#endif

#endif /* LOGLIKELIHOOD_H_ */
