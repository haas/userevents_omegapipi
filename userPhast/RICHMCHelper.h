/*
 * Helper class to handle RICH Pid for MC events
 * using efficiency maps from data.
 * gRandom is used to generate random numbers
 */

#ifndef RICH_MCHELPER_H_
#define RICH_MCHELPER_H_

#include<map>
#include<stdexcept>
#include<iostream>
#include<TH2D.h>

class TDirectory;

namespace RICH {
	class MCHelper;
	void geant3ToRICHPid(const int geant3_pid, int& rich_pid_out, int& charge_out);
	namespace Helper {
		class Predictor;
	}
	namespace PID{
		enum pid { pion = 0, kaon = 1, proton = 2, electron = 3, muon = 4, background = 5, nopid = 6, error = -3, outOfKinematicRange = -4 };
	}
	namespace Charges{
		enum charges { neg = -1, pos = +1, error = -3 };
	}
	namespace Exceptions{
		class NoPidMCForParticle: public std::out_of_range{
		public:
			explicit NoPidMCForParticle(const std::string& what): out_of_range(what){}
		};
		class NoRichPidForGeant3Pid: public std::out_of_range{
		public:
			explicit NoRichPidForGeant3Pid(const std::string& what): out_of_range(what){}
		};
	}
}




class RICH::Helper::Predictor {
public:
	Predictor(){}
	Predictor(TDirectory* dir, const int realPID, const int charge);
	int predictPID(const double momAbs, const double theta)const;

private:
	std::map< int, TH2D* > efficiencyMaps;
	TH2D* validMap; // defines the kinematic region where the maps are valid


};



class RICH::MCHelper {
public:

	typedef std::map<int, RICH::Helper::Predictor> ChargeMap;
	typedef std::map< int, ChargeMap > PidChargeMap;

	MCHelper(const char* efficiencyMapFilename, const char* cutThreshold);
	/*
	 * Predict the PID for a MC event
	 * momAbs: Absolute momentum of the particle
	 * theta: Theta angel of the particle (w.r.t. z-axis)
	 * realPID: MC truth of the PID of the particle
	 * charge: Charge of the particle
	 */
	int predictPID(const double momAbs, const double theta, const int realPID, const int charge)const{
	PidChargeMap::const_iterator it = predictors_.find(realPID);
		if(it != predictors_.end()){
			ChargeMap::const_iterator jt = it->second.find(charge);
			if(jt != it->second.end()){
				return jt->second.predictPID(momAbs, theta);
			} else {
				throw RICH::Exceptions::NoPidMCForParticle(Form("Can not find charge '%d'.", charge));
			}
		} else {
				throw RICH::Exceptions::NoPidMCForParticle(Form("No RICH efficiency map for real PID '%d'.", realPID));
		}
	}

	const static std::map<int, const char*> chargeNames, pidNames;

private:
	PidChargeMap predictors_; // <realPID><charge>


};


inline void RICH::geant3ToRICHPid(const int geant3_pid, int& rich_pid_out, int& charge_out){
	rich_pid_out = RICH::PID::error;
	charge_out = RICH::Charges::error;
	switch (geant3_pid) {
		case 8:
			rich_pid_out = RICH::PID::pion;
			charge_out = RICH::Charges::pos;
			break;
		case 9:
			rich_pid_out = RICH::PID::pion;
			charge_out = RICH::Charges::neg;
			break;
		case 11:
			rich_pid_out = RICH::PID::kaon;
			charge_out = RICH::Charges::pos;
			break;
		case 12:
			rich_pid_out = RICH::PID::kaon;
			charge_out = RICH::Charges::neg;
			break;
		case 14:
			rich_pid_out = RICH::PID::proton;
			charge_out = RICH::Charges::pos;
			break;
		case 15:
			rich_pid_out = RICH::PID::proton;
			charge_out = RICH::Charges::neg;
			break;
		case 2:
			rich_pid_out = RICH::PID::electron;
			charge_out = RICH::Charges::pos;
			break;
		case 3:
			rich_pid_out = RICH::PID::electron;
			charge_out = RICH::Charges::neg;
			break;
		case 5:
			rich_pid_out = RICH::PID::muon;
			charge_out = RICH::Charges::pos;
			break;
		case 6:
			rich_pid_out = RICH::PID::muon;
			charge_out = RICH::Charges::neg;
			break;
		default:
//			throw RICH::Exceptions::NoRichPidForGeant3Pid(Form("There is no RICH Pid for Geant3 PID '%d'.", geant3_pid));
			break;
	}
}


#endif
