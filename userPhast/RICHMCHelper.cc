
#include "RICHMCHelper.h"

#include <TFile.h>
#include <TDirectory.h>
#include <TRandom.h>
#include <stdexcept>
#include <array>
#include <iostream>


namespace {
	std::map<int, const char* > getChargedNames(){
		std::map<int, const char*> chargeNames;
		chargeNames[-1]                 = "Neg";
		chargeNames[+1]                 = "Pos";
		return chargeNames;
	}
	std::map<int, const char* > getPidNames(){
		std::map<int, const char*> pidNames;
		pidNames[RICH::PID::pion]       = "Pion";
		pidNames[RICH::PID::kaon]       = "Kaon";
		pidNames[RICH::PID::proton]     = "Proton";
		pidNames[RICH::PID::electron]   = "Electron";
		pidNames[RICH::PID::muon]       = "Muon";
		pidNames[RICH::PID::background] = "Background";
		pidNames[RICH::PID::nopid]      = "NoPID";
		return pidNames;
	}
}

const std::map<int, const char* > RICH::MCHelper::chargeNames = getChargedNames();
const std::map<int, const char* > RICH::MCHelper::pidNames = getPidNames();


RICH::MCHelper::MCHelper(const char* efficiencyMapFilename, const char* cutThreshold){

	if(efficiencyMapFilename != NULL and cutThreshold != NULL){

		std::cout << "\nRICH MC helper:" << std::endl;
		std::cout << "\tUsing RICH efficiency maps from '" << efficiencyMapFilename << "' for RICH MC." << std::endl;
		std::cout << std::endl;
		TFile* rfile = new TFile(efficiencyMapFilename);

		if (rfile->IsOpen()){
			if( rfile->FindKey(cutThreshold) == NULL ){
				rfile->Close();
				throw std::invalid_argument(Form("Cannot find cut threshold '%s' in efficiency-map file '%s'.", cutThreshold, efficiencyMapFilename));
			}

			TDirectory* dir = dynamic_cast<TDirectory*>(rfile->Get(cutThreshold));

			// TODO: random number generator?
			const std::vector<int> pidToLoad = {RICH::PID::pion, RICH::PID::kaon, RICH::PID::muon, RICH::PID::proton};
			for(std::vector<int>::const_iterator it = pidToLoad.begin(); it != pidToLoad.end(); ++it){
				const int realPID = *it;
				for(std::map<int, const char*>::const_iterator jt = chargeNames.begin(); jt != chargeNames.end(); ++jt){
					const int charge = jt->first;
					predictors_[realPID][charge] = RICH::Helper::Predictor(dir, realPID, charge);
				}
			}
			rfile->Close();
		}
		delete rfile;
	}
}


RICH::Helper::Predictor::Predictor(TDirectory* dir, const int realPID, const int charge){


	const std::map<int, const char*>& pidNames = RICH::MCHelper::pidNames;
	const std::map<int, const char*>& chargeNames = RICH::MCHelper::chargeNames;

	for(std::map<int, const char*>::const_iterator it = pidNames.begin(); it != pidNames.end(); ++it){
		const int outPID = it->first;
		const char* histname = Form("%s%s_effX_%s_0",pidNames.at(realPID), chargeNames.at(charge), pidNames.at(outPID));
		TH2D* hist = dynamic_cast<TH2D*>(dir->Get(histname));
		if( hist == NULL){
			throw std::invalid_argument(Form("Cannot find histogram '%s'.", histname));
		}

		hist->SetDirectory(0);
		efficiencyMaps[outPID] = hist;
	}

	const char* histname = Form("%s%s_effX_Sum_0",pidNames.at(realPID), chargeNames.at(charge));
	TH2D* hist = dynamic_cast<TH2D*>(dir->Get(histname));
	if( hist == NULL){
		throw std::invalid_argument(Form("Cannot find histogram '%s'.", histname));
	}
	hist->SetDirectory(0);
	validMap = hist;

}



int RICH::Helper::Predictor::predictPID(const double momAbs, const double theta)const{


	const double theta_sqrt = sqrt(theta);
	if( validMap->GetBinContent(validMap->FindBin(momAbs, theta_sqrt)) < 0.5){ // out of validity range
//		throw std::out_of_range(Form("p=%f GeV and theta=%f rad are out of range of the efficiency maps!", momAbs, theta));
		return RICH::PID::outOfKinematicRange;
	}

	// get interpolated probabilities
	std::array<double, 7> probs;
	double probSum = 0.0;
	for(int i=0; i<7; ++i){
		const double p = efficiencyMaps.at(i)->Interpolate(momAbs, theta_sqrt);
		probSum += p;
		probs[i] = p;
	}

	// times probSum to avoid normalizing the probs to one
	const double r = gRandom->Uniform()*probSum;
	int outPID = -1;
	double probComulative = 0.0;
	while (probComulative <= r){
		outPID += 1;
		probComulative += probs[outPID];
	}

	return outPID;
}
