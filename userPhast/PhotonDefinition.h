#ifndef PHOTONDEFINITION_H
#define PHOTONDEFINITION_H

#include "Phast.h"


class PhotonDefinition
{

public:

	PhotonDefinition(TTree& tree);
	~PhotonDefinition() { }

	void clear();

	void fillMC(const PaEvent& event, const PaMCvertex& primVertex);

private:

	Phast& _phast;

	static const int MC_PID_PHOTON = 1;   // Geant3 ID for photon
	static const int MC_PID_PI0    = 7;   // Geant3 ID for pi^0

	// output tree variables
	int                 _numberPhotonsMC;
	std::vector<double> _momentumMCX;
	std::vector<double> _momentumMCY;
	std::vector<double> _momentumMCZ;

};

#endif  // PHOTONDEFINITION_H
