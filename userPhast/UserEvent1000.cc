# include <cassert>

#include "TCanvas.h"
#include "TTree.h"

#include "ECALDefinition.h"
#include "BeamDefinition.h"
#include "PhotonDefinition.h"
#include "RecoilDefinition.h"
#include "RPDDefinition.h"
#include "ScatteredDefinition.h"
#include "SelectionHelper.h"
#include "VertexDefinition.h"


void
UserEvent1000(PaEvent& event)
{
	// Tree
	static TTree* tree = NULL;

	// Scattered particles
	static const size_t CUT_NUMBER_PARTICLES_CHARGED = 3;
	static const size_t NUMBER_PARTICLES_NEUTRAL     = 2;  // needed only for MC
	static const size_t CUT_MIN_NUMBER_ECAL_CLUSTERS = 4;

	// Target region
	static const double CUT_TARGET_UP   = -260;  // upstream boundary [cm]
	static const double CUT_TARGET_DOWN =  160;  // downstream boundary [cm]

	// MC omega preselection range
	static const double CUT_MC_OMEGA_MASS_UP   = 855;  // upper limit [MeV]
	static const double CUT_MC_OMEGA_MASS_DOWN = 705;  // lower limit [MeV]

	// Utility classes
	static SelectionHelper*     selectionHelper;
	static VertexDefinition*    vertexDefinition;
	static BeamDefinition*      beamDefinition;
	static ScatteredDefinition* scatteredDefinition;
	static RPDDefinition*       rpdDefinition;
	static RecoilDefinition*    recoilDefinition;
	static ECALDefinition*      ecalDefinition;
	static PhotonDefinition*    photonDefinition;

	// Initial configuration
	static bool first(true);
	if (first) {
		first = false;
		tree  = new TTree("UserEvent1000", "Events");
		assert(tree);

		selectionHelper     = new SelectionHelper    (*tree);
		vertexDefinition    = new VertexDefinition   (*tree);
		beamDefinition      = new BeamDefinition     (*tree);
		scatteredDefinition = new ScatteredDefinition(*tree, CUT_NUMBER_PARTICLES_CHARGED, NUMBER_PARTICLES_NEUTRAL, CUT_MC_OMEGA_MASS_UP, CUT_MC_OMEGA_MASS_DOWN);
		rpdDefinition       = new RPDDefinition      (*tree);
		recoilDefinition    = new RecoilDefinition   (*tree);
		ecalDefinition      = new ECALDefinition     (*tree);
		photonDefinition    = new PhotonDefinition   (*tree);

		Phast::Ref().HistFileDir("UserEvent");
	}
	// Set all tree variables to default values
	selectionHelper->clear();
	vertexDefinition->clear();
	beamDefinition->clear();
	scatteredDefinition->clear();
	rpdDefinition->clear();
	recoilDefinition->clear();
	ecalDefinition->clear();
	photonDefinition->clear();

	// Set MC truth
	if (event.IsMC()) {
		bool alreadyFilledMCInfo = false;
		for (int iMCvertex = 0; iMCvertex < event.NMCvertex(); ++iMCvertex) {
			const PaMCvertex& vertexMC = event.vMCvertex(iMCvertex);
			if (vertexMC.IsPrimary() and not vertexMC.IsPileup()) {
				assert(alreadyFilledMCInfo == false);  // ensure that there is exactly 1 primary vertex in MC
				if(!scatteredDefinition->checkMCOmega(event, vertexMC)) {
					return;
				}
				vertexDefinition->fillMC   (event, vertexMC);
				beamDefinition->fillMC     (event, vertexMC);
				scatteredDefinition->fillMC(event, vertexMC);
				recoilDefinition->fillMC   (event, vertexMC);
				photonDefinition->fillMC   (event, vertexMC);
				alreadyFilledMCInfo = true;
			}
		}
	}

	// Statistics
	selectionHelper->fillStatistics("Total");

	// DT0 trigger
	if (not selectionHelper->isDT0()) {
		if (event.IsMC()) {
			selectionHelper->handleMCEvent(*tree, false, event);
		}
		return;
	}
	selectionHelper->fillStatistics("DT0");

	// Require exactly one (i.e. best) primary vertex
	const int numberPrimaryVertex = vertexDefinition->getNumberPrimaryVertices(event);
	const int iBestPrimaryVertex  = event.iBestPrimaryVertex();
	if (numberPrimaryVertex != 1 or iBestPrimaryVertex == -1) {
		if (event.IsMC()) {
			selectionHelper->handleMCEvent(*tree, false, event);
		}
		return;
	}
	const PaVertex& primVertex = event.vVertex(iBestPrimaryVertex);
	selectionHelper->fillStatistics("One vertex");

	// Vertex cut around target
	if (not (CUT_TARGET_UP <= primVertex.Z() and primVertex.Z() <= CUT_TARGET_DOWN)) {
		if (event.IsMC()) {
			selectionHelper->handleMCEvent(*tree, false, event);
		}
		return;
	}
	selectionHelper->fillStatistics("Target");

	// Number of outgoing particles in primary vertex
	if (primVertex.NOutParticles() != (int)CUT_NUMBER_PARTICLES_CHARGED) {
		if (event.IsMC()) {
			selectionHelper->handleMCEvent(*tree, false, event);
		}
		return;
	}
	selectionHelper->fillStatistics("Outgoing");

	// Beam and scattered particle
	const PaParticle& beamParticle = beamDefinition->getBeamParticle(event, primVertex);
	const vector<PaParticle>& scatteredParticles = scatteredDefinition->getScatteredParticles(event, primVertex);
	if (not selectionHelper->ensureChargeConservation(event, iBestPrimaryVertex, beamParticle, scatteredParticles)) {
		if (event.IsMC()) {
			selectionHelper->handleMCEvent(*tree, false, event);
		}
		return;
	}
	selectionHelper->fillStatistics("Charge");

	// Best proton in RPD
	rpdDefinition->initRPD(event, primVertex);  // needed for functions below
	if (not rpdDefinition->cutHasBestProton()) {
		if (event.IsMC()) {
			selectionHelper->handleMCEvent(*tree, false, event);
		}
		return;
	}
	selectionHelper->fillStatistics("RPD proton");

	// Number of neutral clusters in event
	if (ecalDefinition->getNumberNeutralClusters(event) < CUT_MIN_NUMBER_ECAL_CLUSTERS) {
		if (event.IsMC()) {
			selectionHelper->handleMCEvent(*tree, false, event);
		}
		return;
	}
	selectionHelper->fillStatistics("ECAL Cluster");

	// Fill accepted events
	vertexDefinition->fill   (event, primVertex);
	beamDefinition->fill     (event);
	scatteredDefinition->fill(event, scatteredParticles, primVertex);
	rpdDefinition->fill      (event, primVertex);
	ecalDefinition->fill     (event);
	if (event.IsMC()) {
		selectionHelper->handleMCEvent(*tree, true, event);
	} else {
		selectionHelper->fill(event);
		tree->Fill();
		Phast::Ref().h_file = tree->GetCurrentFile();
	}
}
