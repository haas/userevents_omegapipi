#ifndef RPDDEFINITIONS_H
#define RPDDEFINITIONS_H

#include "TF1.h"
#include "TH1D.h"
#include "TSpline.h"
#include "TString.h"

#include "Phast.h"

#include "RPD_Helper.h"


class RPDDefinition
{

public:

	RPDDefinition(TTree& tree);
	~RPDDefinition() { };

	void clear();

	void initRPD(const PaEvent& event, const PaVertex& primVertex);
	void fill   (const PaEvent& event, const PaVertex& primVertex);
	bool cutHasBestProton() const;

private:

	enum energyCorrectionMethod {noCorrection = 0, oldCorrection = 1, splineEnergyLoss = 2, splineEnergyLossHeun = 3, splineEnergyLossRK4 = 4, splineRange = 5};
	enum targetType             {hydrogenTarget = 0, diskTarget = 1};
	enum materialType           {hydrogen = 0, aluminium = 1, mylar = 2, scintillator = 3, lead = 4, tungsten = 5};
	enum integrationMethod      {euler = 0, heun = 1, RK4 = 2};
	double getEnergy(const energyCorrectionMethod useMethod) const;
	double getCorrectedEnergy(const energyCorrectionMethod useMethod, const PaEvent& event, const PaVertex& primVertex) const;
	static double getCorrectedEnergyFunction         (const double totalEnergy, const materialType material, const double traversedThickness, const energyCorrectionMethod useMethod);
	static double getCorrectedEnergySplineEnergyLoss (const double totalEnergy, const materialType material, const double traversedThickness, const integrationMethod intMeth);
	static double calcLoss                           (const TSpline3& spline, const double Ekin, const double traversedThickness, const int recursion, const integrationMethod intMeth);
	static double getCorrectedEnergySplineRange      (const double totalEnergy, const materialType material, const double traversedThickness);
	static double getInterpolatedEnergy              (const double totalEnergy, const materialType material, const double traversedThickness);
	static double interpolate3                       (const double xx1, const double xx2, const double xx3, const double yy1, const double yy2, const double yy3, const double x);

	Phast& _phast;
	RPD&   _rpd;

	// output tree variables
	int    _numberTracks;
	int    _hasTracks;
	int    _indexBestProton;
	double _momentumX;
	double _momentumY;
	double _momentumZ;
	double _energyNoCorrection;
	double _energyOldCorrection;
	double _energyELossCorrection;
	double _energyELossCorrectionHeun;
	double _energyELossCorrectionRK4;
	double _energyRangeCorrection;
	double _vertexTime;
	double _vertexPositionZ;
	int    _hitsRingA;
	int    _hitsRingB;
	double _hitRingAPositionZ;
	double _hitRingBPositionZ;
	double _energyLossRingA;
	double _energyLossRingB;

};

#endif  // RPDDEFINITIONS_H
