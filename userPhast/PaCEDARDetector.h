#ifndef PaCEDARDetector_h
#define PaCEDARDetector_h
#include <string>
#include <bitset>
#include <vector>
#include "TFile.h"
#include "TDirectory.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TTree.h"
#ifdef PHAST_HOME
#include "PaDigit.h"
#include "Phast.h"
#include "PaEvent.h"
#endif

#if __cplusplus > 199711L
#include <unordered_map>
#define PaCEDARDetectorMapType std::unordered_map
#else
#include <map>
#define PaCEDARDetectorMapType std::map
#endif

#include "PaCEDARLikelihood.h"

class PaCEDARDetector;

namespace PaCEDARDetectorCalibration {
class PerRunData{
public:
	PerRunData();
	double getInclinationCalib(const int i_CEDAR, const int i_XY) const{return _inclinationCalib[i_CEDAR][i_XY]; }
	double getInclinationCalibX(const int i_CEDAR) const{return getInclinationCalib(i_CEDAR, 0); }
	double getInclinationCalibY(const int i_CEDAR) const{return getInclinationCalib(i_CEDAR, 1); }
	double getTimeCalibOffset(const int i_CEDAR, const int i_PMT) const {return _timeCalibOffset[i_CEDAR][i_PMT]; }
	double getTimeCalibSigma(const int i_CEDAR, const int i_PMT) const {return _timeCalibSigma[i_CEDAR][i_PMT]; }
	const Cedar::Likelihood& getLikelihood(const int i_CEDAR) const;

	void setInclinationCalib(const int i_CEDAR, const int i_XY, const double val) {_inclinationCalib[i_CEDAR][i_XY] = val; }
	void setTimeCalibOffset(const int i_CEDAR, const int i_PMT, const double val) {_timeCalibOffset[i_CEDAR][i_PMT] = val; }
	void setTimeCalibSigma(const int i_CEDAR, const int i_PMT, const double val) {_timeCalibSigma[i_CEDAR][i_PMT] = val; }
	void setInclinationCalibX(const int i_CEDAR, const double value) {setInclinationCalib(i_CEDAR, 0, value); }
	void setInclinationCalibY(const int i_CEDAR, const double value) {setInclinationCalib(i_CEDAR, 1, value); }
	void setLikelihood(const int i_CEDAR, const Cedar::Likelihood& likelihood);
	void unsetLikelihood(const int i_CEDAR);

protected:
	double _inclinationCalib[2][2];
	double _timeCalibOffset[2][8];
	double _timeCalibSigma[2][8];
	Cedar::Likelihood _likelihood[2];
	int _likelihoodID[2];

};

/**
 * Handles the reading and writing of per-run calibration data from and to the root file.
 * Store the calibration of the current run.
 * The current run is selected by the getEntry method.
 */
class PerRunDataHandler: public PerRunData{
public:
	PerRunDataHandler(const char* calibFilename, const bool write= false );
	int getRunNumber() const {return _runNumber;}
	int getRunAvgTimestamp() const{	return _runAvgTimestamp;}

	void setRunNumber( const int runNumber ) {_runNumber = runNumber;}
	void setRunAvgTimestamp(int runAvgTimestamp){_runAvgTimestamp = runAvgTimestamp;}

	Long64_t getEntries() const {return _calibTree->GetEntries(); }
	void getEntry( const Long64_t i) { _calibTree->GetEntry(i); _setLikelihoodParameters();}

	void setLikelihoodParameterErrors(const int i_CEDAR, const std::vector<double>& parameterErrors);
	void unsetLikelihoodParameterErrors(const int i_CEDAR);
	std::vector<double> getLikelihoodParameterErrors(const int i_CEDAR);

	void fill(){
		_getLikelihoodParameters();
		if( _write) _calibTree->Fill();
		else throw std::runtime_error("PaCEDARCalibData is not created for writing!");
	}

	void close(){
		if( _write){
			_calibFile->Write();
		}
		_calibFile->Close();
	}


private:
	void _addBranchToTree(double& address, const char* format, const int i_CEDAR, const int i_PMT = -1 );
	void _setLikelihoodParameters();
	void _getLikelihoodParameters();

	int _runNumber;
	int _runAvgTimestamp;

	const bool _write;
	TFile* _calibFile;
	TTree* _calibTree;
	double _likelihoodParameters[2][128];
	double _likelihoodParameterErrors[2][128];
};

// Class store and loads mapping for PMTs
// It Performs NO consistency checks of maps when reading them
class ConstantDataHandler{
public:
	ConstantDataHandler(const char* calibFilename, const bool write= false );
	int getRemap(const int iCEDAR, const int iPMT) const { return _remap[iCEDAR][iPMT]; }
	double getTransportMatrixElem(const int iCEDAR, const int iElem) const { return _transportMatrix[iCEDAR][iElem]; }

	void setRemap(const int iCEDAR, const int iPMT, const int pmt){ _remap[iCEDAR][iPMT] = pmt;}
	void setTransitionMatrixElem(const int iCEDAR, const int iElem, const double val){ _transportMatrix[iCEDAR][iElem] = val; }

	void close();
	void checkRemap();

private:
	int _remap[2][8];
	double _transportMatrix[2][65];
	const bool _write;
	TFile* _calibFile;
	TTree* _calibTree;
};
}

class PaCEDARDetector {
public:

	typedef std::pair<Cedar::LikelihoodResult,Cedar::LikelihoodResult> Likelihoods;


	class NoCalibrationError: public std::logic_error{
	public:
		NoCalibrationError(const std::string& w): std::logic_error(w){}
	};

	PaCEDARDetector();
	virtual ~PaCEDARDetector() {
	}
	void readCalibration(const std::string &fileName);
	void readDCS(const std::string& filename);
	#ifdef PHAST_HOME
	virtual void readDigits(const PaEvent& event, bool forceRaw){ _readDigits(event, forceRaw, NULL, NULL); }
	virtual void initEvent( const PaEvent& event, const PaTPar& beamTrackParameter, const bool foreRaw);
	virtual void initMCEvent( const PaEvent& event, const PaTPar& beamTrackParameter);
	#endif
	virtual void initDXDY(const double beamPar[5]);
	void Gethitmask(unsigned char& CE1PMn, unsigned char& CE2PMn) {
		CE1PMn = hitMask_[0];
		CE2PMn = hitMask_[1];
	}
	void Getmajority(int& CE1Mj, int& CE2Mj) const {
		CE1Mj = ((hitMask_[0] & 1) + (hitMask_[0] & 2) / 2 + (hitMask_[0] & 4) / 4 + (hitMask_[0] & 8) / 8 + (hitMask_[0] & 16) / 16 + (hitMask_[0] & 32) / 32
		        + (hitMask_[0] & 64) / 64 + (hitMask_[0] & 128) / 128);
		CE2Mj = ((hitMask_[1] & 1) + (hitMask_[1] & 2) / 2 + (hitMask_[1] & 4) / 4 + (hitMask_[1] & 8) / 8 + (hitMask_[1] & 16) / 16 + (hitMask_[1] & 32) / 32
		        + (hitMask_[1] & 64) / 64 + (hitMask_[1] & 128) / 128);
	}
	void getDXDY(double& dxCEDAR, double& dyCEDAR)const{ dxCEDAR = dxCEDAR_; dyCEDAR = dyCEDAR_;}
	void getDXDYcor(double& dxCEDAR1, double& dyCEDAR1, double& dxCEDAR2, double& dyCEDAR2) const ;
	void getThetacor(double& thetaCEDAR1, double& thetaCEDAR2);

	#ifdef PHAST_HOME
	void getPreassureAndMeanTemp(const PaEvent& event, int& event_timestamp, double& PCEDAR1, double& PCEDAR2, double& TempCEDAR1, double& TempCEDAR2);
	#endif
	double getPressureCE1(const int timestamp, const bool interpolate = false) const;
	double getTempMeanCE1(const int timestamp, const bool interpolate = false) const;
	double getTempHeadCE1(const int timestamp, const bool interpolate = false) const;
	double getTempMiddleCE1(const int timestamp, const bool interpolate = false) const;
	double getTempTailCE1(const int timestamp, const bool interpolate = false) const;
	double getPressureCE2(const int timestamp, const bool interpolate = false) const;
	double getTempMeanCE2(const int timestamp, const bool interpolate = false) const;
	double getTempHeadCE2(const int timestamp, const bool interpolate = false) const;
	double getTempMiddleCE2(const int timestamp, const bool interpolate = false) const;
	double getTempTailCE2(const int timestamp, const bool interpolate = false) const;

	Likelihoods getLikelihoods() const;

	const std::map<int, double>& getPressureMapCE1() const {
		return DCS_CEDAR1_preasure_;
	}
	const std::map<int, double>& getPressureMapCE2() const {
		return DCS_CEDAR2_preasure_;
	}

	std::vector<char> getPid() {
		std::vector<char> vec(CPID_, CPID_ + 2);
		return vec;
	}
	void setDxDy(const double dxCED, const double dyCED) {
		dxCEDAR_ = dxCED;
		dyCEDAR_ = dyCED;
	}
	void setDxDycor(const double dxCED1, const double dyCED1, const double dxCED2, const double dyCED2) {
		dxCEDARcor_[0] = dxCED1;
		dyCEDARcor_[0] = dyCED1;
		dxCEDARcor_[1] = dxCED2;
		dyCEDARcor_[1] = dyCED2;
	}
	void setPMTGroups(unsigned char CE1PMna, unsigned char CE2PMna) {
		hitMask_[0] = CE1PMna;
		hitMask_[1] = CE2PMna;
	}
	void printCedarInfo() {
		for (unsigned int i = 0; i < 4; ++i)
			std::cout << cedarInfo[0][i] << "  ";
		for (unsigned int i = 0; i < 4; ++i)
			std::cout << cedarInfo[1][i] << "  ";
		std::cout << std::endl;
	}

protected:


	PaCEDARDetectorMapType<int,PaCEDARDetectorCalibration::PerRunData> perRunCalib_;

	PaCEDARDetectorCalibration::PerRunData MCCalib_;

#ifdef PHAST_HOME
	void _readDigits(const PaEvent &event, bool forceRaw, TH1D* timeHist[2][8], TH1D* timeOfRunHist);
#endif
	double CPLOG[2];
	double CKLOG[2];
	int CG[2][9];
	double nSigma_;
	double dxCEDAR_;
	double dyCEDAR_;
	double dxCEDARcor_[2];
	double dyCEDARcor_[2];
	const PaCEDARDetectorCalibration::PerRunData* currentCalib_;
	std::vector<double> transpMat_[2];
	unsigned char hitMask_[2];
	int remap_[2][8];
	bool storeHistos_;

	double pilike_[2][9][3][13];
	double kaonlike_[2][9][3][13];
	std::vector<float> cedarInfo[2];
	char CPID_[3];

	TF1 *CLIM_[2][8];
	bool CisC[2];

	bool isCalibrationObject;

	// DCS informations
	std::map<int, double> DCS_CEDAR1_preasure_;
	std::map<int, double> DCS_CEDAR1_temp_head_;
	std::map<int, double> DCS_CEDAR1_temp_middle_;
	std::map<int, double> DCS_CEDAR1_temp_tail_;
	std::map<int, double> DCS_CEDAR1_temp_room_;
	std::map<int, double> DCS_CEDAR1_temp_mean_;
	std::map<int, double> DCS_CEDAR2_preasure_;
	std::map<int, double> DCS_CEDAR2_temp_head_;
	std::map<int, double> DCS_CEDAR2_temp_middle_;
	std::map<int, double> DCS_CEDAR2_temp_tail_;
	std::map<int, double> DCS_CEDAR2_temp_room_;
	std::map<int, double> DCS_CEDAR2_temp_mean_;


};


class PaCEDARDetectorCalib: public ::PaCEDARDetector {
public:

	PaCEDARDetectorCalib(const std::string& outFileName = std::string("of.root"));
#ifdef PHAST_HOME
	virtual void readDigits(const PaEvent &event, bool forceRaw){ _readDigits(event, forceRaw, hTime_, hTimeOfRun_); }
#endif
	virtual void initDXDY(const double beamPar[5]);
	void drawPhiCorrelation();

	TFile* getOutputFile() {
		return of;
	}

	virtual ~PaCEDARDetectorCalib() {
	}

private:
	TH1D* hDx_[4];
	TH1D* hDy_[4];
	TH1D* hTime_[2][8];
	TH1D* hTimeOfRun_;
	TH2D* hPhiNum_[2];
	TH3D* hPhiRU_[2];
	TH2D* hPhiR_[2];
	TFile* of;
};


#endif
