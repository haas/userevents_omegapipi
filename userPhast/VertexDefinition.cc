#include "VertexDefinition.h"


VertexDefinition::VertexDefinition(TTree& tree)
	: _phast(Phast::Ref())
{
	clear();
	_phast.h_file->cd();

	tree.Branch("Vertex_Number",      &_numberVertices,   "numberVertices/I");
	tree.Branch("Vertex_Outgoing",    &_numberOutgoing,   "numberOutgoing/I");
	tree.Branch("Vertex_X",           &_vertexX,          "vertexX/D");
	tree.Branch("Vertex_Y",           &_vertexY,          "vertexY/D");
	tree.Branch("Vertex_Z",           &_vertexZ,          "vertexZ/D");
	tree.Branch("Vertex_Chi2red",     &_vertexChi2red,    "vertexChi2red/D");
	tree.Branch("Vertex_MC_Number",   &_numberMCVertices, "numberMCVertices/I");
	tree.Branch("Vertex_MC_Outgoing", &_numberMCOutgoing, "numberMCOutgoing/I");
	tree.Branch("Vertex_MC_X",        &_vertexMCX,        "vertexMCX/D");
	tree.Branch("Vertex_MC_Y",        &_vertexMCY,        "vertexMCY/D");
	tree.Branch("Vertex_MC_Z",        &_vertexMCZ,        "vertexMCZ/D");
}


void
VertexDefinition::clear()
{
	const double failValue = std::numeric_limits<double>::quiet_NaN();
	_numberVertices   = 0;
	_numberOutgoing   = 0;
	_vertexX          = failValue;
	_vertexY          = failValue;
	_vertexZ          = failValue;
	_vertexChi2red    = failValue;
	_numberMCVertices = 0;
	_numberMCOutgoing = 0;
	_vertexMCX        = failValue;
	_vertexMCY        = failValue;
	_vertexMCZ        = failValue;
}


void
VertexDefinition::fill(const PaEvent&  event,
                       const PaVertex& primVertex)
{
	_numberVertices = getNumberPrimaryVertices(event);
	_numberOutgoing = primVertex.NOutParticles();
	_vertexX        = primVertex.X();
	_vertexY        = primVertex.Y();
	_vertexZ        = primVertex.Z();
	_vertexChi2red  = primVertex.Chi2() / primVertex.Ndf();
}


void
VertexDefinition::fillMC(const PaEvent&    event,
                         const PaMCvertex& primVertex)
{
	_numberMCVertices = getNumberPrimaryMCVertices(event);
	_numberMCOutgoing = primVertex.NMCtrack();
	_vertexMCX        = primVertex.Pos(0);
	_vertexMCY        = primVertex.Pos(1);
	_vertexMCZ        = primVertex.Pos(2);
}


int
VertexDefinition::getNumberPrimaryVertices(const PaEvent& event) const
{
	int numberPrimaryVertices = 0;
	for (int i = 0; i < event.NVertex(); ++i) {
		const PaVertex& paVertex = event.vVertex(i);
		if (paVertex.IsPrimary()) {
			numberPrimaryVertices++;
		}
	}
	return numberPrimaryVertices;
}


int
VertexDefinition::getNumberPrimaryMCVertices(const PaEvent& event) const
{
	int numberPrimaryVertices = 0;
	const vector<PaMCvertex>& mcVertices = event.vMCvertex();
	for (int i = 0; i < event.NMCvertex(); ++i) {
		const PaMCvertex& paMCvertex = mcVertices[i];
		if (paMCvertex.IsPrimary()) {
			numberPrimaryVertices++;
		}
	}
	return numberPrimaryVertices;
}
