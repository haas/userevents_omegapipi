#include "PhotonDefinition.h"


PhotonDefinition::PhotonDefinition(TTree& tree)
	: _phast(Phast::Ref())
{
	clear();
	_phast.h_file->cd();

	tree.Branch("Photon_numberPhotonsMC", &_numberPhotonsMC, "numberPhotonsMC/I");
	tree.Branch("Photon_momentumMCX",     &_momentumMCX);
	tree.Branch("Photon_momentumMCY",     &_momentumMCY);
	tree.Branch("Photon_momentumMCZ",     &_momentumMCZ);
}


void
PhotonDefinition::clear()
{
	_numberPhotonsMC = 0;
	_momentumMCX.clear();
	_momentumMCY.clear();
	_momentumMCZ.clear();
}


void
PhotonDefinition::fillMC(const PaEvent&    event,
                         const PaMCvertex& primVertex)
{
	// find pi^0 decay vertices
	std::vector<int> pi0VertexIndices;
	for (int i = 0; i < primVertex.NMCtrack(); ++i) {
		const PaMCtrack& track = event.vMCtrack(primVertex.iMCtrack(i));
		if (track.Pid() == MC_PID_PI0
		    and not track.IsPileup()
		    and not track.IsBeam()
		    and track.vMCvertex().size() == 1) {
			pi0VertexIndices.push_back(track.vMCvertex()[0]);
		}
	}

	// get photons from pi^0 decays
	_numberPhotonsMC = 2 * pi0VertexIndices.size();
	_momentumMCX.resize(_numberPhotonsMC);
	_momentumMCY.resize(_numberPhotonsMC);
	_momentumMCZ.resize(_numberPhotonsMC);
	for (size_t i = 0; i < pi0VertexIndices.size(); ++i) {
		const PaMCvertex& decayVertex = event.vMCvertex(pi0VertexIndices[i]);
		if (decayVertex.IsPileup() or decayVertex.NMCtrack() != 2) {
			continue;
		}
		const PaMCtrack& track1 = event.vMCtrack(decayVertex.iMCtrack(0));
		const PaMCtrack& track2 = event.vMCtrack(decayVertex.iMCtrack(1));
		if (track1.Pid() != MC_PID_PHOTON or track2.Pid() != MC_PID_PHOTON) {
			continue;
		}
		_momentumMCX[2 * i    ] = track1.Px();
		_momentumMCY[2 * i    ] = track1.Py();
		_momentumMCZ[2 * i    ] = track1.Pz();
		_momentumMCX[2 * i + 1] = track2.Px();
		_momentumMCY[2 * i + 1] = track2.Py();
		_momentumMCZ[2 * i + 1] = track2.Pz();
	}
}
