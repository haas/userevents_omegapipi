/*
 * Helper class to handle CEDAR Pid for MC events
 * using efficiency maps from data.
 * gRandom is used to generate random numbers
 */


#ifndef CEDAR_MCHELPER_H_
#define CEDAR_MCHELPER_H_

#include<stdexcept>

#include<TH2D.h>
#include<TFile.h>
#include<TRandom.h>

namespace CEDARHelper{
	class MCHelper;
}




class CEDARHelper::MCHelper{
public:
	enum pid {pion = 0, kaon = 1, noPID = 6};
	/***
	 *
	 * @param mapsFilename: Path to the root file containing the efficiency maps
	 * @param thresholdKaonID: Combined-likelihood threshold that was used for kaon PID in the data
	 * @param thresholdPionID: Combined-likelihood threshold that was used for pion PID in the data
	 */
	MCHelper(const char* mapsFilename, const char* thresholdKaonID, const char* thresholdPionID):
	mapKaon_Kaon_(NULL),
	mapPion_Pion_(NULL),
	mapKaon_Pion_(NULL),
	mapPion_Kaon_(NULL){
	if(mapsFilename){
		TFile* f = new TFile(mapsFilename);

		if(f->IsOpen()){
			mapKaon_Kaon_ = dynamic_cast<TH2D*>(f->Get(Form("maps/%s,%s/CEDAR1/Kaon_Kaon",thresholdKaonID,thresholdPionID)));
			mapPion_Pion_ = dynamic_cast<TH2D*>(f->Get(Form("maps/%s,%s/CEDAR1/Pion_Pion",thresholdKaonID,thresholdPionID)));
			mapKaon_Pion_ = dynamic_cast<TH2D*>(f->Get(Form("maps/%s,%s/CEDAR1/Kaon_Pion",thresholdKaonID,thresholdPionID)));
			mapPion_Kaon_ = dynamic_cast<TH2D*>(f->Get(Form("maps/%s,%s/CEDAR1/Pion_Kaon",thresholdKaonID,thresholdPionID)));

		}
	}
}

	int predict(const int realPID, const double dXdZ_at_CEDAR1, const double dYdZ_at_CEDAR1)const;



private:
	TH2D* mapKaon_Kaon_;
	TH2D* mapPion_Pion_;
	TH2D* mapKaon_Pion_;
	TH2D* mapPion_Kaon_;
};





inline int CEDARHelper::MCHelper::predict(const int realPID, const double dXdZ_at_CEDAR1, const double dYdZ_at_CEDAR1)const{
	if(mapKaon_Kaon_==NULL or mapPion_Pion_== NULL or mapKaon_Pion_==NULL or mapPion_Kaon_==NULL){
		throw std::invalid_argument("Can not find at least one of the maps for CEDAR PID");
	}

	if(realPID != pion and realPID != kaon){
		throw std::invalid_argument("Unknown real PID given to CEDAR MC helper");
	}

	TH2D* hID = (realPID == kaon)? mapKaon_Kaon_ : mapPion_Pion_;
	TH2D* hMissID = (realPID == kaon)? mapKaon_Pion_ : mapPion_Kaon_;

	const int binxID = hID->GetXaxis()->FindBin(dXdZ_at_CEDAR1);
	const int binyID = hID->GetYaxis()->FindBin(dYdZ_at_CEDAR1);
	const int binxMissID = hMissID->GetXaxis()->FindBin(dXdZ_at_CEDAR1);
	const int binyMissID = hMissID->GetYaxis()->FindBin(dYdZ_at_CEDAR1);
	if(binxID > 0 and binxID <= hID->GetNbinsX() and
	   binyID > 0 and binyID <= hID->GetNbinsY() and
	   binxMissID > 0 and binxMissID <= hMissID->GetNbinsX() and
	   binyMissID > 0 and binyMissID <= hMissID->GetNbinsY() 	){

		double p_ID = hID->Interpolate(dXdZ_at_CEDAR1, dYdZ_at_CEDAR1);
		double p_MissID = hMissID->Interpolate(dXdZ_at_CEDAR1, dYdZ_at_CEDAR1);
		if(p_ID + p_MissID > 1.0){ // if p_sum > 1, decrease the p_MissID as p_ID is determined much better
			p_MissID = 1.0-p_ID;
		}

		const double r = gRandom->Uniform();
		if(r < p_ID){
			return realPID;
		} else if (r < p_ID+p_MissID){
			return (realPID==kaon)? pion : kaon;
		} else {
			return noPID;
		}
	} else {
		return noPID;
	}

}

#endif
