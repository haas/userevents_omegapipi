#ifndef VERTEXDEFINITION_H
#define VERTEXDEFINITION_H

#include "TH1D.h"
#include "TH2D.h"

#include "Phast.h"


class VertexDefinition
{

public:

	VertexDefinition(TTree& tree);
	~VertexDefinition() { };

	void clear();

	void fill  (const PaEvent& event, const PaVertex&   primVertex);
	void fillMC(const PaEvent& event, const PaMCvertex& primVertex);

	int getNumberPrimaryVertices  (const PaEvent& event) const;

private:

	int getNumberPrimaryMCVertices(const PaEvent& event) const;

	Phast& _phast;

	// output tree variables
	int    _numberVertices;
	int    _numberOutgoing;
	double _vertexX;
	double _vertexY;
	double _vertexZ;
	double _vertexChi2red;
	int    _numberMCVertices;
	int    _numberMCOutgoing;
	double _vertexMCX;
	double _vertexMCY;
	double _vertexMCZ;

};

#endif  // VERTEXDEFINITION_H
