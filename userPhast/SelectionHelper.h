#ifndef SELECTIONHELPER_H
#define SELECTIONHELPER_H

#include "TH1D.h"

#include "Phast.h"
#include "RPD_Helper.h"

using namespace std;


class SelectionHelper
{

public:

	SelectionHelper(TTree& tree);

	virtual ~SelectionHelper() { }

	void clear();

	void fill(const PaEvent &event);
	void fillStatistics(const TString& name) { _hStatistics->Fill(name, 1); }

	void handleMCEvent(TTree& tree, const bool accepted, const PaEvent& event);

	static bool ensureChargeConservation(const PaEvent& event, const int primVertexIndex, const PaParticle& beamParticle, const vector<PaParticle>& particles);
	static bool isDT0() { return RPD::Instance().IsDT0(); }
	static std::string getYear();

	static bool isBadRun(const PaEvent& event);

private:

	void setIsAcceptedMC(const int  accepted) { _isAcceptedMC = accepted; }
	void setIsAcceptedMC(const bool accepted) { setIsAcceptedMC((int)accepted); }

	static bool hasTrackParameters(const PaParticle& particle) { return particle.NFitPar() == 0; }

	static std::map<int, std::vector<int>> _badRunList;

	Phast& _phast;

	TH1D* _hStatistics;

	// output tree variables
	int    _runNumber;
	int    _spillNumber;
	int    _eventInSpill;
	int    _triggerMask;
	int    _masterTrigger;
	int    _DT0;
	double _tcsPhase;
	double _timeInSpill;
	int    _isAcceptedMC;

};

#endif  // SELECTIONHELPER_H
