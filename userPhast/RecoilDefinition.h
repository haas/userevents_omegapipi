#ifndef RECOILDEFINITION_H
#define RECOILDEFINITION_H

#include "Phast.h"


class RecoilDefinition
{
public:

	RecoilDefinition(TTree& tree);
	~RecoilDefinition() { }

	void clear();

	void fillMC(const PaEvent& event, const PaMCvertex& primVertex);

private:

	const PaMCtrack& getRecoilTrackMC(const PaEvent& event, const PaMCvertex& primVertex);

	Phast& _phast;

	static const int MC_PID_PROTON = 14;  // Geant3 ID for proton

	// output tree variables
	double _momentumMCX;
	double _momentumMCY;
	double _momentumMCZ;

};

#endif  // RECOILDEFINITION_H
