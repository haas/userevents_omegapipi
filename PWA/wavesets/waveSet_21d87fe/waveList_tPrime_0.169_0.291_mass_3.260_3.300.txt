[1-,0-,0+]=[multiChannelRelativisticBreitWigner[rho_1450_-]=[omega_782_0[1,1]pi-][1,1]pi0]
[1-,0-,0+]=[rho_770_-=[pi-[1,0]pi0][1,1]omega_782_0]
[1-,1+,0+]=[multiChannelRelativisticBreitWigner[b1_1235_-]=[omega_782_0[0,1]pi-][1,1]pi0]
[1-,1+,0+]=[multiChannelRelativisticBreitWigner[rho_1450_-]=[pi-[1,0]pi0][2,1]omega_782_0]
[1-,2+,1+]=[multiChannelRelativisticBreitWigner[b1_1235_-]=[omega_782_0[0,1]pi-][1,1]pi0]
[1-,2+,1+]=[multiChannelRelativisticBreitWigner[rho_1450_-]=[omega_782_0[1,1]pi-][2,1]pi0]
[1-,2+,1+]=[rho_770_-=[pi-[1,0]pi0][0,2]omega_782_0]
[1-,2+,1+]=[rho_770_-=[pi-[1,0]pi0][2,0]omega_782_0]
[1-,2-,0+]=[multiChannelRelativisticBreitWigner[rho_1450_-]=[omega_782_0[1,1]pi-][3,1]pi0]
[1-,2-,0+]=[rho_770_-=[pi-[1,0]pi0][1,1]omega_782_0]
[1-,2-,0+]=[rho_770_-=[pi-[1,0]pi0][3,1]omega_782_0]
[1-,3+,0+]=[multiChannelRelativisticBreitWigner[b1_1235_-]=[omega_782_0[0,1]pi-][3,1]pi0]
[1-,3+,0+]=[multiChannelRelativisticBreitWigner[rho3_1690_-]=[omega_782_0[3,1]pi-][0,3]pi0]
[1-,3+,0+]=[multiChannelRelativisticBreitWigner[rho3_1690_-]=[omega_782_0[3,1]pi-][2,3]pi0]
[1-,3+,0+]=[multiChannelRelativisticBreitWigner[rho_1450_-]=[omega_782_0[1,1]pi-][2,1]pi0]
[1-,3+,0+]=[multiChannelRelativisticBreitWigner[rho_1450_-]=[omega_782_0[1,1]pi-][4,1]pi0]
[1-,3+,0+]=[rho_770_-=[pi-[1,0]pi0][2,1]omega_782_0]
[1-,3+,0+]=[rho_770_-=[pi-[1,0]pi0][4,1]omega_782_0]
[1-,3+,0+]=[rho_770_-=[pi-[1,0]pi0][4,2]omega_782_0]
[1-,4+,1+]=[multiChannelRelativisticBreitWigner[b1_1235_-]=[omega_782_0[0,1]pi-][3,1]pi0]
[1-,4+,1+]=[multiChannelRelativisticBreitWigner[rho_1450_-]=[omega_782_0[1,1]pi-][4,1]pi0]
[1-,4+,1+]=[rho_770_-=[pi-[1,0]pi0][2,2]omega_782_0]
[1-,4-,0+]=[multiChannelRelativisticBreitWigner[b1_1235_-]=[omega_782_0[0,1]pi-][4,1]pi0]
[1-,4-,0+]=[multiChannelRelativisticBreitWigner[rho3_1690_-]=[omega_782_0[3,1]pi-][1,3]pi0]
[1-,4-,0+]=[rho_770_-=[pi-[1,0]pi0][3,1]omega_782_0]
[1-,4-,0+]=[rho_770_-=[pi-[1,0]pi0][5,1]omega_782_0]
[1-,5+,0+]=[multiChannelRelativisticBreitWigner[rho3_1690_-]=[omega_782_0[3,1]pi-][2,3]pi0]
[1-,5+,0+]=[multiChannelRelativisticBreitWigner[rho_1450_-]=[omega_782_0[1,1]pi-][4,1]pi0]
[1-,5+,0+]=[multiChannelRelativisticBreitWigner[rho_1450_-]=[omega_782_0[1,1]pi-][6,1]pi0]
[1-,5+,0+]=[multiChannelRelativisticBreitWigner[rho_1450_-]=[pi-[1,0]pi0][4,2]omega_782_0]
[1-,5+,0+]=[rho_770_-=[pi-[1,0]pi0][6,2]omega_782_0]
[1-,6+,1+]=[multiChannelRelativisticBreitWigner[b1_1235_-]=[omega_782_0[0,1]pi-][5,1]pi0]
[1-,6+,1+]=[multiChannelRelativisticBreitWigner[rho3_1690_-]=[pi-[3,0]pi0][2,4]omega_782_0]
[1-,6+,1+]=[multiChannelRelativisticBreitWigner[rho_1450_-]=[omega_782_0[1,1]pi-][6,1]pi0]
[1-,6+,1+]=[rho_770_-=[pi-[1,0]pi0][4,2]omega_782_0]
[1-,6+,1+]=[rho_770_-=[pi-[1,0]pi0][6,2]omega_782_0]
[1-,6-,0+]=[multiChannelRelativisticBreitWigner[b1_1235_-]=[omega_782_0[0,1]pi-][6,1]pi0]
[1-,6-,0+]=[multiChannelRelativisticBreitWigner[rho3_1690_-]=[omega_782_0[3,1]pi-][3,3]pi0]
[1-,6-,0+]=[rho_770_-=[pi-[1,0]pi0][5,1]omega_782_0]
[1-,6-,0+]=[rho_770_-=[pi-[1,0]pi0][7,1]omega_782_0]
[1-,7+,0+]=[multiChannelRelativisticBreitWigner[b1_1235_-]=[omega_782_0[0,1]pi-][7,1]pi0]
[1-,7+,0+]=[multiChannelRelativisticBreitWigner[rho3_1690_-]=[omega_782_0[3,1]pi-][4,3]pi0]
[1-,7+,0+]=[rho_770_-=[pi-[1,0]pi0][8,2]omega_782_0]
[1-,8+,1+]=[multiChannelRelativisticBreitWigner[b1_1235_-]=[omega_782_0[0,1]pi-][7,1]pi0]
[1-,8+,1+]=[rho_770_-=[pi-[1,0]pi0][6,2]omega_782_0]
[1-,8+,1+]=[rho_770_-=[pi-[1,0]pi0][8,2]omega_782_0]
[1-,8-,0+]=[multiChannelRelativisticBreitWigner[rho3_1690_-]=[omega_782_0[3,1]pi-][5,3]pi0]
[1-,8-,0+]=[rho_770_-=[pi-[1,0]pi0][7,1]omega_782_0]
