#!/usr/bin/env python
# coding: utf-8

import ROOT
import yaml
import numpy as np

# script to generated omega shape
# omega shape (amplitude level) is approximated as sqrt of Voigt
# relies on values obtained by fitting real data for 2008 and 2009

# values obtained from fitting a (non-rel) Voigt to data
# seperately for 2008/2009 due to different mean

# 2008
mOmega2008  = 0.78420
sigma2008   = 0.00374
gammaBW2008 = 0.01404

# 2009
mOmega2009  = 0.78360
sigma2009   = 0.00363
gammaBW2009 = 0.01524

# Combined
mOmegaComb  = 0.78393
sigmaComb   = 0.00369
gammaBWComb = 0.01459

# y-sec and slope of linear background for Combined
backgroundComb0 = -26511.31
backgroundComb1 = 47454.89

# background pars must be multiplied by 1000 for binning and scaled down by amplitude of the omega peak to have right proportions
ampOmegaPeakComb = 1785540.18

backgroundComb0 *= 1000. / ampOmegaPeakComb
backgroundComb1 *= 1000. / ampOmegaPeakComb

def createTable(fileName, mOmega, sigma, gammaBW, spacing):
	voigtAmpVals = [ np.sqrt(ROOT.TMath.Voigt(x - mOmega, sigma, gammaBW)) for x in spacing ]
	data = [ {'mass': float(spacing[i]), 'ampRe': float(voigtAmpVals[i]), 'ampIm': 0} for i in range(len(spacing)) ]
	with open(fileName, 'w') as ymlFile:
		yaml.dump(data, ymlFile)#, default_flow_style=False)

def createTableBackground(fileName, par0, par1, spacing):
	backgroundAmps = [ np.sqrt(max(par0 + par1*x, 0.)) for x in spacing ]
	data = [ {'mass': float(spacing[i]), 'ampRe': float(backgroundAmps[i]), 'ampIm': 0} for i in range(len(spacing)) ]
	with open(fileName, 'w') as ymlFile:
		yaml.dump(data, ymlFile)#, default_flow_style=False)

def main():

	# create spacing from 0. to 4.0 GeV with coarse spacing at tails
	# and 2 MeV binning in peak area
	linspaces = [np.linspace(0., 0.5, 11)[:-1],\
	             np.linspace(0.5, 0.75, 26)[:-1],\
	             np.linspace(0.75, 0.81, 301)[:-1],\
	             np.linspace(0.81, 1.0, 20)[:-1],\
	             np.linspace(1.1, 1.6, 11)[:-1],\
	             np.linspace(1.6, 4.0, 25)]
	spacing = np.array([])
	for i in linspaces:
		spacing = np.append(spacing, np.array(i))

	createTable('omegaShape2008.yaml', mOmega2008, sigma2008, gammaBW2008, spacing)
	createTable('omegaShape2009.yaml', mOmega2009, sigma2009, gammaBW2009, spacing)
	createTable('omegaShapeComb.yaml', mOmegaComb, sigmaComb, gammaBWComb, spacing)
	createTableBackground('backgroundShapeComb.yaml', backgroundComb0, backgroundComb1, spacing)
	exit()

if __name__ == '__main__':
	main()
