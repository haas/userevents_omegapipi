#!/usr/bin/env python
# coding: utf-8
'''
Created on Mon 09 Oct 2017 03:17:29 PM CEST


@author: Stefan Wallner
@author: Philipp Haas
'''

program_description = '''
	<description>
'''

# std includes
from optparse import OptionParser
from collections import defaultdict
import os
import sys
import subprocess as sp
import shutil
import glob



OmegaParameters = {
	"2008pars":     { 'filePath': '/nfs/freenas/tuph/e18/project/compass/analysis/phaas/software/userevents_omegapipi/PWA/omegaMassShape/omegaShape2008.yaml', 'interpolate': 'true' },
        "2009pars":     { 'filePath': '/nfs/freenas/tuph/e18/project/compass/analysis/phaas/software/userevents_omegapipi/PWA/omegaMassShape/omegaShape2009.yaml', 'interpolate': 'true' },
}

def main():
	optparser = OptionParser( usage="Usage:%prog <args> [<options>]", description = program_description );
#	 optparser.add_option('', '--n-threads', dest='n_threads', action='store', type='int', default=multiprocessing.cpu_count(), help="Number of parallel threads for [default: %default].")

	( options, args ) = optparser.parse_args();

	if os.path.exists("keyfiles"):
		if input("'keyfiles' exists! Overwrite [y/N]:").lower() in ['y']:
			shutil.rmtree("keyfiles")
		else:
			sys.exit(1)

	sp.check_call("./GenerateKeyfiles.sh", shell=True)

	keyFiles = glob.glob("keyfiles/*.key")

	for keyFile in keyFiles:
		tmpFileName = keyFile + ".tmp"
		shutil.move(keyFile, tmpFileName)
		for name, parameters in OmegaParameters.items():
			fileName = keyFile.replace("omega_782_0", "omega_782_0<{}>".format(name))
			with open(fileName, "w") as fout:
				with open(tmpFileName) as fin:
					for line in fin:
						if 'name = "omega(782)0";' in line:
							fout.write('          name = "omega(782)0";\n')
							fout.write('          massDep : {\n')
							fout.write('              name ="lookupTable";\n')
							fout.write('              tag = "{0}";\n'.format(name))
							fout.write('              filePath = "{0}";\n'.format(parameters['filePath']))
							fout.write('              interpolate = "{0}";\n'.format(parameters['interpolate']))
							fout.write('            };\n')
						else:
							fout.write(line)
		fileName = keyFile.replace("keyfiles", "keyfilesWithBS")
		with open(fileName, "w") as fout:
			with open(tmpFileName) as fin:
				for line in fin:
					if 'boseSymmetrize = false' in line:
						line.replace("false", "true")
					else:
						fout.write(line)
		os.remove(tmpFileName)
	keyFiles = glob.glob("keyfiles/*.key")
	print("{0} keyfiles generated".format(len(keyFiles)))


	sys.exit(0)


if __name__ == '__main__':
	main()




