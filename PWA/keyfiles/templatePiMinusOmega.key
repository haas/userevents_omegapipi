//-------------------------------------------------------------------------
//
// Description:
//      wave set generator template key file for decay topology pi- pi0 omega
//
//
// Author List:
//      Philipp Haas          TUM            (original author)
//
//
//-------------------------------------------------------------------------


productionVertex :
{
	type = "diffractiveDissVertex";
	beam :
	{
		name = "pi-";
	};
	target :
	{
		name = "p+";
	};
};


decayVertex :
{
	XDecay :
	{
		isobars = ({
				name        = "isobarA";
				specials = ({
						name        = "omega(782)0";
						fsParticles = ({
								name  = "pi-";
								index = 0;
							},
							{
								name  = "pi0";
								index = 0;
							},
							{
								name  = "pi+";
								index = 0;
							});
					});
				fsParticles = ({
						name  = "pi-";
						index = 1;
					});
			});
		fsParticles = ({
				name     = "pi0";
				index = 1;
			});
	};
};


amplitude :
{
	boseSymmetrize = false;
	isospinSymmetrize = true;
	useReflectivityBasis = true;
};

waveSetParameters :
{
	// X quantum number ranges
	isospinRange     = [0,  2];
	JRange           = [0,  14];
	MRange           = [0,  4];
	reflectivity     = 0;
	useReflectivity  = true;
	allowSpinExotics = true;

	// X and isobar decay quantum number ranges
	LRange = [0,  14];
	SRange = [0,  6];

	// allowed isobars
	isobarWhiteList = [
		"rho3(1690)",
		"b1(1235)",
		"rho(1450)",
	];
};
